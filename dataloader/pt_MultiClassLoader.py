        
import torch
from functools import reduce
import numpy as np
import random
class MultiClassDataset(torch.utils.data.Dataset):
    
    
    def __init__(self,root=None,cache_dir="/space/grafr", transforms = None,validation = False,labels = ["Pneumothorax","Pleural Effusion","Cardiomegaly","Support Devices"], dataset = 0):
        super().__init__()
        self.transforms = transforms
        self.validation = validation
        self.labels = labels
        if self.transforms is None:
            print('Warning: no transform given.')
        self.dataset = dataset
        if dataset == 0:
            from dataloader.chexpert import Chexpert
            self.data = Chexpert(root = root,cache_dir=cache_dir,version="small",verbose=False,validation = validation)
        elif dataset == 3:
            from dataloader.chestxray14 import ChestXray14
            
            self.data = ChestXray14(root = root,cache_dir=cache_dir,verbose=False,validation = validation)
            labels=[self.data.label_mapping[l] for l in labels]
            self.labels = labels
            for l in labels:
                assert(l != "")
        elif dataset == 2:
            from dataloader.vinbigdata import VinBigData
            self.data = VinBigData(root = root, cache_dir = cache_dir,validation=validation)
            labels=[self.data.label_mapping[l] for l in labels]
            self.labels = labels
            for l in labels:
                assert(l != "")
        elif dataset == 1:
            from dataloader.mimic_cxr import MIMIC
            self.data = MIMIC(root = root, cache_dir = cache_dir,validation = validation)
        else:
            raise NotImplementedError
        self.data.load()

        #edit class labels
        for label in labels:
            #if not self.data.labels is None:
            self.data.df[label] = self.data.df[label].apply(lambda label: 1 if label == -1 else label)
            #else:
            #    self.data.df[label] = self.data.df["class_name"].apply(lambda text: 1 if  label.lower() in text.lower() else 0)
        if not validation:
            #add Labels
            last = self.data.df
            self.classes = []
            self.lengths = 0
            self.num_lables = len(labels)+1
            for label in labels:
                self.classes += [last[last[label]==1].reset_index()]
                self.lengths+=len(self.classes[-1])
                last = last[last[label]!=1]
            self.classes += [last]

    def __str__ (self):
        return str(self.data.cache_dir)

    def getWeight(self,args):
        return self.data.getWeight(args)
    
    def __len__(self):
        if self.validation:
            return len(self.data.df)
        return self.lengths 
        
    def getSample(self,idx):
        
        if self.validation:
            sample = self.data.df.iloc[idx] 
        else:
            label_id = idx % self.num_lables
            data_frame = self.classes[label_id]
            
            idx = random.randint(0, len(data_frame)-1)
            #idx = np.random.choice(data_frame.index.tolist(),1)[0]
            #print(idx,idx2,'test')
            try:
                sample = data_frame.iloc[idx] 
            except Exception as e:
                print('\n',data_frame,idx, len(data_frame),'\n')
                raise e
        
        
        # label = np.array([self.data.df[l][idx] for l in self.labels])   
        label = np.array([sample[l] for l in self.labels])   
        
        label = np.nan_to_num(label)
        #print('\t ',sample,label_id,idx , label)
        img = self.data.load_image(sample.Path)
        self.img = img
        if not self.transforms is None:
            img = self.transforms(img).float()
        label =  torch.from_numpy(label).float()
        return img, label, img, sample

    def __getitem__(self, idx):
        img, label, _, _ = self.getSample(idx)
        return img,label
  
class MultiClassMultiDataset(torch.utils.data.Dataset):

    def __init__(self,root=None,cache_dir="/space/grafr", transforms = None,labels = ["Pneumothorax","Pleural Effusion","Cardiomegaly"],validation = False):
        super().__init__()
        self.validation = validation
        if validation:
            self.datasets = [MultiClassDataset(root=root,cache_dir=cache_dir,transforms=transforms,labels = labels,dataset = i,validation=validation) for i in range(2,3)]
        else:
            self.datasets = [MultiClassDataset(root=root,cache_dir=cache_dir,transforms=transforms,labels = labels,dataset = i) for i in range(4)]
        self.lens = [len(ds) for ds in self.datasets]
        print([(str(self.datasets[i]),self.lens[i]) for i in range(len(self.lens))])
    def __len__(self):
        return reduce(lambda a, b: a + b,self.lens)
    def __getitem__(self, idx):
        for i in range(len(self.lens)):
            if idx < self.lens[i]:
                return self.datasets[i][idx]
            else:
                idx -= self.lens[i]
        raise IndexError(f"Index out of Bounds {idx} of {len(self)}")