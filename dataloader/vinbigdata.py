#!/usr/bin/env python3
from cmath import isnan
from linecache import cache
import numpy as np
import torch
import cv2
from pathlib import Path
import subprocess

import pandas as pd
from PIL import Image

from dataloader.dataset import Dataset


class VinBigData(Dataset):
    classes = ['No finding', 'Cardiomegaly', 'Aortic enlargement', 'Pleural thickening',
               'ILD', 'Nodule/Mass', 'Pulmonary fibrosis', 'Lung Opacity', 'Atelectasis',
               'Other lesion', 'Infiltration', 'Pleural effusion', 'Calcification',
               'Consolidation', 'Pneumothorax']
    label_mapping = {
        "No Finding": "No finding",
        "Enlarged Cardiomediastinum": "",
        "Cardiomegaly": "Cardiomegaly",
        "Lung Opacity": "Lung Opacity",
        "Lung Lesion": "Other lesion",
        "Edema": "",
        "Consolidation": "Consolidation",
        "Pneumonia": "",
        "Atelectasis": "Atelectasis",
        "Pneumothorax": "Pneumothorax",
        "Pleural Effusion": "Pleural effusion",
        "Pleural Other": "",
        "Fracture": "",
        "Support Devices": "",
    }

    def __init__(self, root=None, cache_dir=None, resolution="512", validation=False):

        super().__init__(root)
        self.cache_dir = cache_dir
        self.name = "vinbigdata"
        self.resolution = resolution
        self.validation = validation

        if resolution == "256":
            self.resolution_dir = "vinbigdata-256"
        elif resolution == "512":
            self.resolution_dir = "vinbigdata-512"
        else:
            self.resolution_dir = "original"

        self.data_dir = self.root / "chestxray-data" / self.name / self.resolution_dir
        self.image_dir = self.data_dir
        self.path_labels = self.data_dir / "train.csv"
        self.path_test_labels = self.data_dir / "test.csv"

        if cache_dir is not None:
            cache_dir = Path(cache_dir).expanduser()
            print(f"Using {cache_dir} as cache dir.")
            if cache_dir.exists():
                cache_dir = cache_dir / self.name / self.resolution_dir
                cache_dir.mkdir(exist_ok=True)
                self.cache_dir = cache_dir
                self.cache = True
            else:
                raise ValueError(
                    "You need to specify an existing cache directory!")
        else:
            self.cache = False
        self.df = self._read_annotations(self.path_labels, "train")
        #self.classes = self.df.class_name.unique()
        self.labels = self.classes

    def load(self):
        pass

    def load_image(self, path):
        """Load an image."""
        path = self._cache(path)
        return Image.open(path).convert("RGB")

    def _get_image_path(self, image_id: str, split: str) -> str:
        """Return the path for an image id."""
        file_type = "dicom" if self.resolution == "original" else "png"
        return str(self.image_dir / split / f"{image_id}.{file_type}")

    def _read_annotations(self, path, split):
        path = self._cache(path)

        try:
            if self.validation:
                df = pd.read_csv(path)
                self.df_org = df
                df = pd.read_csv(str(path)[:-4]+'_transformed1.csv')
            else:
                df = pd.read_csv(str(path)[:-4]+'_transformed2.csv')
        except:
            df = pd.read_csv(path)
            #path_test = self._cache(self.path_test_labels)
            #df_test = pd.read_csv(path_test)
            #self.df_org = df
            #self.df_test = df_test
            df = self.transform_data_set(df, path)
        df.loc[:, "Path"] = df.loc[:, "image_id"].apply(
            lambda x: self._get_image_path(x, split=split)
        )

        return df

    def transform_data_set(self, df, path):

        # No Finding,Enlarged Cardiomediastinum,Cardiomegaly,Lung Opacity,Lung Lesion,Edema,Consolidation,Pneumonia,Atelectasis,Pneumothorax,Pleural Effusion,Pleural Other,Fracture,Support Devices
        # image_id,class_name,class_id,rad_id,x_min,y_min,x_max,y_max,width,height
        #
        #
        print(df)
        class_id = df.class_id.unique()
        self.classes = df.class_name.unique()
        image_id = df.image_id.unique()

        d = {}
        d['image_id'] = image_id
        print(self.classes)
        for label in self.classes:
            print(label)
            d[label] = [1 if ((df['image_id'] == i) & (
                df['class_name'] == label)).any() else 0 for i in image_id]

            #((df['image_id'] == i) & (df['class_name'] == label)).any()
        d1 = {}
        d2 = {}
        label = 'image_id'
        d1[label] = [image_id[i] for i in range(0, len(image_id), 2)]
        d2[label] = [image_id[i] for i in range(1, len(image_id), 2)]
        for label in self.classes:
            d1[label] = [d[label][i] for i in range(0, len(image_id), 2)]
            d2[label] = [d[label][i] for i in range(1, len(image_id), 2)]
            
        #######
        #for label in self.classes:
        #    print(label)
        #    d[label] = [1 if ((df['image_id'] == i) & (
        #        self.df_test['class_name'] == label)).any() else 0 for i in image_id]
        #
        #######

        df1 = pd.DataFrame(data=d1)
        df2 = pd.DataFrame(data=d2)
        df1.to_csv(str(path)[:-4]+'_transformed1.csv', index=False)
        df2.to_csv(str(path)[:-4]+'_transformed2.csv', index=False)
        print(df1)
        if self.validation:
            return df1
        else:
            return df2

    def load_mask(self, sample: pd.core.series.Series):
        df = self.df_org
        df = df[df['image_id'] == sample.image_id]
        df = df.reset_index()
        # image_id          class_name  class_id rad_id   x_min   y_min   x_max   y_max  width  height
        masks = {}
        for index, row in df.iterrows():
            try:
                masks[row['class_name']+str(index)] = (int(row['x_min']),
                                                       int(row['y_min']),
                                                       int(row['x_max']),
                                                       int(row['y_max']),
                                                       int(row['width']),
                                                       int(row['height']),)
            except:
                pass
        return masks

    def load_bbox(self, masks, label):
        mask = None
        for l, box in masks.items():
            x_min, y_min, x_max, y_max, width, height = box
            if mask is None:
                mask = np.zeros((width, height))
            if label in l:
                if not isnan(x_min):
                    x = int(x_min)
                    #x = int(sample.x)
                    y = int(y_min)
                    x2 = int(x_max)
                    y2 = int(y_max)
                    mask[x:x2, y:y2] = 255
        return mask.T
        

    def get_box_sampls(self, label):
        if label in self.label_mapping:
            label = self.label_mapping[label]
        return self.df_box[self.df_box['Finding Label'] == label]

    def __len__(self):
        return len(self.df)

    def _cache(self, source):
        """Cache the source file if a cache directory is available.

        Parameters
        ----------
        source : str, Path
            Source of the file.
        """

        if self.cache:
            source = Path(source)
            destination = self.cache_dir / source.relative_to(self.data_dir)
            if destination.exists():
                return destination
            print(f"Caching {source} in {self.cache_dir}..")
            command = f"mkdir -p {str(destination.parent)}"
            subprocess.run(command.split())
            command = f"rsync -avhW --progress {str(source)} {str(destination)}"
            subprocess.run(command.split())
            print(f"Copied {source} to {destination}.")
            return destination
        else:
            return source


class VinBigDataSeg(torch.utils.data.Dataset):
    def __init__(self, root, cache_dir, transforms, label='', labels=None):
        super().__init__()
        from dataloader.vinbigdata import VinBigData
        self.data = VinBigData(root=root, cache_dir=cache_dir, validation=True)
        labels = [self.data.label_mapping[l] for l in labels]
        label = self.data.label_mapping[label]
        for l in labels:
            assert(l != "")
        self.data.load()
        self.data.df = self.data.df[self.data.df['No finding']!=1]
        self.data.df.reset_index()
        self.label = label
        self.labels = labels
        assert(label != '')
        self.transforms = transforms
        
    def limit2class(self):
        self.data.df = self.data.df[self.data.df[self.label]==1]
        self.data.df.reset_index()
        
    def __len__(self):
        return len(self.data)

    def getSample(self, idx, short=True):
        # print('IDX',idx,self.index_list[idx])
        sample = self.data.df.iloc[idx]

        # print(sample)
        img = self.data.load_image(sample.Path)
        masks = self.data.load_mask(sample)
        mask = self.data.load_bbox(masks, self.label)
        if isinstance(mask, int):
            mask = np.zeros(img.size)
        if short:
            return img, mask.T

        label = np.array([sample[l] for l in self.labels])
        label = np.nan_to_num(label)

        return img, mask, masks, label

    def __getitem__(self, idx):
        img, mask = self.getSample(idx, short=True)

        img = self.transforms(img).float()

        mask = cv2.resize(mask, dsize=img.shape[-2:], 
                          interpolation=cv2.INTER_NEAREST)
        #mask = torch.from_numpy(mask).float()
        return img, mask
