import torch
from pathlib import Path
import re
import subprocess

import pandas as pd
from PIL import Image

from dataloader.dataset import Dataset
import numpy as np

#from ciip_dataloader.chexpert import Chexpert


class Chexpert(Dataset):
    """This is the CheXpert data set.

    See: https://stanfordmlgroup.github.io/competitions/chexpert/
    """

    labels = [
        "No Finding",
        "Enlarged Cardiomediastinum",
        "Cardiomegaly",
        "Lung Opacity",
        "Lung Lesion",
        "Edema",
        "Consolidation",
        "Pneumonia",
        "Atelectasis",
        "Pneumothorax",
        "Pleural Effusion",
        "Pleural Other",
        "Fracture",
        "Support Devices",
    ]

    def __init__(self, root=None, version="small",
                 cache_dir='/space/grafr', verbose=False,
                 validation=False):
        """Abstraction of the CheXpert data set.

        Parameters
        ----------
        root : Optional(str/Path)
            Path to the share-all dir.
        version : str = "small"
            Dataset version.
        cach_dir : Optional(str/Path)
            Path to the cache dir.
        """
        super().__init__(root=root, cache_dir=cache_dir, verbose=verbose)

        self.versions = {"original": "CheXpert-v1.0",
                         "small": "CheXpert-v1.0-small"}
        self.name = self.versions[version]
        self.data_dir = self.root / "chestxray-data/chexpert" / self.name
        self.validation = validation

    def load(self):
        print("Load the training dataframe into memory.")
        """Load the training dataframe into memory."""
        self.labels_path = self._cache(self.data_dir / "train.csv")
        if self.validation:
            self.labels_path = self._cache(self.data_dir / "valid.csv")

        self.images_path = self.data_dir / "train"
        if self.validation:
            self.images_path = self.data_dir / "valid"

        self._read_data()
        self.categories = self.df.columns

    def _get_patient_id(self, path: str) -> str:
        return re.search("patient[0-9]+", path).group(0)

    def _get_image_path(self, path: str) -> str:
        return str(self.data_dir.parent / path)

    def get_original_size(self, path: str) -> str:
        path = Path(path)
        image_path = path.relative_to(self.data_dir)
        original_path = self.data_dir.parent / \
            self.versions["original"] / image_path
        return original_path

    def _read_data(self):
        df = pd.read_csv(self.labels_path)
        df["patient_id"] = df.Path.apply(self._get_patient_id)
        df["Path"] = df.Path.apply(self._get_image_path)
        self.df = df

    def getWeight(self, args, u=1):
        df = self.df
        w = []
        num_all = len(df)
        for label in self.labels:
            frame = df[label].value_counts()
            num_pos = frame[1.0]
            if u == 1 and (-1 in frame):
                num_pos += frame[-1]
            num_neg = num_all - num_pos
            if args.no_weight:
                w.append(1)
            elif args.relativ:
                w.append(num_neg/num_pos)
            elif args.inf_relativ:
                w.append(num_pos/num_neg)

        print("weights:", [(self.labels[i], w[i]) for i in range(len(w))])
        return torch.cuda.FloatTensor(w)

    def __len__(self) -> int:
        return len(self.df["Path"])

    def _label_transform(self, x, u=1):
        if pd.isna(x):
            return 0
        if x == -1:
            return u
        else:
            return x

    def _getLabel(self, index, ):
        l = [self._label_transform(self.df[l][index]) for l in self.labels]
        return np.array(l)

    def getItem(self, index):
        image_path = self.df["Path"][index]
        label = self._getLabel(index)
        return self.load_image(image_path), label

    def get_train_labels(self, category=None, study=None,
                         frontal_lateral=None, ap_pa=None):
        result = self.df
        if study is not None:
            result = result.loc[result.Path.str.contains(
                f"/study{self.study}/")]
        if category is not None and category in self.categories:
            result = result.loc[result[category] == 1]
        if frontal_lateral is not None:
            result = result.loc[result["Frontal/Lateral"] == frontal_lateral]
        if ap_pa is not None:
            result = result.loc[result["AP/PA"] == ap_pa]
        return result

        return self.df

    # def get_val_labels(self):
    #    df_val = pd.read_csv(self.val_labels_path)
    #    return df_val

    def load_image(self, path):
        return Image.open(self._cache(path)).convert("RGB")

    def show_image(self, img, label=None):
        import matplotlib.pyplot as plt
        import matplotlib.image as mpimg
        imgplot = plt.imshow(img)
        if label is not None:
            plt.settitle(str(label))
        plt.show()

    def download(self, destination):
        """Copy the dataset to destination.

        Parameters
        ----------
        destination : str, Path
            Destination of the dataset.
        """

        # Copy the dataset from ceph to destination.
        source_path = str(self.data_dir) + ".zip"
        destination_path = Path(destination).expanduser()
        if not destination_path.exists():
            raise ValueError("You need to specify a correct destination path!")
        command = f"rsync -avhW --progress {source_path} {str(destination_path)}"
        subprocess.run(command.split())
        print(f"Copied to {destination_path}.")

        # Unpack the zip and update the data_dir.
        if not (destination_path / self.data_dir.name).exists():
            print(f"Extracting..")
            filename = str(self.data_dir.name + ".zip")
            source_file = destination_path / filename
            destination = str(destination_path)
            command = f"unzip {source_file} -d {destination}"
            subprocess.run(command.split())
        self.data_dir = destination_path / self.data_dir.name


class ChexpertDataset(torch.utils.data.Dataset):
    def __init__(self, root=None, cache_dir="/space/grafr", transforms=None, version="small", validation=False):
        super().__init__()
        self.transforms = transforms
        self.data = Chexpert(root=root, cache_dir=cache_dir,
                             version=version, verbose=False, validation=validation)
        self.data.load()

    def getWeight(self, args):
        return self.data.getWeight(args)
        #df = df.groupby('domain')['ID'].nunique()

    def __len__(self):
        return len(self.data.df)

    def __getitem__(self, idx):
        label = self.data._getLabel(idx)
        sample = self.data.df.iloc[idx]
        img = self.data.load_image(sample.Path)
        #img = np.array(img)
        self.img = img
        img = self.transforms(img).float()
        label = torch.from_numpy(label).float()
        return img, label  # ,sample, idx


class ChexpertDatasetSingle(torch.utils.data.Dataset):

    def __init__(self, root=None, cache_dir="/space/grafr", transforms=None, version="small", validation=False, label="Pneumothorax"):
        super().__init__()
        self.transforms = transforms
        self.data = Chexpert(root=root, cache_dir=cache_dir,
                             version=version, verbose=False, validation=validation)
        self.data.load()
        assert(label in self.data.labels)
        self.data.df[self.data.df[label] == -1][label] = 1
        self.pos = self.data.df[self.data.df[label] == 1]
        self.neg = self.data.df[self.data.df[label] != 1]
        assert(len(self.pos) <= len(self.neg))

    def getWeight(self, args):
        return self.data.getWeight(args)
        #df = df.groupby('domain')['ID'].nunique()

    def __len__(self):
        return 2*len(self.pos)

    def __getitem__(self, idx):
        if idx % 2 == 0:
            sample = self.pos.iloc[int(idx//2)]
            label = np.array(1)
            #print('pos', sample.Path,idx//2)
        else:
            idx2 = np.random.randint(len(self.neg))
            sample = self.neg.iloc[idx2]
            label = np.array(0)
            #print('neg', sample.Path,idx2)

        img = self.data.load_image(sample.Path)
        #img = np.array(img)
        self.img = img
        img = self.transforms(img).float()
        label = torch.from_numpy(label).float()
        return img, label  # ,sample, idx
