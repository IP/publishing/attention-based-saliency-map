
import cv2
import torch
import pandas as pd
from PIL import Image
import numpy as np
from dataloader.dataset import Dataset


class ChestXray14(Dataset):
    """This is the ChestX-ray 14 data set.

    See: https://arxiv.org/abs/1705.02315
    """

    labels = [
        "Cardiomegaly",
        "Emphysema",
        "Effusion",
        "No Finding",
        "Hernia",
        "Infiltration",
        "Mass",
        "Nodule",
        "Atelectasis",
        "Pneumothorax",
        "Pleural_Thickening",
        "Pneumonia",
        "Fibrosis",
        "Edema",
        "Consolidation",
    ]

    # "Emphysema",

    # "Infiltration",
    # "Mass",
    # "Nodule",
    # "Pleural_Thickening",
    # "Fibrosis",
    label_mapping = {
        "No Finding": "No Finding",
        "Enlarged Cardiomediastinum": "",
        "Cardiomegaly": "Cardiomegaly",
        "Lung Opacity": "",
        "Lung Lesion": "",
        "Edema": "Edema",
        "Consolidation": "Consolidation",
        "Pneumonia": "Pneumonia",
        "Atelectasis": "Atelectasis",
        "Pneumothorax": "Pneumothorax",
        "Pleural Effusion": "Effusion",
        "Pleural Other": "",
        "Fracture": "Hernia",
        "Support Devices": "",

    }

    def __init__(self, root=None, cache_dir=None, verbose=False, validation=False):
        super().__init__(root, cache_dir=cache_dir, verbose=verbose)
        self.data_dir = self.root / "chestxray-data/chestxray14"
        self.validation = validation

    def load(self):
        """Load the training dataframe into memory."""
        self.labels_path = self._cache(
            self.data_dir / "Data_Entry_2017_v2020.csv"
        )
        self.image_dir = self.data_dir / "images"
        self.train_val_images_path = self.data_dir / "train_val_list.txt"
        self.test_images_path = self.data_dir / "test_list.txt"
        self.bbox_path = self.data_dir / "BBox_List_2017.csv"
        self._read_data()

    def _read_data(self):
        df = pd.read_csv(self.labels_path)

        # Add disease labels as columns instead of string-based "Disease1|Disease2".
        for label in self.labels:
            df.loc[:, label] = self._get_disease_label(df, label)
        df.drop("Finding Labels", axis=1, inplace=True)

        df.loc[:, "Path"] = df["Image Index"].apply(self._get_image_path)
        self.df_train = df.loc[df["Image Index"].isin(
            self._read_file(self.train_val_images_path))]
        self.df_test = df.loc[df["Image Index"].isin(
            self._read_file(self.test_images_path))]

        self.df_box = pd.read_csv(self.bbox_path)
        self.df_box.loc[:, "Path"] = self.df_box["Image Index"].apply(
            self._get_image_path)
        if self.validation:
            self.df = self.df_test
        else:
            self.df = self.df_train

    def load_image(self, path: str) -> Image:
        """Cache and load an image."""
        return Image.open(self._cache(path)).convert("RGB")

    def _get_disease_label(self, df: pd.DataFrame, disease: str) -> pd.Series:
        return df["Finding Labels"].apply(lambda label: 1 if disease in label else 0)

    def _get_image_path(self, path: str) -> str:
        return str(self.image_dir / path)

    def _read_file(self, path):
        with open(path) as f:
            return f.read().split("\n")

    def load_mask(self, sample: pd.core.series.Series, label):
        mask = np.zeros((1024, 1024))

        x = int(sample['Bbox [x'])
        #x = int(sample.x)
        y = int(sample.y)
        x2 = int(sample['Bbox [x']+sample.w)
        y2 = int(sample.y+sample['h]'])

        if sample['Finding Label'] == label:
            mask[x:x2, y:y2] = 1
        masks = {sample['Finding Label']: (x, y, x2, y2, 1024, 1024)}
        #print(sample,'\n',label,sample['Finding Label'] == label,mask.sum(),'\n','\n',masks,'\n','\n')
        return mask.T, masks
    # def get_box_sampls(self,label):
    #    if label in self.label_mapping:
    #        label = self.label_mapping[label]
    #    return self.df_box[self.df_box['Finding Label'] == label]

    # Image Index,Finding Label,Bbox [x,y,w,h],,,
    # 00012123_001.png,Atelectasis,639.457627118644,611.019216763771,273.35593220339,215.864406779661
    # 1024x1024
    # Oben links kords


class ChestXray14Seg(torch.utils.data.Dataset):
    def __init__(self, root, cache_dir, transforms, label='', labels=None):
        super().__init__()
        self.data = ChestXray14(root=root, cache_dir=cache_dir, verbose=False)
        self.data.load()
        label = self.data.label_mapping[label]
        self.label = label
        self.labels = [self.data.label_mapping[l] for l in labels]

        assert(label != '')

        self.transforms = transforms
        self.limit2classes()

    def __len__(self):
        return len(self.df_box)

    def limit2classes(self):
        self.df_box = self.data.df_box
        self.df_box["Finding Label"] = self.df_box["Finding Label"].apply(lambda label: label if label in self.labels else 0)
        self.df_box = self.df_box[self.df_box['Finding Label'] != 0]
        self.df_box = self.df_box.reset_index()

    def limit2class(self):

        self.df_box = self.df_box[self.df_box['Finding Label'] == self.label]
        self.df_box = self.df_box.reset_index()
        #print(self.df_box)

    def getSample(self, idx, short=True):
        # print('IDX',idx,self.index_list[idx])

        sample_box = self.df_box.iloc[idx]
        img = self.data.load_image(sample_box.Path)
        mask, masks = self.data.load_mask(sample_box, self.label)
        if short:
            return img, mask
        img_idx = sample_box['Image Index']
        sample = self.data.df[self.data.df["Image Index"] == img_idx]

        label = np.array([sample[l] for l in self.labels])
        label = np.nan_to_num(label)
        return img, mask, masks, label

    def __getitem__(self, idx):
        img, mask = self.getSample(idx, short=True)

        img = self.transforms(img).float()

        mask = cv2.resize(
            mask, dsize=img.shape[-2:], interpolation=cv2.INTER_NEAREST)
        #mask = torch.from_numpy(mask).float()
        return img, mask
