#!/usr/bin/env python3
from pathlib import Path
import subprocess

import pandas as pd
from PIL import Image

from dataloader.dataset import Dataset


class MIMIC(Dataset):
    labels = ["Atelectasis", "Cardiomegaly", "Consolidation", "Edema", "Enlarged Cardiomediastinum", "Fracture",
               "Lung Lesion", "Lung Opacity", "No Finding", "Pleural Effusion", "Pleural Other", "Pneumonia",
               "Pneumothorax", "Support Devices"]

    def __init__(self, root=None, cache_dir=None, resolution="512", validation=False):

        super().__init__(root)
        self.cache_dir = cache_dir
        self.name = "mimic-cxr"
        self.resolution = resolution
        self.validation = validation
        if resolution == "256":
            assert(False)
        elif resolution == "512":
            self.resolution_dir = "dataset_512"
        else:
            assert(False)

        self.data_dir = self.root / "chestxray-data" / self.name / self.resolution_dir
        self.image_dir = self.data_dir / "files"
        self.chexpert_label = self.data_dir / 'mimic-cxr-2.0.0-chexpert.csv'
        self.hash2label = self.data_dir / 'mimic-cxr-2.0.0-split.csv'
        self.path_labels = self.data_dir / "train.csv"
        self.path_test_labels = self.data_dir / "test.csv"

        if cache_dir is not None:
            cache_dir = Path(cache_dir).expanduser()
            print(f"Using {cache_dir} as cache dir.")
            if cache_dir.exists():
                cache_dir = cache_dir / self.name / self.resolution_dir
                cache_dir.mkdir(exist_ok=True)
                self.cache_dir = cache_dir
                self.cache = True
            else:
                raise ValueError(
                    "You need to specify an existing cache directory!")
        else:
            self.cache = False
        self.df = self._read_annotations(self.path_labels,)
        
    def load(self):
        pass

    def load_image(self, path):
        """Load an image."""
        path = self._cache(path)
        return Image.open(path).convert("RGB")
    def __len__(self):
        return len(self.df)
    def _get_image_path(self, image_id: str,) -> str:
        """Return the path for an image id."""
        file_type = "dicom" if self.resolution == "original" else "jpg"
        return str(self.image_dir / f"{image_id}.{file_type}")
    def _label_transform(self,label, x,u = 1):
        try:
            x = x[label].item()
        except Exception as e:
            print(x)
            raise e
        if pd.isna(x):
            return 0
        if x == -1:
            return u
        else:
            return int(x)

    def _read_annotations(self, path):
        path = self._cache(path)
        try:
            if self.validation:
                df = pd.read_csv(self.path_test_labels)
            else:
                df = pd.read_csv(self.path_labels)

        except:
            df_label = pd.read_csv(self.chexpert_label)
            df_hash = pd.read_csv(self.hash2label)
            df = self.transform_data_set(df_label, df_hash, path)
        df.loc[:, "Path"] = df.loc[:, "image_id"].apply(
            lambda x: self._get_image_path(x)
        )

        return df

    def transform_data_set(self, df_label, df_hash, path):

        # subject_id,study_id,Atelectasis,Cardiomegaly,Consolidation,Edema,Enlarged Cardiomediastinum,Fracture,Lung Lesion,Lung Opacity,No Finding,Pleural Effusion,Pleural Other,Pneumonia,Pneumothorax,Support Devices
        # dicom_id,study_id,subject_id,split
        #
        #
        if self.validation:
            df_hash = df_hash[df_hash['split'] == 'validate']
        else:
            df_hash = df_hash[df_hash['split'] == 'train']

        # Path /p{subject_id[:2]}/p{subject_id}/s{study_id}/{dicom_id}  (.png)
        # dicom_id,study_id,subject_id
        print('compute names')
        image_id = [f"p{str(subject_id)[:2]}/p{subject_id}/s{study_id}/{dicom_id}"
                    for subject_id, study_id, dicom_id in zip(df_hash['subject_id'], df_hash['study_id'], df_hash['dicom_id'],)]
        
        

        d = {}
        d['image_id'] = image_id
        print(self.labels)
        
        for label in self.labels:
            d[label] = []
        count = 0
        for subject_id, study_id,dicom_id in zip(df_hash['subject_id'], df_hash['study_id'], df_hash['dicom_id']):
            frame = df_label.loc[(df_label['subject_id'] == subject_id) & (df_label['study_id'] == study_id)]
            name = f"p{str(subject_id)[:2]}/p{subject_id}/s{study_id}/{dicom_id}"
            import os.path
           
            if not os.path.isfile(str(self.image_dir / name )+'.jpg'):
                print(str(self.image_dir / name )+'.jpg', 'this image does not exist.', (count))
                image_id.remove(name)
            elif len(frame) == 0:
                print(name, 'not in mimic-cxr-2.0.0-chexpert.csv')
                image_id.remove(name)
            else:
                count+=1
                for label in self.labels:
                    d[label].append(self._label_transform(label,frame))
        d['image_id'] = image_id
                    

            #((df['image_id'] == i) & (df['class_name'] == label)).any()
        df = pd.DataFrame(data=d)
        
        if self.validation:
            df.to_csv(self.path_test_labels, index=False)
        else:
            df.to_csv(self.path_labels, index=False)
                
        return df

    def _cache(self, source):
        """Cache the source file if a cache directory is available.

        Parameters
        ----------
        source : str, Path
            Source of the file.
        """

        if self.cache:
            source = Path(source)
            destination = self.cache_dir / source.relative_to(self.data_dir)
            if destination.exists():
                return destination
            print(f"Caching {source} in {self.cache_dir}..")
            command = f"mkdir -p {str(destination.parent)}"
            subprocess.run(command.split())
            command = f"rsync -avhW --progress {str(source)} {str(destination)}"
            subprocess.run(command.split())
            print(f"Copied {source} to {destination}.")
            return destination
        else:
            return source
