import torch
import numpy as np
import pandas as pd
from PIL import Image
from dataloader.dataset import Dataset
from dataloader.utils import dicom, mask_functions


class SIIM_ACR_Pneumothorax_Segmentation(Dataset):
    """This is the SIIM ACR Pneumothorax Segmentation data set.
    The images are based on the NIH ChestXray14 data set.

    See: https://www.kaggle.com/c/siim-acr-pneumothorax-segmentation
    """

    labels = ["Pneumothorax"]

    def __init__(self, root=None, cache_dir=None, verbose=False):
        super().__init__(root, cache_dir=cache_dir, verbose=verbose)
        self.data_dir = self.root / "chestxray-data/siim-acr-pneumothorax-segmentation-data"

    def load(self):
        """Load the training dataframe into memory."""
        self.train_labels_path = self._cache(
            self.data_dir / "stage-2/siim/train-rle.csv"
        )
        self.image_dir = self.data_dir / "stage-2/siim/dicom-images-train"
        self.image_paths = {path.stem: str(path)
                            for path in self.image_dir.rglob('*.dcm')}
        
        self._read_data()
        self.keys = list(np.unique(list(self.df.ImageId)))

    def _read_data(self):
        df_train = pd.read_csv(self.train_labels_path)
        # " EncodedPixels" --> "EncodedPixels"
        df_train = self._fix_column_name(df_train)
        df_train = self._add_pneumothorax_label(df_train)
        df_train.loc[:, "Path"] = df_train["ImageId"].apply(
            self._get_image_path)
        self.df = df_train

    def load_image(self, path: str) -> Image:
        """Cache and load an image."""
        return Image.fromarray(dicom.dicom_to_array(self._cache(path), rgb=True))

    def load_mask(self, sample: pd.core.series.Series):
        metadata = dicom.get_dicom_metadata(self._cache(sample.Path))
        mask = mask_functions.rle2mask(sample.EncodedPixels, metadata.Rows, metadata.Columns)*0 #Lazy way to make the mask
        for idx,sample_global in self.df[sample.ImageId == self.df.ImageId].iterrows():
            mask += mask_functions.rle2mask(sample_global.EncodedPixels, metadata.Rows, metadata.Columns)
        mask[mask>255] = 255
        
        # Mask has to be transposed
        return mask.T

    def show_image_with_mask(self, sample: pd.core.series.Series, alpha=0.2) -> Image:
        """Show the mask on the image."""
        image = dicom.dicom_to_array(self._cache(sample.Path))
        mask = self.load_mask(sample)
        overlay = np.clip(alpha * mask + image, 0, 255).astype('uint8')
        overlay = np.stack((overlay, image, image), axis=2)
        return Image.fromarray(overlay)

    def _fix_column_name(self, df: pd.DataFrame):
        """Remove the whitespace in the column name."""
        df.loc[:, "EncodedPixels"] = df.loc[:, " EncodedPixels"]
        return df.drop(" EncodedPixels", axis=1)

    def _add_pneumothorax_label(self, df: pd.DataFrame):
        """Add the Pneumothorax labels based on PTX segmentation mask. """
        df.loc[:, "Pneumothorax"] = df["EncodedPixels"].apply(
            lambda x: 0 if x == "-1" else 1)
        return df

    def _get_image_path(self, path: str) -> str:
        return self.image_paths[path]


class SIIMDatasetSingle(torch.utils.data.Dataset):

    def __init__(self, root=None, cache_dir="/space/grafr", transforms=None, validation=False):
        super().__init__()
        self.data = SIIM_ACR_Pneumothorax_Segmentation(
            root=root, cache_dir=cache_dir, verbose=True)
        self.data.load()
        self.transforms = transforms
        assert(not validation)

    def getWeight(self, args):
        return self.data.getWeight(args)
        #df = df.groupby('domain')['ID'].nunique()

    def __len__(self):
        return len(self.data.keys)#len(self.data.df)
    
    def limit2class(self):
        pass

    def getSample(self, idx, lagacy = False):
        if lagacy:
            sample = self.data.df.iloc[idx]
        else:
            name = self.data.keys[idx]
            sample = self.data.df[name == self.data.df.ImageId].index[0]#.first_valid_index()
            sample = self.data.df.iloc[sample]
            #print(type(sample),sample,len(sample),idx, '\n')
            assert((name == sample.ImageId))
            
        img = self.data.load_image(sample.Path)
        if sample.Pneumothorax == 1:
            mask = self.data.load_mask(sample)
            masked_img = self.data.show_image_with_mask(sample)
        else:
            return img, torch.zeros(img.size), img, 0
        #img = self.transforms(img).float()
        return img, mask, masked_img, 1  # ,sample, idx

    def __getitem__(self, idx):
        img, _, _, label = self.getSample(idx)
        img = self.transforms(img).float()
        label = torch.from_numpy(np.array(label)).float()
        return img, label

import cv2
class SIIMDatasetSeg(torch.utils.data.Dataset):

    def __init__(self, **kwargs):
        super().__init__()
        self.intermidiat = SIIMDatasetSingle(**kwargs)
        
        self.data = self.intermidiat.data
        df = self.data.df[self.data.df['Pneumothorax']==1]
        names = list(set(df.ImageId))
        self.index_list = [self.data.keys.index(n) for n in names ]
        self.transforms = self.intermidiat.transforms
    def __len__(self):
        return len(self.index_list)
    def getSample(self, idx):
        return self.intermidiat.getSample(idx)
    def __getitem__(self, idx):
        img, mask, masked_img, gt = self.intermidiat.getSample(self.index_list[idx])
        assert(gt == 1)
        img = self.transforms(img).float()
        
        mask = cv2.resize(mask, dsize=img.shape[-2:], interpolation=cv2.INTER_NEAREST)
        mask = mask
        return img, mask
    def limit2class(self):
        pass

if __name__ == "__main__":
    data = SIIM_ACR_Pneumothorax_Segmentation(root=None, cache_dir=None, verbose=True)
    print(len(data.image_paths),data.image_paths[0],len(data))
    
