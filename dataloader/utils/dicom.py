import numpy as np
import pydicom
from pydicom.pixel_data_handlers.util import apply_voi_lut

def dicom_to_array(path, voi_lut=True, fix_monochrome=True, rgb=False):
    """Convert a DICOM chest xray to np.array.

    Raw dicom data is not actually linearly convertable to png/jpg.
    In fact, most of DICOM's store pixel values in exponential scale.
    This function applies the necessary transformations.


    Source
    ------
    Kaggle user: raddar
    https://www.kaggle.com/raddar/convert-dicom-to-np-array-the-correct-way
    """
    dicom = pydicom.read_file(path)

    # VOI LUT (if available by DICOM device) is used to transform raw DICOM data to "human-friendly" view
    if voi_lut:
        data = apply_voi_lut(dicom.pixel_array, dicom)
    else:
        data = dicom.pixel_array

    # depending on this value, X-ray may look inverted - fix that:
    if fix_monochrome and dicom.PhotometricInterpretation == "MONOCHROME1":
        data = np.amax(data) - data

    data = data - np.min(data)
    data = data / np.max(data)
    data = (data * 255).astype(np.uint8)

    if rgb:
        data = convert_single_channel_to_three_channel(data)
    return data


def get_dicom_metadata(path):
    """Return the DICOM header metadata."""
    return pydicom.dcmread(path)


def convert_single_channel_to_three_channel(image):
    return np.stack([image, image, image]).transpose(1, 2, 0)
