        
import torch
from functools import reduce
import numpy as np
class SingleClassDataset(torch.utils.data.Dataset):
    
    def __init__(self,root=None,cache_dir="/space/grafr", transforms = None,validation = False,label = "Pneumothorax", dataset = 0):
        super().__init__()
        self.transforms = transforms
        self.validation = validation
        self.label = label
        if dataset == 0:
            from dataloader.chexpert import Chexpert
            self.data = Chexpert(root = root,cache_dir=cache_dir,version="small",verbose=False,validation = validation)
        elif dataset == 1:
            from dataloader.chestxray14 import ChestXray14
            assert(not validation)
            self.data = ChestXray14(root = root,cache_dir=cache_dir,verbose=False)
        elif dataset == 2:
            from dataloader.vinbigdata import VinBigData
            self.data = VinBigData(root = root, cache_dir = cache_dir)

        else:
            raise NotImplementedError
        self.data.load()
        if not self.data.labels is None:
            assert(label in self.data.labels)
            #self.data.df[self.data.df[label]==-1][label] = 1
            self.data.df[label] = self.data.df[label].apply(lambda label: 1 if label == -1 else label)
            self.pos = self.data.df[self.data.df[label]==1]
            self.neg = self.data.df[self.data.df[label]!=1]
        else:
            assert(False)
            self.data.df[label] = self.data.df["class_name"].apply(lambda text: 1 if  label in text else 0)
            self.pos = self.data.df[self.data.df[label]==1]

            self.neg = self.data.df[self.data.df[label]!=1]
            
        assert(len(self.pos)<=len(self.neg))

    def __str__ (self):
        return str(self.data.cache_dir)

    def getWeight(self,args):
        return self.data.getWeight(args)
    
    def __len__(self):
        if self.validation:
            return len(self.data)
        return 2*len(self.pos)
    def getSample(self,idx):
        if self.validation:
            sample = self.data.df.iloc[int(idx)]
            label = np.array(1) if sample[self.label] == 1 else np.array(0)
        elif idx %2 == 0:
            sample = self.pos.iloc[int(idx//2)]
            label = np.array(1)
        else:
            idx2 = np.random.randint(len(self.neg))
            #idx2 = idx
            sample = self.neg.iloc[idx2]
            label = np.array(0)   
        img = self.data.load_image(sample.Path)
        self.img = img
        img = self.transforms(img).float()
        label =  torch.from_numpy(label).float()
        return img, label, img, sample

    def __getitem__(self, idx):
        img, label, _, _ = self.getSample(idx)
        return img,label
    
class SingleClassMultiDataset(torch.utils.data.Dataset):
    
    def __init__(self,root=None,cache_dir="/space/grafr", transforms = None,label = "Pneumothorax"):
        super().__init__()
        self.datasets = [SingleClassDataset(root=root,cache_dir=cache_dir,transforms=transforms,label = label,dataset = i) for i in range(3)]
        self.lens = [len(ds) for ds in self.datasets]
        print([(str(self.datasets[i]),self.lens[i]) for i in range(len(self.lens))])
    def __len__(self):
        return reduce(lambda a, b: a + b,self.lens)
    def __getitem__(self, idx):
        for i in range(len(self.lens)):
            if idx < self.lens[i]:
                return self.datasets[i][idx]
            else:
                idx -= self.lens[i]
        raise IndexError(f"Index out of Bounds {idx} of {len(self)}")