from abc import ABC
from pathlib import Path
import subprocess
from PIL import Image

class Dataset(ABC):
    data_path: Path

    def __init__(self, root=None, cache_dir=None, verbose=True):
        if root is None:
            root = "/mnt/cephstorage/share-all"

        self.root = Path(root)
        if not self.root.is_dir():
            print(root)
            raise FileNotFoundError

        if cache_dir is not None:
            cache_dir = Path(cache_dir).expanduser()
            if cache_dir.is_dir():
                self.cache_dir = cache_dir
            else:
                raise FileNotFoundError(f"{cache_dir} does not exist.")
        else:
            self.cache_dir = None
        self.verbose = verbose

    def load_image(self, path):
        """Load an image given path. PNG is assumed"""
        path = self._cache(path)
        return Image.open(path).convert("RGB") 

    def getItem(self,index):
        raise NotImplementedError

    def __len__(self) -> int:
        raise NotImplementedError
    def _cache(self, source):
        """Cache the source file if a cache directory is available.

        Parameters
        ----------
        source : str, Path
            Source of the file.
        """

        if self.cache_dir is not None:
            source = Path(source)
            destination = self.cache_dir / source.relative_to(self.root)
            if destination.exists():
                return destination

            command = f"mkdir -p {str(destination.parent)}"
            subprocess.run(command.split())
            command = f"rsync -avhW --progress {str(source)} {str(destination)}"

            if self.verbose:
                print(f"Caching {source} in {self.cache_dir}..")
                print(subprocess.check_output(command.split()))
                print(f"Copied {source} to {destination}.")
            else:
                subprocess.run(command.split(), stdout=subprocess.DEVNULL)
            return destination
        else:
            return source


from torch.utils.data import Dataset as TorchDataset
class GeneralDataset(Dataset):
    #Default data set for one to one predectoion.
    #Works with legacy data.
    def __init__(self, source : Dataset,args=None):
        self.source = source
        self.ignor = 0
    def __getitem__(self, index):
        A,y = self.source.getItem(index)
        return A,y
    def __len__(self):
        return len(self.source)
    