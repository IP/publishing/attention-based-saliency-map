from tkinter import *
from PIL import ImageTk, Image
import salience.gradcam.attention_hook as ah
import torch
from pytorch_grad_cam.utils.image import show_cam_on_image, deprocess_image, preprocess_image
import os
import cv2
import numpy as np
from torchvision import transforms as T

from dataloader.siim_acr_pneumothorax_segmentation import SIIMDatasetSingle

from training import Loader
import argparse
if __name__ == "__main__":
    ####NETWORK#####
    parser = argparse.ArgumentParser()
    Loader.parserNN(parser)
    Loader.parseArgmentation(parser)
    parser.add_argument("-idx", "--index", type=int,
                        default=1, help="index")
    args = Loader.parserDefaults(parser)
    args.batch_size = 1

    # Make models
    model = Loader.getNN(args)
    # To Cuda
    Loader.toCuda(args, nets=[model], shape=model.input_shape)
    # load checkpoint
    if not args.new:
        Loader.load(args, model)
    #(model,target_layers,image_size,patches) = ah.get_pretrained_vit(cuda=False)

    (model, target_layers, image_size, patches) = ah.getHocks(args, model)
    model.eval()

    #### DATASET #####
    transforms, transforms_val = Loader.getArgmentation(args)
    # SIIMDatasetSingle
    input_tensor, rgb_img = ah.getExampleImage(
        image_size, cuda=False, version=0)
    print("root=", args.dataset_path, "cache_dir", args.cache_dir,)
    dataset = SIIMDatasetSingle(
        root=args.dataset_path, cache_dir=args.cache_dir, transforms=transforms,)
    print("Dataset:", "SIIMDatasetSingle", "Len:", len(dataset))

    def load_image(idx):
        global rgb_img, rgb_img_mask, input_tensor
        img, mask, masked_img, label = dataset.getSample(idx)
        input_tensor = dataset[idx][0].unsqueeze(0)
        if not args.cpu:
            input_tensor = input_tensor.cuda()
        rgb_img = img
        rgb_img_mask = masked_img

        s = torch.nn.Sigmoid()  # torch.nn.Softmax(dim=1)
        with torch.no_grad():
            out = s(model(input_tensor))
        global attention
        attention = model.getAttention()
        print(len(attention), [
              attention[i].shape for i in range(len(attention))])
        # for i in range(len(attention)):
        #    (img1,img2) = ah.pixelAttetion(model, i,0)
    load_image(args.index)
    ##########

    image_size = model.image_size
    image_size_small = model.image_size  # //2
    if patches == 24:
        image_size_small = image_size_small//2
    root = Tk()
    panel = Canvas(root, width=image_size+5, height=image_size+5)
    print(rgb_img_mask)
    rgb_img_small = cv2.resize(
        1-np.array(rgb_img), (image_size_small, image_size_small))
    img_inital_small = ImageTk.PhotoImage(Image.fromarray(
        (rgb_img_small.astype("float")*255).astype("uint8")), 'rgb')

    rgb_img = cv2.resize(1-np.array(rgb_img_mask), (image_size, image_size))
    img_inital = ImageTk.PhotoImage(Image.fromarray(
        (rgb_img.astype("float")*255).astype("uint8")), 'rgb')

    image_container = panel.create_image(
        1, 1, image=img_inital, anchor='nw', tags='image')
    # .pack(side="top", fill="none", expand="no")
    panel.grid(row=0, column=0, columnspan=2, rowspan=2, padx=1, pady=30)

    panel2 = Canvas(root, width=image_size_small+5, height=image_size+5)
    panel2.grid(row=0, column=3, columnspan=4, rowspan=3)

    panel3 = Canvas(root, width=image_size+5, height=image_size+5)
    panel3.grid(row=2, column=0, columnspan=2, rowspan=1, padx=0)
    text = Label(panel3, text="Hier könnte ihre Werbung \n\n")
    # .pack(side="bottom", fill="none", expand="no")
    text.grid(row=2, column=0, columnspan=1, rowspan=1, padx=70, pady=1)

    flip = False
    j = 0
    pixel_id = 0
    pixel_id_old = 1
    show_image = True
    force_update = 0
    head_id = 0
    typ_attention = 0

    def setLabel():
        text.configure(text='{}, {},\n Layer {} L:↑ O:↓; Head {} K:↑ I:↓; \nFlip F, Image B'.format(
            int(pixel_id % patches), int(pixel_id//patches), j, head_id))
    setLabel()

    class SubImage(Canvas):
        def __init__(self, parent, value, **kwargs):
            super().__init__(parent, **kwargs)
            parent.after(2, self.print_img)
            self.parent = parent
            self.image_container = self.create_image(
                1, 1, image=img_inital_small, anchor='nw', tags='image')
            self.update_frame = -1
            self.value = value

        def print_img(self):
            global force_update
            global j

            if self.update_frame != force_update:
                (img1, img2) = ah.pixelAttetion(model, j, pixel_id,
                                                head=self.value, image_size=image_size_small)
                if flip:
                    img1 = img2
                #img1 = img1.copy()
                img1_bw = img1.copy()
                img1 = np.concatenate(
                    (1-img1, 1-img1, np.ones_like(img1)), axis=2)
                img = (rgb_img_small.astype("float")*255).astype("uint8")
                if show_image:
                    img_show = (img*img1).astype("uint8")
                    img_show = ImageTk.PhotoImage(
                        Image.fromarray(img_show), 'rgb')
                else:
                    img_show = (np.concatenate((img1_bw, img1_bw, img1_bw), axis=2).astype(
                        "float")*255).astype("uint8")
                    img_show = ImageTk.PhotoImage(
                        Image.fromarray(img_show), 'grey')
                self.img_show = img_show
                self.itemconfig(image_container, image=img_show)
                self.update_frame = force_update
            self.after(4, self.print_img)
    force_mulimage_update = False
    ramen = 0

    class SubMatMulImage(Canvas):
        def __init__(self, parent, **kwargs):
            super().__init__(parent, **kwargs)
            parent.after(2, self.print_img)
            self.parent = parent
            self.image_container = self.create_image(
                1, 1, image=img_inital_small, anchor='nw', tags='image')
            self.update_frame = -1
            self.j = -1

        def print_img(self):
            global force_update
            global force_mulimage_update
            global j
            global ramen
            global attention
            if (j != self.j or force_mulimage_update):
                print("UPDATE")
                img1 = ah.attention_vis(
                    attention[:j+1], model.patches, ramen=ramen, image_size=image_size_small)
                #img = (rgb_img_small.astype("float")*255).astype("uint8")
                #img1 = np.concatenate((img1,img1,img1),axis=2)**2
                if show_image:
                    img_show = show_cam_on_image(
                        rgb_img_small.astype("float")/255, 1-img1)
                    img_show = ImageTk.PhotoImage(
                        Image.fromarray(img_show), 'rgb')
                else:
                    img_show = (np.concatenate((img1, img1, img1), axis=2).astype(
                        "float")*255).astype("uint8")
                    img_show = ImageTk.PhotoImage(
                        Image.fromarray(img_show), 'grey')
                self.img_show = img_show
                self.itemconfig(image_container, image=img_show)
                self.j = j
                self.update_frame = force_update
                force_mulimage_update = False
            self.after(4, self.print_img)
    subMatMulImage = SubMatMulImage(panel3)
    subMatMulImage.grid(row=2, column=1, columnspan=1,
                        rowspan=1, padx=1, pady=1)

    si_list = []
    for i in range(attention[0].shape[1]):
        si = SubImage(panel2, i, width=image_size_small,
                      height=image_size_small)
        si.grid(row=i//4, column=i %
                4, columnspan=1, rowspan=1, padx=1, pady=1)
        si_list.append(si)
    force_update_main = 0

    def task():
        global i
        global canvas
        global image_container
        global img_show
        global img1
        global img2
        global pixel_id_old
        global force_update
        global force_update_main
        if pixel_id != pixel_id_old or force_update != force_update_main:
            force_update += 1
            # if not flip:
            #    (img1,img2) = ah.pixelAttetion(model, j,pixel_id,head = head_id,image_size = image_size)
            #    img = (rgb_img.astype("float")*255).astype("uint8")
            #    if show_image:
            #        img_show = ImageTk.PhotoImage(Image.fromarray((img*img1).astype("uint8")),'rgb')
            #    else:
            #        img_show = ImageTk.PhotoImage(Image.fromarray((np.concatenate((img1,img1,img1),axis=2).astype("float")*255).astype("uint8")),'grey')
            #    panel.itemconfig(image_container,image=img_show)
            #
            #    pixel_id_old = pixel_id
            # else:
            #    (img1,img2) = ah.pixelAttetion(model, j,pixel_id,head = head_id,image_size = image_size)
            #    img = (rgb_img.astype("float")*255).astype("uint8")
            #    if show_image:
            #        img_show = ImageTk.PhotoImage(Image.fromarray((img*img2).astype("uint8")),'rgb')
            #    else:
            #        img_show = ImageTk.PhotoImage(Image.fromarray((np.concatenate((img2,img2,img2),axis=2).astype("float")*255).astype("uint8")),'grey')
            #    panel.itemconfig(image_container,image=img_show)

            pixel_id_old = pixel_id
            force_update_main = force_update

        #i = 1-i
        root.after(4, task)  # reschedule event in 2 seconds

    def motion(event):
        global pixel_id
        x, y = event.x, event.y
        lv_x, lv_y = panel.coords('image')
        x1 = max(min(x-lv_x, image_size-1), 0)
        x2 = max(min(y-lv_y, image_size-1), 0)
        pixel_id = int(x1//16 + x2//16*patches)
        setLabel()

    root.bind('<Motion>', motion)

    def key(event):
        global show_image
        global j
        global head_id
        global flip
        global force_update
        global force_mulimage_update
        global ramen
        global typ_attention
        global attention
        if event.char == 'b':
            show_image = not show_image

        elif event.char == 'l':
            j += 1
            if j >= len(attention):
                j = 0
        elif event.char == 'o':
            j -= 1
            if j < 0:
                j = len(attention)-1
        elif event.char == 'k':
            head_id += 1
            if head_id >= 12:
                head_id = 0
        elif event.char == 'i':
            head_id -= 1
            if head_id < 0:
                head_id = 11
        elif event.char == 'f':
            flip = not flip
            force_mulimage_update = True
        elif event.char == 'q':
            ramen += 1
            force_mulimage_update = True
        elif event.char == 'a':
            ramen -= 1
            if ramen <= 0:
                ramen = 0
            force_mulimage_update = True
        elif event.char == 'y':
            pass
            # typ_attention+=1
            # typ_attention%=4
            # if typ_attention == 0:
            ##    typ_attention = 3
            # else:
            ##    typ_attention = 0
            # print(typ_attention)
            #attention = model.getAttention(source_id = typ_attention)
            #print(len(attention), [attention[i].shape for i in range(len(attention))])
            #force_mulimage_update = True
        else:
            print("pressed", repr(event.char))
        setLabel()
        force_update += 1

    # def callback(event):
    #    print("clicked at", event.x, event.y)
    #root.bind("<Button-1>", callback)

    root.bind("<Key>", key)
    root.after(0, task)
    root.mainloop()
