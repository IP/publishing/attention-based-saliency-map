import torch
import torch.nn as nn
from dataloader.pt_SingleClassLoader import SingleClassDataset, SingleClassMultiDataset
from dataloader.chexpert import ChexpertDataset
from util.Logger import Logger
import os
from torch.autograd import Variable
import training.Loader as Loader
import random
import numpy as np
import time

# Single Target training


def save(args, model, epoch):
    torch.save(model.state_dict(), os.path.join(args.ckpt_path,
                                                args.exp_name,
                                                f'{model.name}.pth'))
    if epoch % 100 == 0:
        torch.save(model.state_dict(), os.path.join(args.ckpt_path,
                                                    args.exp_name,
                                                    f'{model.name}.pth'))


def train(args, model):
    Tensor = torch.cuda.FloatTensor if not args.cpu else torch.Tensor
    input_A = Tensor(*((args.batch_size,) + model.input_shape))
    output_A = Tensor(*((args.batch_size,) + model.output_shape))

    input_A_val = Tensor(*((1,) + model.input_shape))
    output_A_val = Tensor(*((1,) + model.output_shape))

    transforms, transforms_val = Loader.getArgmentation(args)
    # Loaders ##### random apply
    n_cpu = args.num_cpu

    def worker_init_fn(x):
        t = int(time.time() * 1000.0)
        t = ((t & 0xff000000) >> 24) + ((t & 0x00ff0000) >> 8) + \
            ((t & 0x0000ff00) << 8) + ((t & 0x000000ff) << 24)
        seed = 42 + x + t
        np.random.seed(seed)
        random.seed(seed)
        torch.manual_seed(seed)
        return
    trainset = SingleClassMultiDataset(
        root=args.dataset_path, cache_dir=args.cache_dir, transforms=transforms)  # original
    trainLoader = torch.utils.data.DataLoader(
        trainset, batch_size=args.batch_size, shuffle=True, num_workers=n_cpu, drop_last=True, worker_init_fn=worker_init_fn)
    valset = SingleClassDataset(root=args.dataset_path, cache_dir=args.cache_dir,
                                transforms=transforms_val, dataset=0,
                                validation=True)
    valset = torch.utils.data.DataLoader(
        valset, batch_size=1, shuffle=False, num_workers=n_cpu, drop_last=True, worker_init_fn=worker_init_fn)
    print("Dataset:", "SingleClassDataset", "Len:", len(trainLoader))
    print("Valset:", "SingleClassDataset", "Len:", len(valset))
    #### Optimicer ####
    if args.sgd:
        optimizer = torch.optim.SGD(
            model.parameters(), lr=args.lr, momentum=0.9)
    else:
        print('ADAM WARNING')
        optimizer = torch.optim.Adam(
            model.parameters(), lr=args.lr, betas=(0.9, 0.999))

    ###### LOSS #######
    criterion = torch.nn.BCELoss()
    ##### Logger ######
    train_len = len(trainLoader)
    val_len = len(valset)
    print(train_len)
    logger = Logger(1, args.num_epochs, train_len, val_len, args.batch_size,
                    prev=False, path=os.path.join(args.ckpt_path, args.exp_name, 'plot/'))
    logger.name_mapping = {}
    sig = nn.Sigmoid()
    torch.set_printoptions(precision=3, threshold=15,
                           linewidth=400, profile=None, sci_mode=False)
    ######### Training ########
    print('From Epoch', 1, ' to ', args.num_epochs)
    loss_dic = {}
    for epoch in range(1, args.num_epochs+1):
        model.train()
        try:
            for i, batch in enumerate(trainLoader):
                optimizer.zero_grad()
                input_A = Variable(input_A.copy_(batch[0]))

                output_A = Variable(output_A.copy_(
                    batch[1].unsqueeze(dim=1)))

                predction = sig(model(input_A)).reshape(output_A.shape)

                loss = criterion(predction, output_A)
                loss_dic['ALL'] = loss.data.cpu()
                loss.backward()
                optimizer.step()
                logger.log(loss_dic)
                if args.verbose:
                    print("\n", (predction), "\n", output_A+0.00001)

            model.eval()
            with torch.no_grad():
                for i, batch in enumerate(valset):
                    # optimizer.zero_grad()
                    input_A_val = Variable(input_A_val.copy_(batch[0]))
                    output_A_val = Variable(output_A_val.copy_(batch[1]))
                    predction = sig(model(input_A_val))

                    loss = criterion(predction, output_A_val)
                    loss_dic['ALL'] = loss.data.cpu()
                    logger.logVAL(loss_dic)
                    if args.verbose:
                        print("\n", sig(predction), "\n",
                              output_A_val+0.00001, "val")
                logger.logVAL(store=True)

        except Exception as e:
            save(args, model, -1)
            raise e
        save(args, model, epoch)
