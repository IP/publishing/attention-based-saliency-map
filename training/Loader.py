from typing import Tuple
import torch
import torch.nn as nn
import time
import json
import os



import numpy as np
from PIL import Image
import torchvision.transforms as T
from timm.data.constants import IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD


class deitAddon(nn.Module):
    def __init__(self, a, mode=1, out_shape=4, remove_last=False):
        super().__init__()
        if remove_last:
            head = a.head
            a.head = nn.Linear(head.in_features, out_shape)
            a.head_dist = a.head
            self.a = a
            self.b = nn.Identity()
        else:
            self.a = a
            self.b = nn.Linear(1000, out_shape)

        def forward(x):
            x, x_dist = a.forward_features(x)
            x = a.head(x)
            return x
        self.a.forward = forward
        self.output_shape = (out_shape,)
        self.mode = mode

    def forward(self, x):
        x = self.a(x)
        if isinstance(x, Tuple):
            x = x[0]
        x = self.b(x)
        if len(x.shape) == 1:
            x = x.unsqueeze(0)
        return x


def reloadSettings(file, dataset_path, args=None):
    if not os.path.exists(file):
        print(file, 'does not extist yet.')
        args.ckpt_path = 'output'
        if str(args.exp_name).startswith('.\output'):
            args.exp_name = args.exp_name[9:]
        file = os.path.join(args.ckpt_path, args.exp_name)
    print('relaod settings', file)
    assert(os.path.exists(file))
    if os.path.isdir(file):
        full_path = [os.path.join(file, x) for x in os.listdir(
            file) if '.txt' in x or '.json' in x]
        file = max(full_path, key=os.path.getctime)
        print("reload", file)
    assert(os.path.exists(file))
    args_new = type('reloaded_arguments', (), {})()
    with open(file, "r") as read_file:
        args_new.__dict__ = json.load(read_file)
    if not "cache_dir" in args_new.__dict__:
        args_new.cache_dir = None
    # add missing keys
    if not "ag384" in args_new.__dict__:
        args_new.ag384 = False
    if not "remove_last" in args_new.__dict__:
        args_new.remove_last = False
    if not "CoAtNetS" in args_new.__dict__:
        args_new.CoAtNetS = False
    if not "dropout" in args_new.__dict__:
        args_new.dropout = 0

    if not args is None:
        args_new.ckpt_path = args.ckpt_path
        args_new.exp_name = args.exp_name
        args_new.cpu = args.cpu
        args_new.train = args.train
        args_new.train2 = args.train2
        args_new.test = args.test
        args_new.pertubate = args.pertubate
        args_new.ehr = args.ehr
        args_new.example = args.example
        args_new.sensitivity_n = args.sensitivity_n
        args_new.sensitivity_n_pix = args.sensitivity_n_pix
        args_new.sanity = args.sanity

        args_new.new = False
        args_new.hint1 = args.hint1
        args_new.hint2 = args.hint2
        args_new.hint3 = args.hint3
        args_new.hint4 = args.hint4
        args_new.num_cpu = args.num_cpu
        
        print(args_new)
        if args_new.channel_hint == 1:
            args_new.channel_hint = args.channel_hint
        args_new.__dict__ = {**args.__dict__, **args_new.__dict__}
        args_new.dataset_path = dataset_path
        print("Reloaded Arguments ", args_new.__dict__)
    args_new.dataset_path = dataset_path
    return args_new


def parserDefaults(parser):
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--train", action="store_true")
    group.add_argument("--train2", action="store_true")
    group.add_argument("--test", action="store_true")
    group.add_argument("--pertubate", action="store_true")
    group.add_argument("--ehr", action="store_true")
    group.add_argument("--example", action="store_true")
    group.add_argument("--sanity", action="store_true")
    group.add_argument("--sensitivity_n", action="store_true")
    group.add_argument("--sensitivity_n_pix", action="store_true")

    parser.add_argument("-lr", "--lr", type=float,
                        default=0.001, help="Learning rate of the network")
    parser.add_argument("-bs", "--batch_size", type=int,
                        default=32, help="Batch size")
    parser.add_argument("-ne", "--num_epochs", type=int,
                        default=30, help="Number of epochs")
    parser.add_argument("-nec", "--num_cpu", type=int,
                        default=16, help="Number of epochs")

    parser.add_argument("-en", "--exp_name", type=str,
                        required=True, help="Experiment name")
    parser.add_argument("-cp", "--ckpt_path", type=str,
                        default="/", help="Path to the root checkpoint folder")
    parser.add_argument("-bc", "--base_ckpt", type=int,
                        default=-1, help="Specify an older version of the net")

    parser.add_argument("-cpu", "--cpu", action="store_true", default=False)
    parser.add_argument("-reload", "--reload",
                        action="store_true", default=False)
    parser.add_argument("-new", "--new", action="store_true", default=False)
    parser.add_argument("-sleep", "--sleep", type=int,
                        default=0, help="Sets some matix in datasets to 0")
    parser.add_argument("--verbose", action="store_true",
                        default=False, help="...")
    parser.add_argument("--sgd", action="store_true",
                        default=False, help="...")
    parser.add_argument("--cos", action="store_true",
                        default=False, help="...")

    parser.add_argument("-ch", "--channel_hint", type=int,
                        default=5, help="Number of epochs")
    parser.add_argument("-dropout", "--dropout", type=float,
                        default=0, help="dropout if implemented")
    parser.add_argument("-h1", "--hint1", type=int,
                        default=1, help="Selects the data set")
    parser.add_argument("-h2", "--hint2", type=int,
                        default=-1, help="Select class id")
    parser.add_argument("-h3", "--hint3", type=int,
                        default=1, help="hint")
    parser.add_argument("-h4", "--hint4", type=int,
                        default=1, help="Select maximum")

    if not os.path.isdir("/mnt/cephstorage/share-all"):
        ds_def = "D:/ma/"
    else:
        ds_def = "/mnt/cephstorage/share-all"

    parser.add_argument("-ds", "--dataset_path", type=str,
                        default=ds_def, help="Select maximum")
    if not os.path.isdir("/mnt/cephstorage/share-all"):
        cache_def = None
    else:
        cache_def = "/space/grafr"
    parser.add_argument("-cache", "--cache_dir", type=str,
                        default=cache_def, help="Select maximum")

    #parser.add_argument("-noRandom", "--noRandom", action="store_true", default=False,help="dont Random the flatfields")
    #parser.add_argument("-ld","--linear_decay",action="store_true", default=False, help="Linear Learingrate dekay")
    args = parser.parse_args()
    if args.sleep != 0:
        print("Sleep now for {} howers".format(args.sleep))
        time.sleep(args.sleep*3600)

    if not os.path.exists(args.ckpt_path) and not args.reload:
        os.makedirs(args.ckpt_path)
    if args.reload:
        args = reloadSettings(os.path.join(
            args.ckpt_path, args.exp_name),args.dataset_path, args=args)

    elif args.train or args.train2:
        t = time.localtime()
        timestamp = time.strftime('%b-%d', t)
        file = os.path.join(args.ckpt_path, args.exp_name,
                            'commandline_args'+timestamp+'.txt')
        if not os.path.exists(os.path.join(args.ckpt_path, args.exp_name)):
            os.makedirs(os.path.join(args.ckpt_path, args.exp_name))
        with open(file, 'w') as f:
            json.dump(args.__dict__, f, indent=2)
    return args


def get_random_deform_parameter():
    minpoints = 3
    maxpoints = 18
    points = np.random.randint(maxpoints-minpoints+1)+minpoints
    sigma = np.random.uniform()*(70/(points*4))+2
    points = int(points)
    return (sigma, points)

#import elasticdeform
#def deformed_np(X, sigma=None, points=None):
#    X = np.asarray(X)
#    # Deform
#    if sigma == None:
#        sigma, points = get_random_deform_parameter()
#
#    out = elasticdeform.deform_random_grid(np.pad(np.float32(X[..., 0]), pad_width=(
#        (5, 5), (5, 5)), mode='reflect'), sigma=sigma, points=points)
#    out = np.uint8(out[5:-5, 5:-5])
#    out[out < 0] = 0
#    out[out > 255] = 255
#    return Image.fromarray(np.stack((out, out, out), axis=2))


def padding(X):
    X = np.asarray(X)
    # if np.max(out) > 50:
    #    out = out/255
    out = np.pad(X[..., 0], pad_width=((5, 5), (5, 5)), mode='reflect')
    return Image.fromarray(np.stack((out, out, out), axis=2))


def parseArgmentation(parser):
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-ag224_1", "--ag224_1", action="store_true",
                       help="ellastic Transform + 330 Crop -> resize224 -> vflip")
    group.add_argument("-ag224_2", "--ag224_2", action="store_true",
                       help="330 Crop -> resize224 -> vflip")
    group.add_argument("-ag224_3", "--ag224_3", action="store_true",
                       help="affine -> resize224 -> vflip->imgNet")
    group.add_argument("-ag224_4", "--ag224_4",
                       action="store_true", help="resize224")
    group.add_argument("-ag224_5", "--ag224_5", action="store_true",
                       help="affine -> resize224 -> vflip->0.5")

    group.add_argument("-ag384", "--ag384", action="store_true",
                       help="affine -> resize384 -> vflip")

    group.add_argument("-ag_native_1", "--ag_native_1",
                       action="store_true", help="affine->vflip->native")
    parser.set_defaults(type='ag224_3')

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-no_weight", "--no_weight", action="store_true",
                       help="ellastic Transform + 330 Crop -> resize224 -> vflip")
    group.add_argument("-relativ", "--relativ", action="store_true",
                       help="ellastic Transform + 330 Crop -> resize224 -> vflip")
    group.add_argument("-inf_relativ", "--inf_relativ", action="store_true",
                       help="ellastic Transform + 330 Crop -> resize224 -> vflip")
    parser.set_defaults(type='relativ')


def getArgmentation(args):
    if args.ag224_1:
        print("This has been removed")
        assert(False)
        #transforms = T.Compose([
        #    deformed_np,
        #    T.RandomRotation(degrees=15),
        #    T.RandomCrop(330, padding=5),
        #    T.Resize((224, 224)),
        #    T.ToTensor(),
        #    T.RandomHorizontalFlip(),
        #    T.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
        #])
        #transforms_val = T.Compose([
        #    T.RandomCrop(330, padding=5),
        #    T.Resize((224, 224)),
        #    T.ToTensor(),
        #    T.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
        #])
        #return transforms, transforms_val

    if args.ag224_2:
        transforms = T.Compose([
            T.RandomRotation(degrees=15),
            T.RandomCrop(330, padding=5),
            T.Resize((224, 224)),
            T.ToTensor(),
            T.RandomHorizontalFlip(),
            T.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
        ])
        transforms_val = T.Compose([
            T.RandomCrop(330, padding=5),
            T.Resize((224, 224)),
            T.ToTensor(),
            T.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
        ])
        return transforms, transforms_val
    if args.ag224_3:
        transforms = T.Compose([
            T.RandomAffine(degrees=(-15, 15), translate=(0.05,
                           0.05), scale=(0.9, 1.05), fill=0),
            T.Resize((224, 224)),
            T.ToTensor(),
            T.RandomHorizontalFlip(),
            T.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
        ])
        transforms_val = T.Compose([
            T.Resize((224, 224)),
            T.ToTensor(),
            T.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
        ])
        return transforms, transforms_val
    if args.ag224_4:
        transforms = T.Compose([
            T.Resize((224, 224)),
            T.ToTensor(),
            T.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
        ])
        transforms_val = T.Compose([
            T.Resize((224, 224)),
            T.ToTensor(),
            T.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
        ])
        return transforms, transforms_val
    if args.ag224_5:
        transforms = T.Compose([
            T.RandomAffine(degrees=(-15, 15), translate=(0.05,
                           0.05), scale=(0.9, 1.05), fill=0),
            T.Resize((224, 224)),
            T.ToTensor(),
            T.RandomHorizontalFlip(),
            T.Normalize(0.5, 0.5),
        ])
        transforms_val = T.Compose([
            T.Resize((224, 224)),
            T.ToTensor(),
            T.Normalize(0.5, 0.5),
        ])
        return transforms, transforms_val
    if args.ag384:
        transforms = T.Compose([
            T.RandomAffine(degrees=(-15, 15), translate=(0.05,
                           0.05), scale=(0.9, 1.05), fill=0),
            T.Resize((384, 384)),
            T.ToTensor(),
            T.RandomHorizontalFlip(),
            T.Normalize(0.5, 0.5),
        ])
        transforms_val = T.Compose([
            T.Resize((384, 384)),
            T.ToTensor(),
            T.Normalize(0.5, 0.5),
        ])
        return transforms, transforms_val
    if args.ag_native_1:
        transforms = T.Compose([
            padding,
            T.RandomAffine(degrees=(-15, 15), translate=(0.05,
                           0.05), scale=(0.9, 1.05), fill=0),
            T.RandomCrop(336, padding=5),
            T.ToTensor(),
            T.RandomVerticalFlip(),
            T.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
        ])
        transforms_val = T.Compose([
            padding,
            T.RandomCrop(336, padding=5),
            T.ToTensor(),
            T.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD),
        ])
        return transforms, transforms_val


def parserNN(parser):
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-vit1", "--deit_tiny_distilled_patch16_224",
                       action="store_true", help="deit_tiny_distilled_patch16_224")
    group.add_argument("-vit3", "--deit_base_distilled_patch16_224",
                       action="store_true", help="deit_base_distilled_patch16_224")
    group.add_argument("-vit5", "--deit_base_distilled_patch16_384",
                       action="store_true", help="deit_base_distilled_patch16_384")

    group.add_argument("-dens121", "--densnet_121",
                       action="store_true", help="..")
    group.add_argument("-CoAtNetS", "--CoAtNetS",
                       action="store_true", help="CoAtNet")
    group.add_argument("-CoAtNet0", "--CoAtNet0",
                       action="store_true", help="CoAtNet")
    group.add_argument("-CoAtNet1", "--CoAtNet1",
                       action="store_true", help="CoAtNet")
    group.add_argument("-CoAtNet2", "--CoAtNet2",
                       action="store_true", help="CoAtNet")
    group.add_argument("-CoAtNet3", "--CoAtNet3",
                       action="store_true", help="CoAtNet")
    group.add_argument("-CoAtNet4", "--CoAtNet4",
                       action="store_true", help="CoAtNet")

    group.add_argument("-BoT", "--BottleneckTransformer",
                       action="store_true", help="BottleneckTransformer")

    group.add_argument("-ResNN12", "--ResNN12", action="store_true",
                       help="A costum ResNN with 12 Layers")

    group.add_argument("-NativeResNN12", "--NativeResNN12",
                       action="store_true", help="A costum ResNN with 12 Layers")

    parser.add_argument("--remove_last", "-rem_la",
                        action="store_true", default=False)


def isCoAt(args):
    return args.CoAtNetS or args.CoAtNet0 or args.CoAtNet1 or args.CoAtNet2 or args.CoAtNet3 or args.CoAtNet4


def isViT(args):
    return args.deit_tiny_distilled_patch16_224 or args.deit_base_distilled_patch16_224 or args.deit_base_distilled_patch16_384


def getNN(args):

    out_shape = args.channel_hint
    if args.train2:
        out_shape = 1
    if args.deit_tiny_distilled_patch16_224:
        net = torch.hub.load('facebookresearch/deit:main',
                             'deit_tiny_distilled_patch16_224', pretrained=True)
        if args.dropout != 0:
            for i in range(len(net.blocks)):
                net.blocks[i].attn.attn_drop.p = args.dropout
                net.blocks[i].attn.proj_drop.p = args.dropout

        net = deitAddon(net, out_shape=out_shape, remove_last=args.remove_last)
        net.input_shape = (3, 224, 224)
        net.name = "deit_16_224"
        net.num_patch = 14
        return net
    if args.deit_base_distilled_patch16_224:
        net = torch.hub.load('facebookresearch/deit:main',
                             'deit_base_distilled_patch16_224', pretrained=True)
        if args.dropout != 0:
            for i in range(len(net.blocks)):
                net.a.blocks[i].attn.attn_drop.p = args.dropout
                net.a.blocks[i].attn.proj_drop.p = args.dropout
        net = deitAddon(net, out_shape=out_shape, remove_last=args.remove_last)
        net.input_shape = (3, 224, 224)
        net.name = "deit_16_224_base"
        net.num_patch = 14
        return net
    elif args.deit_base_distilled_patch16_384:
        net = torch.hub.load('facebookresearch/deit:main',
                             'deit_base_distilled_patch16_384', pretrained=True)
        if args.dropout != 0:
            for i in range(len(net.blocks)):
                net.a.blocks[i].attn.attn_drop.p = args.dropout
                net.a.blocks[i].attn.proj_drop.p = args.dropout
        net = deitAddon(net, out_shape=out_shape, remove_last=args.remove_last)
        net.input_shape = (3, 384, 384)
        net.name = "deit_16_384_base"
        net.num_patch = 24
        return net
    elif args.densnet_121:
        from training.dens import DenseNet121
        net = DenseNet121(out_shape)
        net.input_shape = (3, 224, 224)
        net.output_shape = (out_shape,)
        net.name = "densnet_121"
        return net
    elif args.CoAtNetS:
        import training.CoAtNet as coat
        net = coat.coatnet_S(out_shape)
        #from training.CoAtNet import CoAtNet
        #net = CoAtNet(3,224,out_shape,l1=2,l2=2,l3=2,l4=2,out_chs=[32,64,128,128,128])
        net.input_shape = (3, 224, 224)
        net.output_shape = (out_shape,)
        net.name = "CoAtNet0"
        return net
    elif args.CoAtNet0:
        import training.CoAtNet as coat
        net = coat.coatnet_0(out_shape)

        #from training.CoAtNet import CoAtNet
        #net =CoAtNet(3,224,out_shape,l1=2,l2=3,l3=5,l4=2,out_chs=[64,96,192,384,768])
        net.input_shape = (3, 224, 224)
        net.output_shape = (out_shape,)
        net.name = "CoAtNet0"
        return net
    elif args.CoAtNet1:
        import training.CoAtNet as coat
        net = coat.coatnet_1(out_shape)

        #from training.CoAtNet import CoAtNet
        #net =CoAtNet(3,224,out_shape,l1=2,l2=6,l3=14,l4=2,out_chs=[64,96,192,384,768])
        net.input_shape = (3, 224, 224)
        net.output_shape = (out_shape,)
        net.name = "CoAtNet1"
        return net
    elif args.CoAtNet2:
        import training.CoAtNet as coat
        net = coat.coatnet_2(out_shape)
        #from training.CoAtNet import CoAtNet
        #net =CoAtNet(3,224,out_shape,l1=2,l2=6,l3=14,l4=2,out_chs=[128,128,256,512,1024])
        net.input_shape = (3, 224, 224)
        net.output_shape = (out_shape,)
        net.name = "CoAtNet2"
        return net
    elif args.CoAtNet3:
        import training.CoAtNet as coat
        net = coat.coatnet_3(out_shape)
        #from training.CoAtNet import CoAtNet
        #net =CoAtNet(3,224,out_shape,l1=2,l2=6,l3=14,l4=2,out_chs=[192,192,384,768,1536])
        net.input_shape = (3, 224, 224)
        net.output_shape = (out_shape,)
        net.name = "CoAtNet3"
        return net
    elif args.CoAtNet4:
        import training.CoAtNet as coat
        net = coat.coatnet_4(out_shape)
        #from training.CoAtNet import CoAtNet
        #net =CoAtNet(3,224,out_shape,l1=2,l2=12,l3=28,l4=2,out_chs=[192,192,384,768,1536])
        net.input_shape = (3, 224, 224)
        net.output_shape = (out_shape,)
        net.name = "CoAtNet4"
        return net
    elif args.ResNN12:
        assert(False)
        return net
    elif args.NativeResNN12:
        class xxx(nn.Module):
            def __init__(self, a, out_shape=4, remove_last=False):
                super().__init__()
                head = a.head
                self.b = torch.nn.ModuleList(
                    modules=[nn.Linear(head.in_features, 1) for _ in range(out_shape)])
                a.head = nn.Identity()
                self.a = a
                # if remove_last:
                #a.head = nn.Linear(head.in_features,out_shape)
                #a.head_dist = a.head
                #self.a = a
                #self.b = nn.Identity()
                self.output_shape = (out_shape,)

                def vit_forward(x):
                    if len(x.shape) == 5:
                        x = x.squeeze(0)
                    if len(x.shape) == 3:
                        x = x.unsqueeze(0)

                    B = x.shape[0]
                    x = a.patch_embed(x)

                    # stole cls_tokens impl from Phil Wang, thanks
                    cls_tokens = a.cls_token.expand(B, -1, -1)
                    x = torch.cat((cls_tokens, cls_tokens, x), dim=1)

                    x = x + a.pos_embed

                    # x.register_hook(a.save_inp_grad)

                    for blk in a.blocks:
                        x = blk(x)

                    x = a.norm(x)
                    #x = a.pool(x, dim=1, indices=torch.tensor(0, device=x.device))
                    #x = x.squeeze(1)
                    #x = a.head(x)
                    return x
                a.forward = vit_forward

            def forward(self, x):
                x = self.a(x)
                y = [self.b[i](x[:, i]) for i in range(len(self.b))]
                x = torch.cat(y, axis=1)
                if len(x.shape) == 3:
                    x = x.squeeze(0)
                return x

        net = torch.hub.load('facebookresearch/deit:main',
                             'deit_tiny_distilled_patch16_224', pretrained=True)
        for i in range(len(net.blocks)):
            net.a.blocks[i].attn.attn_drop.p = 0.2
            net.a.blocks[i].attn.proj_drop.p = 0.2

        net = deitAddon(net, out_shape=out_shape)
        net.input_shape = (3, 224, 224)
        net.name = "deit_16_224"
        net.num_patch = 14
        return net

        #from training.ResNN import ResNN
        #input_shape = (3,336, 336)
        #donws = {'2':2,'5':2,'8':2,'11':2}
        #featureL = [4,4,8,8,8,16,16,16,16,16,16,16]
        #featureL = [int(args.channel_hint*i) for i in featureL]
        #net = ResNN(input_shape,feature_list=featureL,downsampling=donws,num_classe=out_shape)
        #net.input_shape = input_shape
        #net.name = "ResNN12_" + str(args.channel_hint)
        # return net
    elif args.BottleneckTransformer:
        from torchvision.models import resnet50
        from bottleneck_transformer_pytorch import BottleStack
        layer = BottleStack(
            dim=256,
            fmap_size=56,        # set specifically for imagenet's 224 x 224
            dim_out=2048,
            proj_factor=4,
            downsample=True,
            heads=4,
            dim_head=128,
            rel_pos_emb=True,
            activation=nn.ReLU()
        )
        resnet = resnet50(pretrained=True)
        backbone = list(resnet.children())
        model = nn.Sequential(
            *backbone[:5],
            layer,
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(1),
            nn.Linear(2048, out_shape)
        )
        model.input_shape = (3, 224, 224)
        model.out_shape = (out_shape,)
        model.name = "bottleneck_transformer"
        return model
    else:
        print('No Network selected')
        exit('No Network selected')


def load(args, net):
    name = net.name
    if args.base_ckpt == None:
        print('No network loaded')
        return
    if args.base_ckpt != -1:
        name1 = os.path.join(args.ckpt_path, args.exp_name,
                             name+str(args.base_ckpt)+'.pth')
        name2 = os.path.join(args.ckpt_path, args.exp_name,
                             name+'_'+str(args.base_ckpt)+'.pth')
        print()
        if os.path.exists(name1) or os.path.exists(name2):
            print('reload base_ckpt Checkpoint for ', name, args.base_ckpt)
            try:
                net.load_state_dict(torch.load(os.path.join(name1)))
            except:
                net.load_state_dict(torch.load(os.path.join(name2)))

        else:
            print('Could not finde ', name1)
            print(name + str(args.base_ckpt) + '.pth - File not Found!')
            assert(False)
    elif os.path.exists(os.path.join(args.ckpt_path, args.exp_name, name+'.pth')):
        print('reload last Checkpoint for ', name)
        net.load_state_dict(torch.load(os.path.join(
            args.ckpt_path, args.exp_name, name+'.pth')))
    else:
        print('No Checkpoint found for ' +
              os.path.join(args.ckpt_path, args.exp_name, name+'.pth'))
        if args.test:
            print('No Checkpoint found for ' +
                  os.path.join(args.ckpt_path, args.exp_name, name+'.pth'))
            assert(False)


def toCuda(args, nets=[], shape=None):
    if not torch.cuda.is_available():
        print("WARNING: You have don't have a CUDA device")
    elif args.cpu:
        print("WARNING: You have a CUDA device, but don't use it.")
    else:
        for i in nets:
            if not i is None:
                i.cuda()
                # sif not shape is None and args.local:
                # s    print(i)
                # s    try:
                # s        from torchsummary import summary
                # s        summary(i, tuple(shape))
                # s    except:
                # s        pass
