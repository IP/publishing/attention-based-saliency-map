import torch
import torch.nn as nn
from dataloader.pt_MultiClassLoader import MultiClassMultiDataset
from util.Logger import Logger
import os
from torch.autograd import Variable
from training import Loader
import random
import numpy as np
import time


# Single Target training


def save(args, model, epoch):
    torch.save(model.state_dict(), os.path.join(
        args.ckpt_path, args.exp_name, f'{model.name}.pth'))
    #print(epoch,epoch-1 % 5,int(epoch-1) % 5 == 0)
    # if int(epoch-1) % 5 == 0 and epoch <= 100:
    #print('store number')
    torch.save(model.state_dict(), os.path.join(args.ckpt_path,
                                                args.exp_name,
                                                f'{model.name}_{epoch}.pth'))


def train(args, model):
    Tensor = torch.cuda.FloatTensor if not args.cpu else torch.Tensor
    input_A = Tensor(*((args.batch_size,) + model.input_shape))
    output_A = Tensor(*((args.batch_size,) + model.output_shape))

    input_A_val = Tensor(*((1,) + model.input_shape))
    output_A_val = Tensor(*((1,) + model.output_shape))

    transforms, transforms_val = Loader.getArgmentation(args)
    # Loaders ##### random apply
    n_cpu = args.num_cpu

    def worker_init_fn(x):
        t = int(time.time() * 1000.0)
        t = ((t & 0xff000000) >> 24) + ((t & 0x00ff0000) >> 8) + \
            ((t & 0x0000ff00) << 8) + ((t & 0x000000ff) << 24)
        seed = 42 + x + t
        np.random.seed(seed)
        random.seed(seed)
        torch.manual_seed(seed)
        return
    labels = ["Pneumothorax", "Pleural Effusion",
              "Cardiomegaly", "Atelectasis", "Consolidation"]

    labels = labels[:args.channel_hint]

    print("root =", args.dataset_path, "cache_dir =", args.cache_dir,)
    trainset = MultiClassMultiDataset(root=args.dataset_path, cache_dir=args.cache_dir,
                                      transforms=transforms, labels=labels)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True,
                                              num_workers=n_cpu, drop_last=True,
                                              worker_init_fn=worker_init_fn)
    valset = MultiClassMultiDataset(root=args.dataset_path, cache_dir=args.cache_dir,
                                    transforms=transforms, labels=labels, validation=True)
    valset = torch.utils.data.DataLoader(valset, batch_size=args.batch_size, shuffle=False,
                                         num_workers=n_cpu, drop_last=False,
                                         worker_init_fn=worker_init_fn)
    print("Dataset:", "SingleClassDataset", "Len:", len(trainloader))
    print("Valset:", "SingleClassDataset", "Len:", len(valset))
    #### Optimicer ####
    if args.sgd:
        optimizer = torch.optim.SGD(
            model.parameters(), lr=args.lr, momentum=0.9)
    else:
        print('ADAM WARNING')
        optimizer = torch.optim.AdamW(
            model.parameters(), lr=args.lr, betas=(0.9, 0.999))

    ###### LOSS #######
    criterion = torch.nn.BCELoss()
    ##### Logger ######
    train_len = len(trainloader)
    val_len = len(valset)
    print(train_len, val_len)
    logger = Logger(1, args.num_epochs, train_len, path=os.path.join(
        args.ckpt_path, args.exp_name, 'plot/'))
    sig = nn.Sigmoid()
    torch.set_printoptions(precision=3, threshold=15,
                           linewidth=400, profile=None, sci_mode=False)
    ######### Training ########
    print('From Epoch', 1, ' to ', args.num_epochs, 'Train labels', labels)

    loss_dic = {}
    for epoch in range(1, args.num_epochs+1):
        print()
        try:
            model.train()
            for j, batch in enumerate(trainloader):

                optimizer.zero_grad()
                input_A = Variable(input_A.copy_(batch[0]))

                output_A = Variable(output_A.copy_(
                    batch[1].reshape(output_A.shape)))

                predction = sig(model(input_A)).reshape(output_A.shape)
                loss_dic['ALL'] = 0
                loss = 0
                for i, label in enumerate(labels):
                    loss_label = criterion(predction[..., i],
                                           output_A[..., i])
                    loss_dic[label] = loss_label.data.cpu().item()
                    loss += loss_label
                loss_dic['ALL'] = loss.data.cpu().item()
                loss.backward()
                optimizer.step()
                logger.log(loss_dic, False, epoch)
                if args.verbose:
                    print("\n", (predction), "\n", output_A+0.00001)
            print()
            with torch.no_grad():
                model.eval()
                # print('Val')
                loss_dic = dict.fromkeys(loss_dic, 0)
                for j, batch in enumerate(valset):
                    input_A_val = Variable((batch[0]).cuda())
                    output_A_val = Variable((batch[1]).cuda())
                    predction = sig(model(input_A_val))

                    loss_dic['ALL'] = 0
                    loss = 0
                    for i, label in enumerate(labels):

                        # print(predction.shape,output_A_val.shape)
                        loss_label = criterion(predction[..., i],
                                               output_A_val[..., i])
                        loss_dic[label] = loss_label.data.cpu().item()
                        loss += loss_label
                    loss_dic['ALL'] = loss.data.cpu().item()
                    logger.log(loss_dic, True, epoch)
                    if args.verbose:
                        print("\n", sig(predction), "\n",
                              output_A_val+0.00001, "val")

        except Exception as e:
            save(args, model, epoch)

            raise e
        save(args, model, epoch)
