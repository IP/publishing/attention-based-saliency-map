# Attention-Based Saliency Map Improve Pneumothorax Classification Interpretability
This repository includes the official implementation of "Attention-Based Saliency Map Improve Pneumothorax Classification Interpretability" by Alessandro Wollek, Robert Graf, Saša Čečatka, Nicola Fink, Theresa Willem, Bastian Sabel, and Tobias Las.

## Description

This repository contains the training for Vision Transformer (ViT) and evaluation methods for Attention Based Saliency Map Methods TMME, LRP+TA, TAM Marcove and compares them with GradCAM.

Please refere to the original repositories and papers for more information.
- TMME (Transformer-Multimodal-Explainability) [Paper](https://arxiv.org/abs/2103.15679)[Repo](https://github.com/hila-chefer/Transformer-MM-Explainability)
- LRP+TA (Layer-wise relevance propagation + Transformer-Attribution)[Paper](https://arxiv.org/abs/2012.09838)[Repo](https://github.com/hila-chefer/Transformer-Explainability)
- TAM Markov (Transition Attention Maps Markov) [Paper](https://openreview.net/forum?id=TT-cf6QSDaQ) [Repo](https://github.com/PaddlePaddle/InterpretDL)

The following test were implemented
- Chest X-Ray Classification: ROC / AUC
- Saliency Map: 
	- positive and negative pertubation
	- effective heat ratio
	- sensitivity_n 
	- sanity checks (single layer, last n layers and others)



## Visualization
There are two files for visualizing the attention-heads: `attetion_vis.py` for ImageNet and `attetion_vis2.py` for ViTs trainded on chest X-rays.
### Saliency Map Example
![image info](salience/gradcam/example_explaination.jpg)
### Attention Visualization
![image info](salience/gradcam/inspector.jpg)

## Installation

### Python
```posh
conda create -n saliency numpy scikit-image matplotlib timm scipy pandas einops pydicom scikit-learn torchvision torchaudio cudatoolkit=11.3 -c pytorch -c anaconda -c conda-forge
conda activate saliency
pip install grad-cam 
```
### Data sets

This repository used five public data sets: 
- Chest X-ray14 (X. Wang et al. 2017)
- Chexpert (Irvin et al. 2019)
- MIMIC-CXR-JPG (Johnson et al. 2019)
- VinBigData Chest Xray Abnormalities Detection Kaggle (2020) 
- SIIMACR Pneumothorax Segmentation Kaggle (2020) 

They are not provided and must be downloaded and installed separately. Store the data sets in the same folder. You provide the folder where "chestxray-data" is located. The following structure is required:

```
chestxray-data
├── chestxray14
│   ├── compressed_images
│   ├── Data_Entry_2017_v2020.csv
│   ├── train_val_list.txt
│   ├── test_list.txt
│   ├── BBox_List_2017.csv
│   └── images
│       └── *.png
├── chexpert
│   ├── CheXpert-v1.0         (optional)
│   └── CheXpert-v1.0-small
│       ├── train.csv
│       ├── valid.csv
│       ├── train
│       │   └── patient*
│       │       └──study*
│       │          └──*.jpg
│       └── valid
│           └── patient*
│               └──study*
│                  └──*.jpg
├── mimic-cxr
│   └── dataset_512
│       ├── mimic-cxr-2.0.0-chexpert.csv
│       ├── mimic-cxr-2.0.0-split.csv
│       ├── test.csv   (will be created automatically, this will take a while)
│       ├── train.csv  (will be created automatically, this will take a while)
│       └── files
│           └── p1*[0-9]
│               └── p*
│                   └── s*
│                       └── *.png
├── siim-acr-pneumothorax-segmentation-data
│   └── stage-2
│       └── siim
│           ├── train-rle.csv
│           └── dicom-images-train
│               └── *.dcm
└── vinbigdata
    ├── original           (optional)
    ├── vinbigdata-256     (optional)
    └── vinbigdata-512
        ├── train.csv
        ├── train
        ├   └── *.png
        ├── train_transformed1.csv (will be created automatically)
        ├── train_transformed2.csv (will be created automatically)
        ├── test.csv
        └── test
            └── *.png
```
If you want to use different data sets or data set setups you have to edit the dataloader/pt_MultiClassLoader.py file. 
 

## Usage

### Training

Python 3 is requiered.

```posh 
#Vit1
python train.py --train -en vit1  -cp output/  -vit1        -lr 0.001 -bs 64 -ne 500  -ag224_3 --sgd --new -ds [Your path to the datasets] 
#Vit3
python train.py --train -en vit3  -cp output/  -vit3        -lr 0.001 -bs 64 -ne 500  -ag224_3 --sgd --new -ds [Your path to the datasets] 
#DenseNet
python train.py --train -en dens5 -cp output/ --densnet_121 -lr 0.001 -bs 32 -ne 30   -ag224_3 --sgd --new -ds [Your path to the datasets] 
```

| Command          | Typ        |Example | Explanation                    |
|------------------|------------|--------|---------------------------------|
| -en --exp_name   | Foldername |    | Experiment name                 |
| [-vit1 \| -vit3 \| -vit5 \| -dens121 \|  -CoAtNetS \| -CoAtNet0 \| -CoAtNet1 \|  -CoAtNet2 \| -CoAtNet3 \| -CoAtNet4 ] | Pick One   | -vitX: Vision Transformer (1 = 224x224; 3 = 224x224 destilled; 5 384x384); -dens121: DensNet; -CoAtNetX: CoAtNet | Select the Networktype          |
| -lr --lr         | float      | recommended 0.001 (0.01 - 0.0001)    | Learning Rate                   |
| -bs --batch_size | int        | recommended 32/64                    | Batch size                      |
| -ne --num_epochs | int        |                                      | Number of epochs                |
| -cp --chpt_path  | path       | Example: /users/training/            | Checkpoint Paths                |
| -cpu --cpu       |            |                                      | Don't use GPU (unstable)        |
| --sgd            |            | recommended for ViT;                 | Use SGD instead of ADAM         |
| -ch --channel_hint| [1,5]     | 5 uses all avilable classes          | Only train on the n classes.    |
| [-ag224_1 \| -ag224_2 \| -ag224_3 \|-ag224_4 \| -ag224_5 \| -ag384 \|   -ag_native_1]          |            | recommended -ag224_3 or -ag384 for ViT5      | Select the image Aggumentation. |
| -ds --dataset_path  | path       | | Path to the dataset                |
| -cache --cache_dir  | path       | | Copys the data set in this folder. Use this if the dataset is in a slow storage.                |
### Run a Tests

```posh 
#ROC/PRC
python train.py --test           -en vit1  -cp output/  --reload -h1 2 -h2 0 -ds [Your path to the datasets] 
#Pertubation test
python train.py --pertubate      -en vit1  -cp output/  --reload -h1 2 -h2 1 -h3 250 -ds [Your path to the datasets] 
#EHR test
python train.py --ehr            -en vit1  -cp output/  --reload -h1 2 -h2 0 -h3 1000 -ds [Your path to the datasets] 
#sensitivity_n test
python train.py --sensitivity_n  -en vit1  -cp output/  --reload -h1 2 -h2 1 -h3 250 -h4 200 -ds [Your path to the datasets] 
#sanity test
python train.py --sanity         -en vit1  -cp output/  --reload -h1 2 -h2 1 -h3 1000 -ds [Your path to the datasets] 
```

| Command          | Explanation                  |
|------------------|------------------------------|
|-h1               | what data set                       |
|-h2               | what class is tested                |
|-h3               | number of max sample                |
|-h4               | number of masks (sensitivity_n only)|

|id|name        |Classes ID|Segmentation|
|--|------------|-------|------------|
|1 |'siim'      |0      | yes               |
|2 |'Chexpert'  |[0-4]  | no                |
|3 |'VinBigData'|[0-4]  | yes               |
|4 |'mimic_vals'|[0-4]  | yes (seg is small)|

|id|Class       |
|--|------------|
|0|Pneumothorax    |        
|1|Pleural Effusion|       
|2|Cardiomegaly    |        
|3|Atelectasis     |    
|4|Consolidation   |         

### ViTs Inspection

```posh 
#ViTs pretrained on ImageNet
python attetion_vis.py -vit 5 -img salience/gradcam/look.png
#ViTs trained on ChestXRay
python attetion_vis2.py -en vit3 -cp output/ --reload -idx 2 -ds [Your path to the datasets] 
```

## License
Apache License 2.0

