from pytorch_grad_cam.utils.image import preprocess_image
from salience.mincut.minFlow import findFlow
from tkinter import *
from PIL import ImageTk, Image
import salience.gradcam.attention_hook as ah
import torch


import cv2
import numpy as np
from torchvision import transforms as T

import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-vit", "--vit_index", type=int,
                    default=4, help="index")

parser.add_argument("-img", "--image_path", type=str,
                    default="salience/gradcam/look.png", help="image_path")
args = parser.parse_args()
# NETWORK
(model, target_layers, image_size, patches) = ah.get_pretrained_vit(
    cuda=False, version=args.vit_index-1)
model.eval()


#input_tensor, rgb_img = ah.getExampleImage(image_size, cuda=False, version=5)
rgb_img = cv2.imread(args.image_path, 1)[:, :, ::-1]
rgb_img = cv2.resize(rgb_img, (image_size, image_size))
rgb_img = np.float32(rgb_img) / 255
input_tensor = preprocess_image(
    rgb_img, mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
# if cuda:
#    input_tensor = input_tensor.cuda()
input_tensor_org = input_tensor


s = torch.nn.Softmax(dim=1)
last_att = []
with torch.no_grad():
    out = s(model(input_tensor))

attention = model.getAttention()
flow = findFlow(attention)
for i in range(len(attention)):
    (img1, img2) = ah.pixelAttetion(model, i, 0)
pli2tensor = T.ToTensor()

##########


image_size = model.image_size
image_size_small = model.image_size//2


root = Tk()
panel = Canvas(root, width=image_size+5, height=image_size+5)
img_inital = ImageTk.PhotoImage(Image.fromarray(
    (rgb_img.astype("float")*255).astype("uint8")), 'rgb')
rgb_img_small = cv2.resize(rgb_img, (image_size_small, image_size_small))
img_inital_small = ImageTk.PhotoImage(Image.fromarray(
    (rgb_img_small.astype("float")*255).astype("uint8")), 'rgb')


image_container = panel.create_image(
    1, 1, image=img_inital, anchor='nw', tags='image')
# .pack(side="top", fill="none", expand="no")
panel.grid(row=0, column=0, columnspan=2, rowspan=2, padx=1, pady=30)

panel2 = Canvas(root, width=image_size_small+5, height=image_size+5)
panel2.grid(row=0, column=3, columnspan=4, rowspan=3)

panel3 = Canvas(root, width=image_size+5, height=image_size+5)
panel3.grid(row=2, column=0, columnspan=2, rowspan=1, padx=0)
text = Label(panel3, text="Hier könnte ihre Werbung \n\n")
# .pack(side="bottom", fill="none", expand="no")
text.grid(row=2, column=0, columnspan=1, rowspan=1, padx=70, pady=1)

flip = False
j = 0
pixel_id = 0
pixel_id_old = 1
show_image = True
force_update = 0
head_id = 0


def setLabel():
    text.configure(text='{}, {},\n Layer {} L:↑ O:↓; \nFlip F, Image B'.format(
        int(pixel_id % patches), int(pixel_id//patches), j, head_id))


    # Head {} K:↑ I:↓;
setLabel()


class SubImage(Canvas):
    def __init__(self, parent, value, **kwargs):
        super().__init__(parent, **kwargs)
        parent.after(2, self.print_img)
        self.parent = parent
        self.image_container = self.create_image(
            1, 1, image=img_inital_small, anchor='nw', tags='image')
        self.update_frame = -1
        self.value = value

    def print_img(self):
        global force_update
        global j

        if self.update_frame != force_update:
            (img1, img2) = ah.pixelAttetion(model, j, pixel_id,
                                            head=self.value, image_size=image_size_small)
            if flip:
                img1 = img2
            img = (rgb_img_small.astype("float")*255).astype("uint8")
            if show_image:
                img_show = (img*img1).astype("uint8")
                img_show = ImageTk.PhotoImage(Image.fromarray(img_show), 'rgb')
            else:
                img_show = (np.concatenate((img1, img1, img1), axis=2).astype(
                    "float")*255).astype("uint8")
                img_show = ImageTk.PhotoImage(
                    Image.fromarray(img_show), 'grey')
            self.img_show = img_show
            self.itemconfig(image_container, image=img_show)
            self.update_frame = force_update
        self.after(4, self.print_img)


force_mulimage_update = False
ramen = 0


class SubMatMulImage(Canvas):
    def __init__(self, parent, **kwargs):
        super().__init__(parent, **kwargs)
        parent.after(2, self.print_img)
        self.parent = parent
        self.image_container = self.create_image(
            1, 1, image=img_inital_small, anchor='nw', tags='image')
        self.update_frame = -1
        self.j = -1

    def print_img(self):
        global force_update
        global force_mulimage_update
        global j
        global ramen
        # (j != self.j or force_mulimage_update):
        if self.update_frame != force_update:
            pass
            # print("UPDATE")
            #img1 = ah.attention_vis(attention[:j+1],model.patches,ramen=ramen,image_size = image_size_small)
            # print(flow[j].shape)
            img3 = np.sum(flow, axis=0)[pixel_id]
            w = model.patches
            # print(img3)
            num_cls_token = img3.shape[0] - w*w
            img3 = img3[num_cls_token:].reshape((w, w))
            # print(np.sum(img3),img3.max())
            if img3.max() != 0:
                img3 = img3/img3.max()
            img3 = cv2.resize((img3), (image_size_small, image_size_small),
                              interpolation=cv2.INTER_NEAREST)[..., np.newaxis]
            img3 = np.concatenate((img3, img3, img3), axis=-1)

            img = (rgb_img_small.astype("float")*255).astype("uint8")
            if show_image:
                img_show = (img3.astype("float")*255).astype("uint8")
                #img_show = show_cam_on_image(rgb_img_small.astype("float"), 1-img1)
                img_show = ImageTk.PhotoImage(Image.fromarray(img_show), 'rgb')
            else:
                img_show = (np.concatenate((img1, img1, img1), axis=2).astype(
                    "float")*255).astype("uint8")
                img_show = ImageTk.PhotoImage(
                    Image.fromarray(img_show), 'grey')
            self.img_show = img_show
            self.itemconfig(image_container, image=img_show)
            self.j = j
            self.update_frame = force_update
            force_mulimage_update = False
        self.after(4, self.print_img)


subMatMulImage = SubMatMulImage(panel3)
subMatMulImage.grid(row=2, column=1, columnspan=1, rowspan=1, padx=1, pady=1)

si_list = []
for i in range(12):
    si = SubImage(panel2, i, width=image_size_small, height=image_size_small)
    si.grid(row=i//4, column=i % 4, columnspan=1, rowspan=1, padx=1, pady=1)
    si_list.append(si)
force_update_main = 0


def task():
    global i
    global canvas
    global image_container
    global img_show
    global img1
    global img2
    global pixel_id_old
    global force_update
    global force_update_main
    if pixel_id != pixel_id_old or force_update != force_update_main:
        force_update += 1
        # if not flip:
        #    (img1,img2) = ah.pixelAttetion(model, j,pixel_id,head = head_id,image_size = image_size)
        #    img = (rgb_img.astype("float")*255).astype("uint8")
        #    if show_image:
        #        img_show = ImageTk.PhotoImage(Image.fromarray((img*img1).astype("uint8")),'rgb')
        #    else:
        #        img_show = ImageTk.PhotoImage(Image.fromarray((np.concatenate((img1,img1,img1),axis=2).astype("float")*255).astype("uint8")),'grey')
        #    panel.itemconfig(image_container,image=img_show)
        #
        #    pixel_id_old = pixel_id
        # else:
        #    (img1,img2) = ah.pixelAttetion(model, j,pixel_id,head = head_id,image_size = image_size)
        #    img = (rgb_img.astype("float")*255).astype("uint8")
        #    if show_image:
        #        img_show = ImageTk.PhotoImage(Image.fromarray((img*img2).astype("uint8")),'rgb')
        #    else:
        #        img_show = ImageTk.PhotoImage(Image.fromarray((np.concatenate((img2,img2,img2),axis=2).astype("float")*255).astype("uint8")),'grey')
        #    panel.itemconfig(image_container,image=img_show)

        pixel_id_old = pixel_id
        force_update_main = force_update

    #i = 1-i
    root.after(4, task)  # reschedule event in 2 seconds


def motion(event):
    global pixel_id
    x, y = event.x, event.y
    lv_x, lv_y = panel.coords('image')
    x1 = max(min(x-lv_x, image_size-1), 0)
    x2 = max(min(y-lv_y, image_size-1), 0)

    pixel_id = int(x1//16 + x2//16*model.patches)
    setLabel()


root.bind('<Motion>', motion)


def key(event):
    global show_image
    global j
    global head_id
    global flip
    global force_update
    global force_mulimage_update
    global ramen
    if event.char == 'b':
        show_image = not show_image

    elif event.char == 'l':
        j += 1
        if j >= len(attention):
            j = 0
    elif event.char == 'o':
        j -= 1
        if j < 0:
            j = len(attention)-1
    elif event.char == 'k':
        head_id += 1
        if head_id >= 12:
            head_id = 0
    elif event.char == 'i':
        head_id -= 1
        if head_id < 0:
            head_id = 11
    elif event.char == 'f':
        flip = not flip
        force_mulimage_update = True
    elif event.char == 'q':
        ramen += 1
        force_mulimage_update = True
    elif event.char == 'a':
        ramen -= 1
        if ramen <= 0:
            ramen = 0
        force_mulimage_update = True
    else:
        print("pressed", repr(event.char))
    setLabel()
    force_update += 1


# def callback(event):
#    print("clicked at", event.x, event.y)
#root.bind("<Button-1>", callback)
root.bind("<Key>", key)
root.after(0, task)
root.mainloop()
