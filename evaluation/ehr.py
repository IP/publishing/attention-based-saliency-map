import json
#from unicodedata import name

import numpy as np
import matplotlib.pyplot as plt
import os
import torch.nn as nn
from salience.Cam_Collection import CamCollection
from tqdm import tqdm
import evaluation.roc as roc
pool = nn.MaxPool2d(16)
cuda = True


def ehr(input_tensor, mask, camCollection: CamCollection, buffer: dict, steps=100):

    #out_img = np.asarray(input_tensor.detach().cpu())
    images, names, cams, cams_org = camCollection.execut_all_cam(
        input_tensor, None, cuda, return_cam=True)
    # print(names)
    if buffer is None:
        buffer = {}
        for name in names:
            buffer[name] = []
            buffer[name+'_th'] = []
    for name, org_cam in zip(names, cams_org):
        maxv = org_cam.max()
        if maxv == org_cam.min():
            maxv += 1

        values = np.arange(org_cam.min(), maxv,
                           (maxv-org_cam.min())/steps)
        threshoulds = [i for i in values][:steps]
        assert(mask.max() != 0)
        pol_mask = pool(mask)/mask.max()
        condi_cam = org_cam*pol_mask[0].cpu().numpy()
        vol_cam = [(org_cam > i).sum() for i in threshoulds]
        vol_condi_cam = [(condi_cam > i).sum() for i in threshoulds]

        ehr = np.array(vol_condi_cam)/np.array(vol_cam)
        ehr = np.nan_to_num(ehr)
        percent = np.array(vol_cam)/org_cam.reshape(-1).shape
        buffer[name].append(ehr)
        buffer[name+'_th'].append(percent)
    return buffer


sig = nn.Sigmoid()


def compute_ehr(args, model):
    global cuda
    cuda = not args.cpu
    ### Folder Stuff ###
    _, class_index = roc.get_roc_name(args, model)

    ehr_name = f"ehr_{roc.get_name(args)}_n{args.hint3}"
    ehr_name_load = f"ehr_{roc.get_name(args)}_n{args.hint4}"

    out_folder = os.path.join(args.ckpt_path, args.exp_name, 'ehr')
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)

    f = os.path.join(out_folder, ehr_name+'.json')
    f_load = os.path.join(out_folder, ehr_name_load+'.json')

    f_th = os.path.join(out_folder, ehr_name+'_th.png')
    f_ehr = os.path.join(out_folder, ehr_name+'.png')
    if not model is None:
        print(f)
    ### Relaod ###
    buffer = None
    if os.path.exists(f):
        if not model is None:
            print("Reload EHR, EHR does not support continue computation.")
        with open(f, "r") as read_file:
            try:
                buffer = json.load(read_file)
            except Exception as e:
                print('Faild to load file: ', str(e))
    ### Compute loop ###
    if buffer is None:
        if model is None:
            return None
        print("EHR does not support continue computation automaticly. Use hints4")

        valset = roc.get_set(args, seg=True, do_iter=True, limited=True)

        if args.hint3 == 1:
            args.hint3 = len(valset)
        args.hint3 = min(len(valset), args.hint3)
        print('n =', args.hint3)
        #Reload Old File if metiond ##
        buffer, skip = reload_other_json(args, f_load)
        camCollection = CamCollection(model,
                                      args.exp_name,
                                      args,
                                      class_index=class_index)
        pbar = tqdm(total=args.hint3)
        # Main Loop
        # print(len(valset))
        for i, batch in enumerate(valset):
            # print(i)
            if i == args.hint3:
                # Stop after min(hint3,len(valset))
                break
            if i < skip:
                # skip the already computed
                pbar.update(1)
                continue
            if cuda:
                input_tensor = batch[0].cuda()
            else:
                input_tensor = batch[0]
            if len(input_tensor.shape) == 3:
                input_tensor = input_tensor.unsqueeze(dim=0)
            mask = batch[1]
            buffer = ehr(input_tensor, mask, camCollection, buffer)
            pbar.update(1)
        # SAVE
        with open(f, 'w') as f:
            for k, v in buffer.items():
                buffer[k] = [vi.tolist() for vi in v]
            json.dump(buffer, f, indent=2)
    if model is None:
        return buffer
    make_graphs(buffer, f_th, f_ehr)


def reload_other_json(args, f_load):
    buffer = None
    skip = -1
    if os.path.exists(f_load):
        with open(f_load, "r") as read_file:
            try:
                buffer = json.load(read_file)
            except Exception as e:
                print('Faild to load file: ', str(e))
        for name in buffer.keys():
            if len(buffer[name]) >= args.hint3:
                buffer[name] = buffer[name][:args.hint3]
            else:
                buffer[name] = [np.array(vi) for vi in buffer[name]]
                skip = len(buffer[name])
    return buffer, skip


def make_graphs(buffer, f_th, f_ehr):
    ###THRESHOLD_GRAPH###
    names = [key for key in buffer.keys() if str(key).endswith('_th')]
    results_th = [buffer[key] for key in names]
    length = len(results_th[0])
    results_th = [np.stack(np.array(r), axis=0).mean(axis=0)
                  for r in results_th]
    legend = [i[:-3] for i in names]
    plt.figure()
    for i in results_th:
        plt.plot(i)
    plt.ylabel(f'% segmented')
    plt.xlabel(f'threshold (relativ)')
    plt.title(f"% of pachtes selected by the threshold. n = {length}")
    plt.legend(legend, loc='center left', bbox_to_anchor=(1, 0.5))
    plt.savefig(f_th, bbox_inches='tight')
    plt.show()
    ##EHR_GRAPH##
    plt.figure()
    results = [buffer[key] for key in legend]
    length = len(results[0])
    results = [np.stack(np.array(r), axis=0).mean(axis=0) for r in results]
    legend = [i[:-3] for i in names]
    for i in results:
        plt.plot(i)

    plt.ylabel('% segmented')
    plt.xlabel('threshold (relativ)')
    plt.title(f"EHR  n = {length}")
    plt.legend(legend, loc='center left', bbox_to_anchor=(1, 0.5))
    plt.savefig(f_ehr, bbox_inches='tight')
    plt.show()
