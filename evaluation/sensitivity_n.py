# Source: https://github.com/CAMP-eXplain-AI/InputIBA/blob/master/input_iba/evaluation/vision/sensitivity_n.py
import os
import matplotlib.pyplot as plt
from torchvision import transforms as T
import torch.nn as nn
from training import Loader
import json
from salience.Cam_Collection import CamCollection
import evaluation.roc as roc
import torch
import numpy as np
from tqdm import tqdm
from threading import current_thread
import sys
sys.path.append("..")


class VisionSensitivityN():

    def __init__(self, camCollection: CamCollection, input_size, n, num_masks=100, patch_size=None):
        self.classifier = camCollection
        self.n = n
        self.device = next(self.classifier.model.parameters()).device
        self.indices, self.masks = self._generate_random_masks(num_masks,
                                                               input_size,
                                                               patch_size,
                                                               device=self.device)

    def evaluate(self,
                 heatmap: torch.Tensor,
                 input_tensor: torch.Tensor,
                 target: int,
                 calculate_corr=False) -> dict:

        pertubated_inputs = []
        sum_attributions = []
        for mask in self.masks:
            # perturb is done by interpolation
            pertubated_inputs.append(input_tensor * (1 - mask))
            sum_attributions.append((heatmap * mask).sum())
        sum_attributions = torch.stack(sum_attributions)
        input_inputs = pertubated_inputs + [input_tensor]
        with torch.no_grad():
            input_inputs = torch.stack(input_inputs).to(self.device)
            output = self.classifier.out(input_inputs, select_index=False)
        output_pertubated = output[:-1]
        output_clean = output[-1:]
        diff = output_clean[:, target] - output_pertubated[:, target]
        #diff = output_clean - output_pertubated
        score_diffs = diff.cpu().numpy()
        sum_attributions = sum_attributions.cpu().numpy()

        # calculate correlation for single image if requested
        corrcoef = None
        if calculate_corr:
            corrcoef = np.corrcoef(
                sum_attributions.flatten(), score_diffs.flatten())
        return {
            "correlation": corrcoef,
            "score_diffs": score_diffs,
            "sum_attributions": sum_attributions
        }

    def _generate_random_masks(self, num_masks, input_size, patch_size, device='cuda:0'):
        """
        generate random masks with n pixel set to zero
        Args:
            num_masks: number of masks
            n: number of perturbed pixels
        Returns:
            masks
        """
        indices = []
        masks = []
        h, w = input_size
        if not patch_size is None:
            h = int(h/patch_size)
            w = int(w/patch_size)
        for _ in range(num_masks):
            idxs = np.unravel_index(
                np.random.choice(h * w, self.n, replace=False), (h, w))
            indices.append(idxs)
            mask = np.zeros((h, w))
            mask[idxs] = 1
            if patch_size is not None:
                mask = np.repeat(mask, patch_size, axis=0)
                mask = np.repeat(mask, patch_size, axis=1)

            masks.append(torch.from_numpy(mask).to(torch.float32).to(device))
        return indices, masks


def sensitivity_n(camCollection,
                  list_of_sampels,
                  h=224, w=224,
                  log_n_max=4.5,
                  log_n_ticks=0.1,
                  num_masks=100,
                  device='cuda:0',
                  buffer=None,
                  pbar=None,
                  patch_size=None):

    if buffer is None:
        buffer = {}
        if patch_size is None:
            patch_size = 1
        else:
            log_n_max = 2  # np.log(14*14-1)
        max_allowed_n = np.log10((h * w)/patch_size/patch_size*0.8)
        assert log_n_max < max_allowed_n, \
            f"log_n_max must smaller than {max_allowed_n}, but got {log_n_max}"
        print(max_allowed_n)
        n_list = np.logspace(0, max_allowed_n,
                             int(max_allowed_n / log_n_ticks),
                             base=10.0, dtype=int)
        # to eliminate the duplicate elements caused by rounding
        n_list = np.unique(n_list).tolist()
        buffer['n_list'] = n_list
        print(f"n_list: [{', '.join(map(str,n_list))}] {len(n_list)}")
        buffer['index'] = []
        buffer['corr_all_n'] = {}
        buffer['corr_all_idx'] = {}
        for n in n_list:
            buffer['corr_all_n'][str(n)] = []
            buffer['corr_all_idx'][str(n)] = []
    else:
        n_list = buffer['n_list']
    # get dataset
    #list_of_sampels = []
    #inputs = batch['input']
    #heatmap = batch['heatmap']
    #targets = batch['target']

    results = {}

    try:
        if pbar is None:
            pbar = tqdm(total=len(n_list) * len(list_of_sampels))
        for n in n_list:
            evaluator = VisionSensitivityN(camCollection,
                                           input_size=(h, w),
                                           n=n,
                                           num_masks=num_masks,
                                           patch_size=patch_size)
            #corr_all = []
            for batch in list_of_sampels:
                input_tensor = batch['input']
                target = batch['target']
                heatmap = batch['heatmap']
                index = batch['index']
                # get data
                if index in buffer['corr_all_idx'][str(n)]:
                    pbar.update(1)
                    continue

                input_tensor = input_tensor.to(device)
                heatmap = torch.from_numpy(heatmap).to(input_tensor) / 255.0
                res_single = evaluator.evaluate(
                    heatmap, input_tensor, target, calculate_corr=True)
                corr = res_single['correlation'][1, 0]
                # corr_all.append(corr)
                corr = corr.item()
                if np.isnan(corr):
                    corr = 0
                buffer['corr_all_n'][str(n)].append(corr)
                buffer['corr_all_idx'][str(n)].append(int(index))
                if not index in buffer['index']:
                    buffer['index'].append(int(index))
                pbar.update(1)
            results.update({str(n): np.mean(buffer['corr_all_n'][str(n)])})
            # except KeyboardInterrupt as e:
            #    print(f'Evaluation ended due to KeyboardInterrupt')
            #    #mmcv.dump(results, file=osp.join(work_dir, file_name))
            #    return results, buffer
    except AssertionError as e:
        print(f'Evaluation ended due to {e}')
        #mmcv.dump(results, file=osp.join(work_dir, file_name))
        # return results, buffer
    #mmcv.dump(results, file=osp.join(work_dir, file_name))
    # print(results)
    return results, buffer


def getSample(idx, valset, camCollection, patch, patch_size, cuda):
    input_tensor, _ = valset[idx]
    if cuda:
        input_tensor = input_tensor.cuda()
    _, names, cams, cams_org = camCollection.execut_all_cam(input_tensor,
                                                            None,
                                                            cuda,
                                                            return_cam=True)
    if patch:
        for idx, cam_o in enumerate(cams_org):
            cam_o = np.repeat(cam_o, patch_size, axis=0)
            cam_o = np.repeat(cam_o, patch_size, axis=1)
            cams[idx] = cam_o
    return names, cams, input_tensor


def load_external(args, patch=True):
    name_add = 'pixel_'
    if patch:
        name_add = 'patch_'
    out_folder1 = os.path.join(args.ckpt_path, args.exp_name, 'sensitivity_n')
    out_folder = os.path.join(out_folder1, 'data')
    out_folder = os.path.join(out_folder, roc.get_name(args))
    print(out_folder)
    return load(out_folder, name_add, None)


def load(out_folder, name_add, camCollection):
    buffers = {}
    results = {}
    file = out_folder + f"{name_add}_buffer.pkl"
    if not file is None and os.path.isfile(file):
        with open(file, 'rb') as f:
            import pickle
            buffers = pickle.load(f)
    file = out_folder + f"{name_add}_result.pkl"
    if not file is None and os.path.isfile(file):
        with open(file, 'rb') as f:
            import pickle
            results = pickle.load(f)
    if not camCollection is None:
        for name in camCollection.id2cam:
            if not name in buffers:
                buffers[name] = None
                results[name] = None
    if not 'index' in buffers:
        buffers['index'] = []
    return buffers, results


def store(buffers, results, out_folder, name_add):
    file = out_folder + f"{name_add}_buffer.pkl"
    if not file is None:
        import pickle
        with open(file, 'wb') as f:
            pickle.dump(buffers, f)
    file = out_folder + f"{name_add}_result.pkl"
    if not file is None:
        import pickle
        with open(file, 'wb') as f:
            pickle.dump(results, f)


def compute_sensitivity_n(args, model, patch=True, ):
    # Hous keeping
    cuda = True
    num_sampls = args.hint3
    num_masks = args.hint4
    name_add = 'pixel_'
    if patch:
        patch_size = 16
        name_add = 'patch_'
    else:
        patch_size = None
    name = args.exp_name
    roc_name, selected_index = roc.get_roc_name(args, model)
    camCollection = CamCollection(
        model, name, args, class_index=selected_index)
    num_of_n = 42
    if patch:
        num_of_n = 18

    if model is not None:
        valset = roc.get_set(args)

        x = np.load(roc_name)
        cutoff = 0.5
        idx_tp = np.where(x[1] >= cutoff)[0]
        #idx_fp = np.where(np.logical_and(x[0] == 0, x[1] >= cutoff))[0]
        num_sampls = min(idx_tp.shape[-1], num_sampls)
        print(num_sampls, '/', idx_tp.shape[-1])
        np.random.seed(1337)
    out_folder1 = os.path.join(args.ckpt_path, args.exp_name, 'sensitivity_n')
    if not os.path.exists(out_folder1):
        os.makedirs(out_folder1)
    out_folder = os.path.join(out_folder1, 'data')
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    out_folder = os.path.join(out_folder, roc.get_name(args))

    buffers, results = load(out_folder, name_add, camCollection)
    pbar = tqdm(total=num_of_n * num_sampls * len(camCollection.id2cam))
    for sample_idx in idx_tp[:num_sampls]:
        if sample_idx in buffers['index']:
            pbar.update(num_of_n * len(camCollection.id2cam))
            continue
        names, cams, input_tensor = getSample(
            sample_idx, valset, camCollection, patch, patch_size, cuda)
        for name, cam in zip(names, cams):
            list_of_sampels = [{'input': input_tensor,
                                'heatmap': cam,
                                'target': camCollection.class_index,
                                'index': sample_idx}]
            current_buffer = buffers[name]
            result, buffer = sensitivity_n(camCollection, list_of_sampels,
                                           num_masks=num_masks, buffer=current_buffer,
                                           pbar=pbar, patch_size=patch_size)
            del list_of_sampels
            buffers[name] = buffer
            results[name] = result
            results[num_masks] = num_masks
        buffers['index'].append(sample_idx)
        store(buffers, results, out_folder, name_add)
    # parse to list
    # print(results)
    if model is not None:
        names = camCollection.id2cam
        for name in names:
            out = results[name]
            keys = list(out.keys())
            print(out, keys)
            plt.plot(np.array(keys), np.array(list(out.values())))
            #results.update({str(n): np.mean(buffer['corr_all_n'][str(n)])})
        plt.ylabel('Correlation')
        plt.xlabel('n removed')

        # plt.xscale("log")
        plt.title(
            f"{name_add}sensitivity_n; num_masks={num_masks}; n_sampels={len(buffers[names[0]]['index'])}")
        plt.legend(names, loc='center left', bbox_to_anchor=(1, 0.5))
        if not patch_size is None:
            patches = 196  # patch_size*patch_size
            # x,_ = plt.xticks() #[1,4,16,64]
            # [1, 2, 3, 4, 5, 6, 8, 11, 14, 18, 23, 29, 37, 48, 61, 78, 100]
            x = [int(k) for k in keys]
            x2 = [i for i in range(len(x))]
            x = [str(int(v/patches*10000)/100) + ' %' +
                 ('      ' + str(v))[-6:] for v in x]
            print(x)
            plt.xticks(x2, x, rotation='vertical')

        else:
            # x,_ = plt.xticks() #[1,4,16,64]
            x = [int(k) for k in keys]
            x2 = [i for i in range(len(x))]
            x = [str(int(v/(224*224)*10000)/100) + ' %' +
                 ('      ' + str(v))[-6:] for v in x]
            for i in range(len(x)):
                if i % 3 != 1:
                    x[i] = ' '
            print(x)
            plt.xticks(x2, x, rotation='vertical')
            print(plt.xticks())

        f = os.path.join(
            out_folder1, f"{roc.get_name(args)}_{name_add}_sensitivity_n_plot.png")
        print('save fig')
        plt.savefig(f, bbox_inches='tight')  # dpi=500
        plt.show()
    return results, buffers
