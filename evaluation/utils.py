import math
import torch
import os
from training import Loader
from skimage.metrics import structural_similarity
import numpy as np


def reload(path, exp_name, dataset_path, args=None, settings=None, reload=True, do_reload_model=True):
    model_path = os.path.join(path, exp_name)
    args = Loader.reloadSettings(model_path,dataset_path, args=args)
    args.ckpt_path = path
    args.exp_name = exp_name
    if do_reload_model:
        # Make models
        model = Loader.getNN(args)
        # To Cuda
        if not args.cpu:
            Loader.toCuda(args, nets=[model])
        # load checkpoint
        if reload:
            Loader.load(args, model)
        model.eval()
    else:
        model = None
    #args.local = not os.path.isdir("/mnt/cephstorage/share-all")
    if not settings is None:
        args.hint1 = settings.hint1
        args.hint2 = settings.hint2
        args.hint3 = settings.hint3
        args.hint4 = settings.hint4
    return args, model


def ssim(mask_1, mask_2):
    mask_1 = convert_mask(mask_1)
    mask_2 = convert_mask(mask_2)
    return structural_similarity(mask_1, mask_2, win_size=5, data_range=255)


def convert_mask(m):
    if m.dtype in (np.float64, np.float32, np.float16):
        # print(m.max())
        assert(m.max() <= 1.0)
        m = (m * 255).astype(np.uint8)
    return m


def youdens_index(y_true, y_score):
    from sklearn.metrics import roc_curve
    '''Find data-driven cut-off for classification

    Cut-off is determied using Youden's index defined as sensitivity + specificity - 1.

    Parameters
    ----------

    y_true : array, shape = [n_samples]
        True binary labels.

    y_score : array, shape = [n_samples]
        Target scores, can either be probability estimates of the positive class,
        confidence values, or non-thresholded measure of decisions (as returned by
        “decision_function” on some classifiers).

    References
    ----------

    Ewald, B. (2006). Post hoc choice of cut points introduced bias to diagnostic research.
    Journal of clinical epidemiology, 59(8), 798-801.

    Steyerberg, E.W., Van Calster, B., & Pencina, M.J. (2011). Performance measures for
    prediction models and markers: evaluation of predictions and classifications.
    Revista Espanola de Cardiologia (English Edition), 64(9), 788-794.

    Jiménez-Valverde, A., & Lobo, J.M. (2007). Threshold criteria for conversion of probability
    of species presence to either–or presence–absence. Acta oecologica, 31(3), 361-369.
    '''
    fpr, tpr, thresholds = roc_curve(y_true, y_score)
    idx = np.argmax(tpr - fpr)
    return thresholds[idx]


def upscale_attetion(inp, factor=2):
    # print(inp.shape)
    # print(type(inp))
    num_patch = math.floor(math.sqrt(inp.size(-1)))
    one = inp.size(1)
    inp = inp.reshape(one, num_patch*num_patch, 1, num_patch, num_patch)
    #inp = torch.nn.functional.interpolate(inp, scale_factor=2, mode='bilinear')
    inp = torch.repeat_interleave(
        torch.repeat_interleave(inp, factor, -1), factor, -2)
    out_size = num_patch*num_patch*factor*factor
    inp = inp.reshape(one, num_patch, num_patch, out_size)
    inp = torch.repeat_interleave(
        torch.repeat_interleave(inp, factor, -2), factor, -3)
    inp = inp.reshape(1, one, out_size, out_size)
    return inp/(factor*factor)
