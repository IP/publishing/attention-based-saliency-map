# Source: https://github.com/CAMP-eXplain-AI/InputIBA/blob/master/input_iba/evaluation/vision/sensitivity_n.py
import sys
sys.path.append("..")
from dataloader.dataset import Dataset

from tqdm import tqdm
import numpy as np
import torch

import evaluation.roc as roc
from salience.Cam_Collection import CamCollection

import matplotlib.pyplot as plt
import argparse
import pickle
import os
import evaluation.utils as utils

def save(status):
    if status['keep_imgs']:
        return
    file = status['file']
    buffer = status['buffer']
    with open(file, 'wb') as f:
        pickle.dump(buffer, f)


def load(file, keep_imgs) -> dict:
    if keep_imgs:
        return
    if os.path.isfile(file):
        with open(file, 'rb') as f:
            return pickle.load(f)
    else:
        return {}

def compute_ssim(settings, keep_imgs = False):
    from evaluation.utils import reload
    args1, model1 = reload(settings.ckpt_path, settings.exp_name1,settings=settings)
    args2, model2 = reload(settings.ckpt_path, settings.exp_name2,settings=settings)

    num_samples = settings.hint4
    dataset = roc.get_set(args1)
    _, selected_index = roc.get_roc_name(args1, model1)
    if selected_index < 0:
        selected_index = None
    cc1 = CamCollection(model1, args1.exp_name, args1,class_index=selected_index)
    cc2 = CamCollection(model2, args2.exp_name, args2,class_index=selected_index)
    
    out_folder = os.path.join(args1.ckpt_path, args1.exp_name, 'sanity_check')
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    name = args2.exp_name.replace('/','_')
    file = os.path.join(out_folder, f'{roc.get_name(args1)}_{name}.pkl')
    f2 = os.path.join(out_folder, f'{roc.get_name(args1)}_{name}.png')
    buffer = load(file,keep_imgs) 
    status = {
        'args1' : args1,
        'args2' : args2,
        'cc1' : cc1,
        'cc2' : cc2,
        'dataset' : dataset,
        'class' : roc.labels[selected_index],
        'keep_imgs' : keep_imgs,
        'file' : file,
        'buffer' : buffer,
        'img_buffer' : buffer,
        'file2': f2,
        'num_samples' : num_samples
    }
    main_loop(status)
    make_graphs(status)
    return status
    
def main_loop(status):
    buffer:dict = status['buffer']
    img_buffer:dict = status['img_buffer']
    keep_imgs:bool = status['keep_imgs']
    num_samples:int = status['num_samples']
    args1 = status['args1']
    cc1:CamCollection = status['cc1']
    cc2:CamCollection = status['cc2']
    dataset = status['dataset']
    print(num_samples)
    
    if len(buffer) != 0:
        # get a random entry to measuer it lengths
        tmp = list(buffer.values())[0]
        start_idx = len(tmp)
    else:
        start_idx = 0
    pbar = tqdm(num_samples-start_idx)
    
    for idx_layer in range(start_idx,min(num_samples,len(dataset))):
        torch.cuda.empty_cache()
        pbar.update(1)
        input_tensor, _ = dataset[idx_layer]
        if not args1.cpu:
            input_tensor = input_tensor.cuda()            
        _, names1, _, cams_org1 = cc1.execut_all_cam(input_tensor.clone(),None,not args1.cpu,return_cam=True)
        _, names2, _, cams_org2 = cc2.execut_all_cam(input_tensor,None,not args1.cpu,return_cam=True)
        for name, map1, map2 in zip(names1,cams_org1,cams_org2):
            score = utils.ssim(map1,map2)
            if not name in buffer:
                buffer[name] = []
                if keep_imgs:
                    img_buffer[name] = []
            buffer[name].append(score)
            if keep_imgs:
                img_buffer[name].append((map1,map2))
        save(status)
    
def make_graphs(status):
    buffer = status['buffer']
    print(buffer)
    file = status['file2']
    ###THRESHOLD_GRAPH###
    names = [key.replace('LPR ','LPR \n',) for key in buffer.keys()]
    results = [buffer[key] for key in names]
    length = len(results[0])
    results = [np.stack(np.array(r), axis=0).mean(axis=0) for r in results]
    names = ['Identity']+names
    results = [1] + results
    plt.figure()
    plt.bar(names,results)
    plt.ylabel(f'ssim')
    plt.xlabel(f' ')
    plt.title(f"Simularaty of 2 networks. n = {length}")
    #plt.legend(names, loc='center left', bbox_to_anchor=(1, 0.5))
    plt.savefig(file, bbox_inches='tight')
    plt.show()    
    
#python simularaty.py -en1 02/CoAt1_cl3 -en2 02/vit1_1cl -cp C:/Users/rober/Desktop/ios/output/ -h1 2 -h2 2 -h4 3
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-cp", "--ckpt_path",
                        type=str, default="/space/grafr/",
                        help="Path to the root checkpoint folder")
    parser.add_argument("-en1", "--exp_name1", type=str,
                        required=True, help="Experiment name")
    parser.add_argument("-en2", "--exp_name2", type=str,
                        required=True, help="Experiment name")
    parser.add_argument("-h1", "--hint1", type=int,
                        default=1, help="hint")
    parser.add_argument("-h2", "--hint2", type=int,
                        default=0, help="hint")
    parser.add_argument("-h3", "--hint3", type=int,
                        default=1, help="hint")
    parser.add_argument("-h4", "--hint4", type=int,
                        default=1, help="hint")
    settings = parser.parse_args()
    print(settings)
    compute_ssim(settings)
    

    