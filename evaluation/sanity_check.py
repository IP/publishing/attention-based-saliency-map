
from math import floor
import evaluation.roc as roc
from salience.Cam_Collection import CamCollection
from salience.Cam_Functions import printGPUstorage
import os
import sys
import torch
import numpy as np
from salience.LRP.LRP_visu import LRP_vis
import training.Loader as Loader
import pickle
from tqdm import tqdm
from evaluation.utils import ssim
import random
import matplotlib.pyplot as plt
sys.path.append("..")


def save(status):
    if status['keep_imgs']:
        return
    file = status['file']
    buffer = status['buffer']
    with open(file, 'wb') as f:
        pickle.dump(buffer, f)
    file = status['file3']
    targes = status['targes']
    with open(file, 'wb') as f:
        pickle.dump(targes, f)


def load(file) -> dict:
    if os.path.isfile(file):
        with open(file, 'rb') as f:
            return pickle.load(f)
    else:
        return {}


def sanity_check(args, model, keep_imgs=False, back2front=True, single=False):
    args.hint2 = -1  # We use random index
    if args.hint4 == -1:
        num_samples = 100
    else:
        num_samples = args.hint4
    print('num_samples', num_samples)
    cuda = not args.cpu
    if not cuda:
        print('Warning: No cuda')
    model.eval()
    # Make models
    model_rand = Loader.getNN(args)
    if cuda:
        Loader.toCuda(args, nets=[model_rand])
    Loader.load(args, model_rand)
    model_rand.eval()
    print('Load dataset')
    valset = roc.get_set(args, seg=False)
    out_folder = os.path.join(args.ckpt_path, args.exp_name, 'sanity_check')
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    if single:
        singel_text = '_single'
    else:
        singel_text = ''
    f1 = os.path.join(
        out_folder, f'{roc.get_name(args)}_{num_samples}{singel_text}.pkl')
    f2 = os.path.join(
        out_folder, f'{roc.get_name(args)}_{num_samples}{singel_text}.png')
    buffer = load(f1)
    f3 = os.path.join(
        out_folder, f'{roc.get_name(args)}_{num_samples}_targets.pkl')
    targes = load(f3)
    if len(targes) == 0:
        targes['idx_img'] = []
        targes['channels'] = []
    status = {
        'model': model,
        'model_rand': model_rand,
        'cc_model': CamCollection(model, 'org', args),
        'cc_model_rand': CamCollection(model_rand, 'rand', args),
        'set': valset,
        'buffer': buffer,
        'keep_imgs': keep_imgs,
        'img_buffer': {},
        'num_samples': num_samples,
        'file': f1,
        'file2': f2,
        'file3': f3,
        'args': args,
        'targes': targes,
        'back2front': back2front,
        'single': single
    }
    main_loop(status)
    make_graphs(status)
    return status


def make_graphs(status):
    buffer = status['buffer']
    # print(buffer)
    # print(status['targes'])
    file = status['file2']
    ###THRESHOLD_GRAPH###
    names = [key for key in buffer.keys()]
    results = [buffer[key] for key in names]
    length = len(results[0][0])
    results = [np.stack(np.array(r), axis=1).mean(axis=0) for r in results]
    plt.figure()
    for i in results:
        plt.plot([1] + list(i))
    if status['back2front']:
        bonus_text = 'Randomization from back to front'
    else:
        bonus_text = 'Randomization from front to back.'

    plt.xlabel(f'The number of randomized layers. {bonus_text}')
    plt.ylabel(f'ssim (between original and the current)')
    plt.title(f"Sanity check. n = {length}")
    plt.legend(names, loc='center left', bbox_to_anchor=(1, 0.5))
    plt.savefig(file, bbox_inches='tight')
    plt.show()


def randomice_liniar(m):
    y = m.in_features
    m.weight.data.normal_(0.0, 1/np.sqrt(y))
    try:
        m.bias.data.fill_(0)
    except:
        pass


def randomice_conv(m):
    torch.nn.init.xavier_uniform(m.weight)
    m.weight.data.fill_(0.01)


def main_loop(status):
    buffer = status['buffer']
    img_buffer = status['img_buffer']
    keep_imgs = status['keep_imgs']
    num_layers = get_num_of_layers(status)
    num_samples = status['num_samples']
    print(num_layers*num_samples)
    pbar = tqdm(num_layers*num_samples)

    for idx_layer in range(num_layers):
        randomice_layer(status, idx_layer)
        torch.cuda.empty_cache()
        if len(buffer) != 0:
            # get a random entry to measuer it lengths
            tmp = list(buffer.values())[0]
            pbar.update(len(tmp[idx_layer]))
            start_idx = len(tmp[idx_layer])
        else:
            start_idx = 0
        for idx in range(start_idx, num_samples):
            pbar.update(1)
            scores, scores_img = compute_random_sample(status, idx)
            for k, v in scores.items():
                if not k in buffer:
                    buffer[k] = [[] for _ in range(num_layers)]
                    if keep_imgs:
                        img_buffer[k] = [[] for _ in range(num_layers)]
                buffer[k][idx_layer].append(v)
                if keep_imgs:
                    img_buffer[k][idx_layer].append(scores_img[k])
            if idx % 42 == 0:
                save(status)
        save(status)


def compute_random_sample(status, idx):
    torch.cuda.empty_cache()
    cc_model: CamCollection = status['cc_model']
    cc_model_rand: CamCollection = status['cc_model_rand']
    args = status['args']
    max_channles: int = args.channel_hint
    dataset = status['set']
    targes = status['targes']

    idx_img = targes['idx_img']
    channels = targes['channels']
    # print(targes)
    if len(idx_img) <= idx:
        selected_channel: int = random.randint(0, max_channles-1)
        selected_img: int = random.randint(0, len(dataset)-1)
        # Prevent dublicats
        while (selected_channel in channels and selected_img in idx_img):
            selected_channel: int = random.randint(0, max_channles-1)
            selected_img: int = random.randint(0, len(dataset)-1)
        channels.append(selected_channel)
        idx_img.append(selected_img)
    else:
        selected_channel: int = channels[idx]
        selected_img: int = idx_img[idx]

    cc_model.class_index = selected_channel
    cc_model_rand.class_index = selected_channel
    input_tensor, _ = dataset[selected_img]
    if not args.cpu:
        input_tensor = input_tensor.cuda()
    # print('exe',idx_img)
    # printGPUstorage()

    _, names, _, cams_org = cc_model.execut_all_cam(input_tensor.clone(),
                                                    None, not args.cpu, return_cam=True)
    _, _, _, cams_org_rand = cc_model_rand.execut_all_cam(input_tensor,
                                                          None, not args.cpu, return_cam=True)
    del input_tensor
    scors = {}
    scors_img = {}
    for i, name in enumerate(names):
        scors[name] = ssim(cams_org[i], cams_org_rand[i])
        if status['keep_imgs']:
            scors_img[name] = (cams_org[i], cams_org_rand[i])
        # print(name,scors[name])
    return scors, scors_img


def get_num_of_layers(status):
    args = status['args']
    model_rand = status['model_rand']
    if Loader.isCoAt(args):
        return sum(model_rand.num_blocks)
    elif Loader.isViT(args):
        return len(model_rand.a.blocks)*2
    else:
        raise NotImplemented


def randomice_layer(status, idx_layer):
    if status['back2front']:
        idx_layer = get_num_of_layers(status)-idx_layer-1
    # print('Layer',idx_layer)
    # printGPUstorage()
    args = status['args']
    if status['single']:
        a = status['model_rand']
        try:
            del a.target_layers
            LRP_vis.registered_models.clear()
        except:
            pass
        Loader.load(args, a)
        status['cc_model_rand'].updateModel(
            'rand', status['model_rand'], not status['args'].cpu)
    model_rand = status['model_rand']
    randomice_layer_help(args, model_rand, idx_layer)
    randomice_layer_lrp(status, idx_layer)


def randomice_layer_help(args, model_rand, idx_layer):
    if Loader.isCoAt(args):
        num_blocks = model_rand.num_blocks
        # s0
        if idx_layer < num_blocks[0]:
            randomice_conv(model_rand.s0[idx_layer][0])
            return
        idx_layer -= num_blocks[0]
        # s1
        if idx_layer < num_blocks[1]:
            randomice_conv(model_rand.s1[idx_layer].conv.fn[0])
            randomice_conv(model_rand.s1[idx_layer].conv.fn[3])
            return
        idx_layer -= num_blocks[1]
        # s2
        if idx_layer < num_blocks[2]:
            randomice_conv(model_rand.s2[idx_layer].conv.fn[0])
            randomice_conv(model_rand.s2[idx_layer].conv.fn[3])
            return
        idx_layer -= num_blocks[2]
        # s3
        if idx_layer < num_blocks[3]:
            # randomice_conv(model_rand.s3[idx_layer].proj)
            randomice_liniar(model_rand.s3[idx_layer].attn[1].fn.to_qkv)
            randomice_liniar(model_rand.s3[1].ff[1].fn.net[0])
            randomice_liniar(model_rand.s3[1].ff[1].fn.net[3])
            return
        idx_layer -= num_blocks[3]
        # s4
        if idx_layer < num_blocks[4]:
            randomice_liniar(model_rand.s4[idx_layer].attn[1].fn.to_qkv)
            randomice_liniar(model_rand.s4[1].ff[1].fn.net[0])
            randomice_liniar(model_rand.s4[1].ff[1].fn.net[3])
            return
        raise NotImplemented
    elif Loader.isViT(args):
        block = model_rand.a.blocks[floor(idx_layer/2)]
        if idx_layer % 2 == 0:
            randomice_liniar(block.attn.qkv)
            randomice_liniar(block.attn.proj)
        else:
            randomice_liniar(block.mlp.fc1)
            randomice_liniar(block.mlp.fc2)
    else:
        raise NotImplemented


def randomice_layer_lrp(status, idx_layer):
    # print('Layer',idx_layer)
    # printGPUstorage()
    args = status['args']
    try:
        model_rand = status['cc_model_rand'].get_LPR_model('rand')
    except:
        return
    # LPR has the same sutructur as non lrp
    randomice_layer_help(args, model_rand, idx_layer)
