from tqdm import tqdm
from salience.Cam_Collection import CamCollection
import numpy as np
import argparse
import os
from training import Loader
import json
import evaluation.roc as roc
import evaluation.utils as utils
from evaluation.utils import ssim
import sys
sys.path.append("..")


parser = argparse.ArgumentParser()


def get_file_name(settings):
    out_folder = os.path.join(
        settings.ckpt_path, settings.exp_name1, 'compare_nets')
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)

    file = os.path.join(out_folder, settings.exp_name1 +
                        '_vs_'+settings.exp_name2+'.json')
    img_file = os.path.join(
        out_folder, settings.exp_name1+'_vs_'+settings.exp_name2 + '_' + str(settings.hint3)+'.png')
    return file, img_file


def reload(settings):
    f, _ = get_file_name(settings)
    buffer = None
    skip = 0
    if os.path.exists(f):
        with open(f, "r") as read_file:
            try:
                buffer = json.load(read_file)
            except Exception as e:
                print('Faild to load file: ', str(e))
        for name in buffer.keys():
            if len(buffer[name]) >= settings.hint3:
                buffer[name] = buffer[name][:settings.hint3]
            skip = len(buffer[name])
    # print(buffer,skip)
    return buffer, skip


def save(buffer, settings):
    f, _ = get_file_name(settings)
    with open(f, 'w') as f:
        json.dump(buffer, f, indent=2)


def save_image(buffer, settings):
    _, f = get_file_name(settings)
    import plotly.express as px
    import plotly.graph_objects as go
    print(f)
    for key, value in buffer.items():
        print(key, np.mean(value), "+-", np.std(value))
    data = [go.Box(y=val, name=key, boxpoints='all')
            for key, val in buffer.items()]
    fig = go.Figure(data=data)

    fig.update_layout(
        font_size=22,
        yaxis_title='SSIM',
        yaxis=dict(constrain='domain', range=[-0.4, 1.0]),
        margin=dict(l=10, r=5, t=5, b=10),
        width=1000, height=400,
        showlegend=False
    )
    fig.show()
    fig.write_image(f)


if __name__ == "__main__":
    cuda = True
    # CUDA_VISIBLE_DEVICES=3 python3 difference_between_nets.py -cp /space/grafr/02/ -en1 vit1_5cl -en2 Vit3_cl5 -h3 10
    # python difference_between_nets.py -cp C:/Users/rober/Desktop/ios/output/02_new/ -en1 vit1_5cl -en2 Vit3_cl5 -h3 10
    parser.add_argument("-cp", "--ckpt_path",
                        type=str, default="/space/grafr/",
                        help="Path to the root checkpoint folder")
    parser.add_argument("-en1", "--exp_name1", type=str,
                        required=True, help="Experiment name")
    parser.add_argument("-en2", "--exp_name2", type=str,
                        required=True, help="Experiment name")
    parser.add_argument("-h1", "--hint1", type=int,
                        default=1, help="data set")
    parser.add_argument("-h2", "--hint2", type=int,
                        default=0, help="class ")
    parser.add_argument("-h3", "--hint3", type=int,
                        default=100, help="number of sampels")
    parser.add_argument("-h4", "--hint4", type=int,
                        default=1, help="")
    settings = parser.parse_args()
    # print(settings)

    args1, model1 = utils.reload(
        settings.ckpt_path, settings.exp_name1, settings=settings)
    _, transforms1 = Loader.getArgmentation(args1)

    args2, model2 = utils.reload(
        settings.ckpt_path, settings.exp_name2, settings=settings)
    _, transforms2 = Loader.getArgmentation(args2)

    valset = roc.get_set(args1)
    roc_name, selected_index = roc.get_roc_name(args1, model1)
    camCollection1 = CamCollection(
        model1, settings.exp_name1, args1, class_index=selected_index)
    camCollection2 = CamCollection(
        model2, settings.exp_name2, args2, class_index=selected_index)

    def getSample(idx):
        input_tensor, _ = valset[idx]
        if cuda:
            input_tensor = input_tensor.cuda()
        _, names, cams, cams_org1 = camCollection1.execut_all_cam(input_tensor,
                                                                  None,
                                                                  cuda,
                                                                  return_cam=True)
        _, names, cams, cams_org2 = camCollection2.execut_all_cam(input_tensor,
                                                                  None,
                                                                  cuda,
                                                                  return_cam=True)
        del input_tensor
        return names, cams_org1, cams_org2,


buffer, skip = reload(settings)
pbar = tqdm(total=args1.hint3-skip)


def compute_example(idx):
    global buffer
    names, cams_org1, cams_org2,  = getSample(idx)
    if buffer is None:
        buffer = {}
        for n in names:
            buffer[n] = []
    for name, c1, c2 in zip(names, cams_org1, cams_org2):
        buffer[name].append(ssim(c1, c2))
    pbar.update(1)


for i in range(skip, args1.hint3):
    compute_example(i)

save(buffer, settings)
save_image(buffer, settings)
