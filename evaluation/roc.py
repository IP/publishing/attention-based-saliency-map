from cProfile import label
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
from training import Loader
import os
from dataloader.pt_SingleClassLoader import SingleClassDataset, SingleClassMultiDataset
import torch.nn as nn
import sys
import torch
from torch.utils.data import DataLoader


def get_roc_name(args, model):
    roc_name = None
    selected_index = -1
    roc_name = f'roc_{get_name(args)}.npy'

    if args.hint2 != -1:
        selected_index = args.hint2
        #roc_name = f'id_{selected_index}_{roc_name}'
    roc_path = os.path.join(args.ckpt_path, args.exp_name, 'roc')
    if not os.path.exists(roc_path):
        os.makedirs(roc_path)
    roc_path = os.path.join(roc_path, roc_name)
    return roc_path, selected_index


labels = ["Pneumothorax", "Pleural Effusion",
          "Cardiomegaly", "Atelectasis", "Consolidation"]  # TODO unify with others


def get_name(args) -> str:
    if args.hint1 == 1:
        name = 'siim_acr'
    elif args.hint1 == 2:
        name = 'Chexpert'
    elif args.hint1 == 3:
        name = 'VinBigData'
    elif args.hint1 == 5:
        name = 'mimic_vals'
    elif args.hint1 == 4:
        name = 'ChestXray14'

    if args.hint2 != -1:
        selected_index = args.hint2
        name = f'{name}_{labels[selected_index]}'
    return name


def get_set(args, seg=False, do_iter=False, limited=False):
    labels_selected = labels[:args.channel_hint]
    _, transforms_val = Loader.getArgmentation(args)
    print(transforms_val)
    if args.hint2 != -1:
        print(get_name(args),'\nuse segmetnation:', seg)
        print("root =", args.dataset_path, "cache_dir =", args.cache_dir,)
    ##### DATA SET without segmentation #######
    if not seg:
        if args.hint1 == 1:
            print("Location Server")
            import dataloader.siim_acr_pneumothorax_segmentation as ssim
            valset = ssim.SIIMDatasetSingle(
                root=args.dataset_path, cache_dir=args.cache_dir, transforms=transforms_val)
        elif args.hint1 == 2:
            from dataloader.pt_MultiClassLoader import MultiClassDataset
            valset = MultiClassDataset(root=args.dataset_path, cache_dir=args.cache_dir,
                                       transforms=transforms_val, dataset=0,
                                       validation=True, labels=labels_selected)
        elif args.hint1 == 3:
            from dataloader.pt_MultiClassLoader import MultiClassDataset
            valset = MultiClassDataset(root=args.dataset_path, cache_dir=args.cache_dir,
                                       transforms=transforms_val, dataset=2,
                                       validation=True, labels=labels_selected)
        elif args.hint1 == 4:
            from dataloader.pt_MultiClassLoader import MultiClassDataset
            valset = MultiClassDataset(root=args.dataset_path, cache_dir=args.cache_dir,
                                       transforms=transforms_val, dataset=3,
                                       validation=True, labels=labels_selected)
        elif args.hint1 == 5:
            from dataloader.pt_MultiClassLoader import MultiClassDataset
            valset = MultiClassDataset(root=args.dataset_path, cache_dir=args.cache_dir,
                                       transforms=transforms_val, dataset=1,
                                       validation=True, labels=labels_selected)

        else:
            assert(False)
    ##### DATA SET with seg #######
    else:
        if args.hint1 == 1:
            print("Location Server")
            import dataloader.siim_acr_pneumothorax_segmentation as ssim
            valset = ssim.SIIMDatasetSeg(root=args.dataset_path, cache_dir=args.cache_dir,
                                         transforms=transforms_val)
        elif args.hint1 == 4:
            from dataloader.chestxray14 import ChestXray14Seg
            valset = ChestXray14Seg(root=args.dataset_path, cache_dir=args.cache_dir,
                                    transforms=transforms_val,
                                    label=labels[args.hint2], labels=labels_selected)
        elif args.hint1 == 3:
            from dataloader.vinbigdata import VinBigDataSeg
            valset = VinBigDataSeg(root=args.dataset_path, cache_dir=args.cache_dir,
                                   transforms=transforms_val,
                                   label=labels[args.hint2], labels=labels_selected)
        else:
            print(
                f'{args.hint1} does not have a seg implemented {args.hint2} ,{seg}')
            assert(False)
    if limited:
        valset.limit2class()
    if do_iter:
        import torch.multiprocessing
        torch.multiprocessing.set_sharing_strategy('file_system')
        return DataLoader(valset, batch_size=1,
                          shuffle=False, num_workers=args.num_cpu,
                          drop_last=False)
    else:
        return valset


def select_index(pred, gt, selected_index):
    if selected_index != -1:
        # print(predction.shape,output_A_val.shape,selected_index)
        if gt.shape[-1] != 1:
            gt = gt[..., selected_index]
        pred = pred[..., selected_index]
        # print(predction.shape,output_A_val.shape,selected_index,input_A_val.shape)
    if pred.shape != gt.shape:
        print('\n#####################################################')
        print('Shape miss match', pred.shape,gt.shape)
        print('selected_index:', selected_index)
        print('If the network produces more than one class, you must select a class. Like -h2 0')
        print('#####################################################')
    assert(pred.shape == gt.shape)
    return pred, gt


def compute_roc(args, model, ignor_file=False, do_print=True, do_spam=True):
    roc_file, selected_index = get_roc_name(args, model)
    if do_spam:
        print(roc_file)
    if os.path.exists(roc_file) and not ignor_file:
        if do_spam:
            print("Reload ROC")
        arr = np.load(roc_file, allow_pickle=True)
        gt = arr[0].tolist()
        pred = arr[1].tolist()
    else:
        Tensor = torch.cuda.FloatTensor if not args.cpu else torch.Tensor
        sig = nn.Sigmoid()
        valset = get_set(args, do_iter=True)
        print(len(valset))
        model.eval()
        pred = []
        gt = []
        with torch.no_grad():
            for i, batch in enumerate(valset):
                # if i%101 == 100:
                #    #break
                sys.stdout.write(f"\r{i} / {len(valset)}")
                # optimizer.zero_grad()

                if not args.cpu:
                    input_A_val = batch[0].cuda()
                else:
                    input_A_val = batch[0]
                if len(input_A_val.shape) == 3:
                    input_A_val = input_A_val.unsqueeze(dim=0)
                #output_A_val = batch[1][...,10]
                output_A_val = batch[1]

                predction = sig(model(input_A_val))

                predction, output_A_val = select_index(
                    predction, output_A_val, selected_index)

                if isinstance(output_A_val, int) or len(output_A_val.shape) == 0 or output_A_val.shape[0] == 1:
                    pred.append(predction.detach().cpu().item())
                    # gt.append(output_A_val.detach().cpu().item())
                    gt.append(output_A_val.detach().cpu().item())
                    # if output_A_val <= 0.1 and predction >= 0.9:
                    #    sys.stdout.write(
                    #        f"\r{i} / {len(valset)}\t| {(predction.detach().cpu().item())} \n")
                else:
                    for i in range(output_A_val.shape[0]):
                        # print(i,predction.shape,output_A_val.shape,input_A_val.shape)
                        pred.append(predction[i].detach().cpu().item())
                        gt.append(output_A_val[i].detach().cpu().item())
                if len(valset) == i+1:
                    break
        # SAVE
        arr = np.stack((np.array(gt), np.array(pred)))
        if not ignor_file:
            np.save(roc_file, arr)

    # Compute ROC curve and ROC area for each class

    fpr, tpr, th = roc_curve(gt, pred)
    roc_auc = auc(fpr, tpr)

    ##################
    from sklearn.metrics import precision_recall_curve, average_precision_score
    precision, recall, thresholds = precision_recall_curve(gt, pred)
    precision = np.concatenate(([0], precision))
    recall = np.concatenate(([1], recall))

    f1_scores = np.nan_to_num(2*recall*precision/(recall+precision))

    f1_score_max_id = np.argmax(f1_scores)
    if do_spam:
        print('Best threshold: ', thresholds[np.argmax(f1_scores)])
        print('Best F1-Score: ', np.max(f1_scores))
    f1_score_max_id_roc = np.argmax(th <= thresholds[np.argmax(f1_scores)])
    if do_spam:
        print('fpr-Score: ', fpr[f1_score_max_id_roc], )
        print('tpr-Score: ', tpr[f1_score_max_id_roc], '(sensitivity)')
        print('tnr-Score: ', 1-fpr[f1_score_max_id_roc], '(specificity)')

    ##############################################

    # Compute micro-average ROC curve and ROC area
    #fpr["micro"], tpr["micro"], _ = roc_curve(gt.ravel(), pred.ravel())
    #roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    if do_spam:
        print('roc_auc: ', roc_auc)
        plt.figure()
        lw = 2
        plt.plot(
            fpr,
            tpr,
            color="darkorange",
            lw=lw,
            label=f"{args.exp_name} -s ROC curve (area = {roc_auc:.2})"
        )
        plt.plot([0, 1], [0, 1], color="navy", lw=lw, linestyle="--")
        plt.plot(fpr[f1_score_max_id_roc], tpr[f1_score_max_id_roc], 'ro')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel("False Positive Rate")
        plt.ylabel("True Positive Rate")
        plt.title("Receiver operating characteristic")
        plt.legend(loc="lower right")
        if not ignor_file:
            plt.savefig(f'{roc_file[:-4]}_roc.png',
                        dpi=100, bbox_inches='tight')
        if do_print:
            plt.show()

        ####################################

    prc_auc = average_precision_score(gt, pred)  # auc(precision, recall)
    if do_spam:
        print('prc_auc: ', prc_auc)
        plt.figure()
        lw = 2
        plt.plot(
            recall,
            precision,
            color="darkorange",
            lw=lw,
            label=f"{args.exp_name} \nPRC (area = {prc_auc:.2})"
        )
        plt.plot(recall[f1_score_max_id], precision[f1_score_max_id], 'ro')
        print('precision-Score: ', precision[f1_score_max_id])
        print('recall-Score: ', recall[f1_score_max_id])
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel("Recall")
        plt.ylabel("Precision")
        plt.title("Precision Recall Curve")
        plt.legend(loc="lower left")
        if not ignor_file:
            plt.savefig(f'{roc_file[:-4]}_prc.png',
                        dpi=100, bbox_inches='tight')
        if do_print:
            plt.show()

    return {'gt': gt, 'pred':  pred, 'fpr': fpr, 'tpr': tpr,  'th': thresholds[np.argmax(f1_scores)],
            'point_roc': (fpr[f1_score_max_id_roc], tpr[f1_score_max_id_roc]),
            'point_prc': (recall[f1_score_max_id], precision[f1_score_max_id]),
            # 'fpr-Score': fpr[f1_score_max_id_roc] ,
            'tpr-Score': tpr[f1_score_max_id_roc],
            'tnr-Score': 1-fpr[f1_score_max_id_roc],
            'Best F1-Score': np.max(f1_scores),
            'prc_auc': prc_auc,
            'roc_auc': roc_auc,
            }
    # if do_spam:
    #    print('Best threshold: ', thresholds[np.argmax(f1_scores)])
    #    print('Best F1-Score: ', np.max(f1_scores))
    #f1_score_max_id_roc = np.argmax(th <= thresholds[np.argmax(f1_scores)])
    # if do_spam:
    #    print('fpr-Score: ', fpr[f1_score_max_id_roc] , )
    #    print('tpr-Score: ', tpr[f1_score_max_id_roc] , '(sensitivity)')
    #    print('tnr-Score: ', 1-fpr[f1_score_max_id_roc], '(specificity)')
