
import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
import torch.nn as nn
from salience.Cam_Collection import CamCollection
from tqdm import tqdm
import evaluation.roc as roc
from torchvision import transforms as T
import random

cuda = True

valsets = []


def compute_exampls(args, model):
    global cuda
    cuda = not args.cpu
    ### Folder Stuff ###
    max_idx = args.hint2
    num_sampls = args.hint4
    mode = args.hint3
    out_folder = os.path.join(args.ckpt_path, args.exp_name, 'exampls')
    global camCol
    camCol = CamCollection(model, args.exp_name, args, class_index=0)
    global valsets
    for idx in range(max_idx):
        args.hint2 = idx
        valset = roc.get_set(args, seg=True)
        global transforms
        transforms = valset.transforms
        valset.transforms = None
        valsets.append(valset)
    #transforms = valset.transforms
    global transforms2
    transforms2 = T.Compose([
        T.Resize((384, 384)),
        T.ToTensor(),
    ])

    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    print('using mode', mode)
    if mode == 1:  # Mode 1: TP beispiele
        pbar = tqdm(total=num_sampls*(max_idx))
        for _ in range(num_sampls):
            sample_id = random.randint(0, len(valset))
            for idx in range(max_idx):
                makeSampel(args, idx, out_folder, sample_id)
                pbar.update(1)
    else:
        pbar = tqdm(total=num_sampls*max_idx)
        for _ in range(num_sampls):
            #sample_id = random.randint(0,len(valset))
            for idx in range(max_idx):
                makeSampel(args, idx, out_folder, valset)
                pbar.update(1)


#buffer_roc = {}


def makeSampel(args, idx_class, out_folder, sample_id=None):
    args.hint2 = idx_class
    roc_path, class_index = roc.get_roc_name(args, camCol.model)
    camCol.class_index = idx_class
    # if roc_path in buffer_roc:
    #    x = buffer_roc[roc_path]
    # else:
    #    x = np.load(roc_path)
    #    buffer_roc[roc_path] = x

    # if sample_id is None:
    #    idx_tp = np.where(np.logical_and(x[0] == 1, x[1] >= 0.5))[0]
    #    sample_id = np.random.choice(idx_tp)

    #pred = x[1, sample_id]

    images, names, org_input, gt, pred, masks, img = getSample(
        idx_class, sample_id)
    gt = gt.reshape(-1)
    pred = pred.reshape(-1)
    pred = [int(pred[i]*1000)/1000 for i in range(pred.shape[0])]
    images.insert(0, org_input)
    names.insert(0,
                 f'{args.exp_name}; gt={gt};\n pred={pred}, \nidx {class_index} is {roc.labels[class_index]}\n {masks}')

    file_name = f"id_{sample_id}_{roc.labels[class_index]}_{roc.get_name(args)}.png"
    target_file = os.path.join(out_folder, file_name)
    file_name = f"id_{sample_id}_{roc.labels[class_index]}_{roc.get_name(args)}_raw.png"
    target_file_raw = os.path.join(out_folder, file_name)
    camCol.print_images(images, names,
                        nrows=2,
                        file=target_file)
    from PIL import Image
    # print(img)
    #im = Image.fromarray(img[0])
    img.save(target_file_raw)
    return sample_id


def getSample(idx_class, sample_id):
    org_input, mask, masks, label = valsets[idx_class].getSample(sample_id,
                                                                 short=False)
    #gt = label[idx_class]

    input_tensor = transforms(org_input)

    if cuda:
        input_tensor = input_tensor.cuda()
    images, names = camCol.execut_all_cam(input_tensor,
                                          org_input,
                                          cuda,
                                          return_cam=False)
    if isinstance(masks, dict):
        org_mask = show_image_with_mask(org_input, mask, masks, idx_class)
    else:
        org_mask = mask
    pred = camCol.out(input_tensor, select_index=False)

    outstr = ''
    for i, j in masks.items():
        x = int(j[0]*org_mask.shape[0]/j[4])
        y = int(j[1]*org_mask.shape[1]/j[5])
        x2 = int(j[2]*org_mask.shape[0]/j[4])
        y2 = int(j[3]*org_mask.shape[1]/j[5])
        name_type = ''.join([j for j in i if not j.isdigit()])

        outstr += f'{name_type}: x {x} - {x2}, y {y} - {y2}\n'

    return images, names, org_mask, label, pred, outstr, org_input


def show_image_with_mask(image, mask, masks, idx_class, alpha=0.2):
    """Show the mask on the image."""
    labels = valsets[idx_class].labels
    target_label = labels[idx_class]
    mask = mask.T
    mask2 = -mask.copy()
    mask3 = -mask.copy()
    for label, box in masks.items():
        label = ''.join([i for i in label if not i.isdigit()])
        if 'no finding' in label.lower():
            continue
        x1, y1, x2, y2, w, h = box
        if target_label == label:
            mask[x1:x2, y1:y2] = 255
        elif label in labels:
            mask3[x1:x2, y1:y2] = 255
        else:
            mask2[x1:x2, y1:y2] = 255

    image = np.array(image)
    mask2 -= mask
    mask = cv2.resize(
        mask.T, dsize=image.shape[:2], interpolation=cv2.INTER_NEAREST)
    mask2 = cv2.resize(
        mask2.T, dsize=image.shape[:2], interpolation=cv2.INTER_NEAREST)
    mask3 = cv2.resize(
        mask3.T, dsize=image.shape[:2], interpolation=cv2.INTER_NEAREST)

    overlay = np.clip(alpha * mask + image[..., 0], 0, 255).astype('uint8')
    overlay2 = np.clip(alpha * mask2 + image[..., 1], 0, 255).astype('uint8')
    overlay3 = np.clip(alpha * mask3 + image[..., 2], 0, 255).astype('uint8')
    return np.stack((overlay, overlay2, overlay3), axis=2)
