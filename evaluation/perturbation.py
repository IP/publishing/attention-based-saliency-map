
from operator import mod
import evaluation.roc as roc
from salience.Cam_Collection import CamCollection
import imp
import os
from salience.LRP.LRP_visu import LRP_vis
import salience.Cam_Functions as cf
import training.Loader as Loader
import numpy as np
import torch
import matplotlib.pyplot as plt
from pickle import NONE
import sys
sys.path.append("..")


def norm(i):
    return (i-i.min())/(i.max()-i.min())


def pertubation_step(camCol: CamCollection, name: str, input_tensor, filtermask, show=False, min_value=False, mode=0, negative_example=None):
    with torch.no_grad():
        input_tensor = input_tensor.detach()
        fit = input_tensor*filtermask
        if not negative_example is None:
            fit += negative_example*(1-filtermask)
        else:
            print('No Negativ')
        fit = fit.detach()

    _, method_name, cam, org_cam = camCol.execut_cam_by_ID(
        mode, fit, None, True, return_cam=True)
    if cam is None:
        return None, None
    org_cam = org_cam.reshape((1, 1,) + org_cam.shape)
    out = camCol.out(fit)

    # print(cam.shape,org_cam.shape,out.shape)
    filtermask_small = filtermask[..., 1, ::16, ::16].detach().cpu().numpy()
    org_cam += 1
    org_cam *= filtermask_small
    if min_value:
        org_cam[0][filtermask_small == 0] = 100000000
    del fit
    with torch.no_grad():
        if min_value:
            idx = org_cam.argmin()
            # print(out,idx)
            org_cam[0][filtermask_small == 0] = 0
        else:
            idx = org_cam.argmax()
        x = (idx // 14)*16
        y = (idx % 14)*16
        filtermask[..., x:x+16, y:y+16] *= 0
        fit = input_tensor*filtermask
        torch.cuda.empty_cache()
        return filtermask.detach().clone(), out


def compute_pertubation_curve(camCol: CamCollection, name, input_tensor, min_value, mode, negative_example=None):
    num_patches = 196
    curve = np.zeros((num_patches,))
    filtermask = input_tensor.clone()*0+1
    out = 1

    for i in range(num_patches):
        # filtermask[i:i+1,i:i+1]*=0
        filtermask, out = pertubation_step(camCol, name, input_tensor, filtermask,
                                           show=False, min_value=min_value, mode=mode, negative_example=negative_example)
        if out is None:
            return None, None
        curve[i] = out
        sys.stdout.write(
            f"\r{i}/{num_patches} -- {out}; Min:{min_value}; Methode:{camCol.id2cam[mode]}                                       ")

    auc = curve.sum()/curve.shape[0]
    sys.stdout.write(f"\r                                                       " +
                     "                                                          \r")

    return curve, auc


def compute_pertubation_curve_all(model, name, args, class_index, input_tensor, negative_example):
    camCol = CamCollection(model, name, args, class_index=class_index)
    names = camCol.id2cam
    typs = ['min_', 'max_']
    dic_curve = {}
    dic_auc = {}

    for i in range(len(typs)):
        for j in range(len(names)):
            current_name = typs[i] + names[j]
            c, auc = compute_pertubation_curve(camCol, name, input_tensor,
                                               min_value=(i == 0), mode=j,
                                               negative_example=negative_example)
            if c is None:
                continue
            dic_curve[current_name] = c
            dic_auc[current_name] = auc
            print(current_name, auc)
    return dic_curve, dic_auc


valset = None


def getSample(index):
    input_tensor, gt = valset[index]
    input_tensor = input_tensor.cuda()
    if len(input_tensor.shape) == 3:
        input_tensor = torch.unsqueeze(input_tensor, 0)
    return input_tensor  # ,org_input,seg,gt


def compute_pertubation_curve_by_index_list(index_list, model, name, args, buffer=None, file=None, negative_example=None, class_index=None):
    allready_computed = 0
    if not file is None and os.path.isfile(file):
        with open(file, 'rb') as f:
            import pickle
            buffer = pickle.load(f)
    if buffer is None:
        buffer = {
            "index": [],
            # "min_*": [],
            # "max_*": [],
            # "auc_*": [],
        }
    for idx in index_list:
        if model is None:
            break
        if idx in buffer["index"]:
            allready_computed += 1
            sys.stdout.write(
                f"\rskip idx {idx}; It already exists. *{allready_computed}                      ")

            continue
        input_tensor = getSample(idx)
        dic_curve, dic_auc = compute_pertubation_curve_all(model, name, args, class_index,
                                                           input_tensor, negative_example)
        buffer["index"].append(idx)
        for k, v in dic_curve.items():
            if k in buffer:
                buffer[k].append(v)
            else:
                buffer[k] = [v]
        for k, v in dic_auc.items():
            if "auc_"+k in buffer:
                buffer["auc_"+k].append(v)
            else:
                buffer["auc_"+k] = [v]

        if not file is None:
            import pickle
            with open(file, 'wb') as f:
                pickle.dump(buffer, f)
    if model is not None:
        legend = []
        typs = ['min_', 'max_']
        for name_typs in typs:
            plt.figure()
            print()
            for k, v in buffer.items():
                if 'auc_' in k and name_typs == '':
                    value = np.array(v)
                    print(k, np.round(value.mean(), 4),
                          np.round(value.std(), 4))
                elif k in 'index':
                    pass
                else:
                    if k.startswith(name_typs):
                        value = np.array(v).mean(axis=0)
                        plt.plot([i/value.shape[0]
                                  for i in range(value.shape[0])], list(value))
                        legend.append(k)
                        #print(k, value.shape)
            plt.ylabel('Value')
            plt.xlabel('% removed')
            plt.title(f"mean {name_typs}pertubation; n={len(buffer['index'])}")
            plt.legend(legend, loc='center left', bbox_to_anchor=(1, 0.5))
            if not file is None:
                if name_typs == '':
                    pass
                else:
                    name_typs = '_' + name_typs[:-1]
                print('save fig')
                print(file[:-4] + name_typs + '.png')
                plt.savefig(file[:-4] + name_typs + '.png',
                            bbox_inches='tight')  # dpi=500

            plt.show()

    return buffer


def pertubate(args, model):
    if args.hint4 == -1:
        num_samples = 100
    else:
        num_samples = args.hint4
    import evaluation.roc as roc
    roc_path, selected_index = roc.get_roc_name(args, model)
    print('num_samples', num_samples, args.hint4)
    import torch
    import numpy as np
    import training.Loader as Loader
    #import salience.Cam_Functions as cf
    cuda = not args.cpu
    if not cuda:
        print('Warning: No cuda')
    if model is not None:
        model.eval()
    name = args.exp_name
    #model.target_layers = [model.a.blocks[-1].norm1]
    #cf.hijack_attention(model, name)
    s = torch.nn.Sigmoid()
    print('Load dataset')
    global valset
    if model is not None:
        valset = roc.get_set(args, seg=False)

    # def get Sample(index):
    #    x = ssim_ds.get Sample(index)
    #    input_tensor = tran sforms(x[0])
    #    if cuda:
    #        input_tensor = input_tensor.cuda()
    #    org_input = np.array(x[0])
    #    seg = np.array(x[2])
    #    gt = x[3]
    #    if len(input_tensor.shape) == 3:
    #        input_tensor = torch.unsqueeze(input_tensor, 0)
    #    return input_tensor, org_input, seg, gt
    print(roc_path)
    x = np.load(roc_path)
    if model is None:
        negative_example = None
    else:
        negative_example = getSample(x[1].argmin())
    # negative_example, _, _, gt2 = get Sample(x[1].argmin()) #TODO argmin

    #out = s(model(negative_example))
    #print(round(out.item()*10000)/100, gt2*100)
    #assert(out <= 0.00001)

    #ssim_ds.data.df["Prediction"] = x[1].round(decimals=5)*100

    idx_tp = np.where(np.logical_and(x[0] == 1, x[1] >= 0.3))[0]
    idx_fp = np.where(np.logical_and(x[0] == 0, x[1] >= 0.3))[0]
    print(idx_tp[:20])
    print(x[1, idx_tp[:20]])
    out_folder = os.path.join(args.ckpt_path, args.exp_name, 'pertubation')
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)

    f1 = os.path.join(out_folder, roc.get_name(args)+'_tp_pertubation.pkl')
    f2 = os.path.join(out_folder, roc.get_name(args)+'_fp_pertubation.pkl')

    buffer_tp = compute_pertubation_curve_by_index_list(
        idx_tp[:num_samples], model, name, args, file=f1, negative_example=negative_example, class_index=selected_index)
    # = compute_pertubation_curve_by_index_list(idx_fp[:num_samples], model, name, args, file=f2, negative_example=negative_example, class_index=selected_index)
    buffer_fp = None
    return buffer_tp, buffer_fp
