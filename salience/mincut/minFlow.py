import numpy as np
import sys
import bisect

from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import maximum_flow
import torch

def findFlow(input_net,output_type = 1):
    #if isinstance(input_net,LayeredNetwork):
    #    num_node = ln.getNumNodes()
    #    num_layer = ln.getNumLayers()
    #    net = ln.intinal_net
    #else:

    att_mat = torch.stack(input_net).clone().squeeze(1).cpu()
    print(att_mat.shape,torch.max(att_mat),torch.min(att_mat[att_mat!=0]))
    att_mat[att_mat<=0.05] = 0
    
    att_mat = torch.sum(att_mat, dim=1)
    print(att_mat.shape,torch.max(att_mat),torch.min(att_mat[att_mat!=0]))
    
    
    num_layer = att_mat.size(0)
    num_node = att_mat.size(1)
    residual_att = torch.eye(num_node)
    #print(residual_att.shape)
    net = att_mat#+ residual_att.cpu()
    net *= 200
    print(net.shape,torch.max(net),torch.min(net[net!=0]))
    size = 2 + num_layer*num_node
    graph = np.zeros((size,size), dtype=int)
    #####Transform data #######
    #s -> layer 0

    graph[0,1:1+num_node] = torch.sum(net)
    for layer_idx in range(num_layer-1):
        #layer i -> j
        offset = layer_idx*num_node
        graph[1+offset:1+offset+num_node,1+offset+num_node:1+offset+2*num_node] = net[layer_idx]
    #layer n -> senke
    graph[size-1-num_node:size-1,size-1] = torch.sum(net)
    #####Transform data END #######

    graph = csr_matrix(graph)
    
    #print(graph)
    #print(graph.toarray())
    #a = maximum_flow(graph, 0, size-1).flow_value
    if output_type == 0:
        return maximum_flow(graph, 0, size-1)
    if output_type == 1:
        flow = maximum_flow(graph, 0, size-1)
        matching = flow.residual.toarray()
        matching[matching<0] = 0
        matching = matching[1:-1-num_node,1+num_node:-1]
        out = [matching[(i)*num_node:(i+1)*(num_node),(i)*num_node:(i+1)*(num_node)] for i in range(num_layer-1)]
        print([torch.sum(net[layer_idx]).item() for layer_idx in range(num_layer-1)])
        print('Flow_value',flow.flow_value)
        return out
    if output_type == 2:
        return maximum_flow(graph, 0, size-1).flow_value




if __name__ == "__main__":
    from shortestArgumentingPath import *
    l = [np.array([[7, 3, 4],[3, 0, 0],[7, 5, 8]]), 
    np.array([[7, 2, 1],[9, 8, 1],[3, 4, 1]]), 
    np.array([[2, 7, 2], [0, 9, 6],[2, 7, 8]])]
    ln = LayeredNetwork(l,verbose=True)
    
    flow = findFlow(ln)
    num_node = 3
    matching = flow.residual.toarray()
    matching[matching<0] = 0
    print(matching[1:-1-num_node,1+num_node:-1])
    print(flow.flow_value)
    