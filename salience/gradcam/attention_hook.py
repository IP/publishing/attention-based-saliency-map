import torch
from torch import tensor
import numpy as np
from pytorch_grad_cam.utils.image import show_cam_on_image, deprocess_image, preprocess_image
import cv2
from torchvision import transforms as T
    
import matplotlib.pyplot as plt
tensor2pli = T.ToPILImage()
pli2tensor = T.ToTensor()

def get_num_cls_token(model):
    height = model.patches
    width = model.patches
    return tensor.size(1) - height*width

def getHocks(args,model):
    model.eval()
    
    if args.deit_base_distilled_patch16_384: #Add Bigger VIT from Facebook here
        image_size = 384
        patches = 24
        target_layers = model.a.blocks[-1].norm1
        hock_fb_vit(model,patches)
    elif args.deit_tiny_distilled_patch16_224 or args.deit_base_distilled_patch16_224:
        image_size = 224
        patches = 14
        target_layers = model.a.blocks[-1].norm1
        hock_fb_vit(model,patches)
    else:
        assert(False)
    model.patches = patches
    model.image_size = image_size
    
    return (model,target_layers,image_size,patches)

def hock_fb_vit(model,patches):
    ## fun 1
    def reshape_transform(tensor):
        num_cls_token = get_num_cls_token(model)
        assert(num_cls_token == 1 or num_cls_token == 2)
        result = tensor[:, num_cls_token:, :].reshape(tensor.size(0),patches, patches, tensor.size(2))
        result = result.transpose(2, 3).transpose(1, 2)
        return result
    
    model.reshape_transform = reshape_transform

    ## fun 2
    #Hijack attention
    last_att = []
    last_q = []
    last_k = []
    last_v = []
    i = len(model.a.blocks)-1
    def makeFun(i):
        def forward_hijack(x):
                if i == 0:
                    last_att.clear()
                    last_q.clear()
                    last_k.clear()
                    last_v.clear()
                self = model.a.blocks[i].attn
                B, N, C = x.shape
                qkv = self.qkv(x).reshape(B, N, 3, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
                q, k, v = qkv[0], qkv[1], qkv[2]   # make torchscript happy (cannot use tensor as tuple)

                attn = (q @ k.transpose(-2, -1)) * self.scale
                attn = attn.softmax(dim=-1)
                #global last_att
                last_att.append(attn)
                last_q.append(q)
                last_k.append(k)
                last_v.append(v)
                #print(len(last_att), 'attention',i)
                attn = self.attn_drop(attn)


                x = (attn @ v).transpose(1, 2).reshape(B, N, C)
                x = self.proj(x)
                x = self.proj_drop(x)
                return x
        return forward_hijack
    for i in range(len(model.a.blocks)):
        model.a.blocks[i].attn.forward = makeFun(i)
    ## fun 3
    def getAttention(source_id = 0):
        if source_id == 0:
            return last_att.copy()
        elif source_id == 1:
            return last_q.copy()
        elif source_id == 2:
            return last_k.copy()
        elif source_id == 3:
            return last_v.copy()
        else:
            assert(False)    
        
    model.getAttention = getAttention


def get_pretrained_vit(version=3,cuda=None):
    global last_att
        
    if version == 0:
        model = torch.hub.load('facebookresearch/deit:main', 'deit_base_distilled_patch16_224', pretrained=True)
    elif version == 1:
        model = torch.hub.load('facebookresearch/deit:main', 'deit_small_distilled_patch16_224', pretrained=True)
    elif version == 2:
        model = torch.hub.load('facebookresearch/deit:main', 'deit_tiny_distilled_patch16_224', pretrained=True)
    elif version == 3:
        model = torch.hub.load('facebookresearch/deit:main', 'deit_base_patch16_384', pretrained=True)
    elif version == 4:
        model = torch.hub.load('facebookresearch/deit:main', 'deit_base_distilled_patch16_384', pretrained=True)
    else:
        raise NotImplementedError()
    if version == 3 or version == 4:
        image_size = 384
        patches = 24
        if cuda is None:
            cuda = False
    else:
        image_size = 224
        patches = 14
        if cuda is None:
            cuda = True
    
    model.eval()
    if cuda:
        model = model.cuda()

    target_layers = model.blocks[-1].norm1
    model.patches = patches
    ## fun 1
    def reshape_transform(tensor):
        global num_cls_token
        height = model.patches
        width = model.patches
        num_cls_token = tensor.size(1) - height*width
        assert(num_cls_token == 1 or num_cls_token == 2)
        result = tensor[:, num_cls_token:, :].reshape(tensor.size(0),height, width, tensor.size(2))
        #result = tensor[:, 1:, :].reshape(tensor.size(0),height, width, tensor.size(2))
        # Bring the channels to the first dimension,
        # like in CNNs.
        result = result.transpose(2, 3).transpose(1, 2)
        return result
    
    model.reshape_transform = reshape_transform
    model.image_size = image_size
    ## fun 2
    #Hijack attention
    last_att = []
    last_q = []
    last_k = []
    last_v = []
    i = len(model.blocks)-1
    def makeFun(i):
        def forward_hijack(x):
                if i == 0:
                    last_att.clear()
                    last_q.clear()
                    last_k.clear()
                    last_v.clear()
                self = model.blocks[i].attn
                B, N, C = x.shape
                qkv = self.qkv(x).reshape(B, N, 3, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
                q, k, v = qkv[0], qkv[1], qkv[2]   # make torchscript happy (cannot use tensor as tuple)

                attn = (q @ k.transpose(-2, -1)) * self.scale
                attn = attn.softmax(dim=-1)
                #global last_att
                last_att.append(attn)
                last_q.append(q)
                last_k.append(k)
                last_v.append(v)
                #print(len(last_att), 'attention',i)
                attn = self.attn_drop(attn)

                x = (attn @ v).transpose(1, 2).reshape(B, N, C)
                x = self.proj(x)
                x = self.proj_drop(x)
                return x
        return forward_hijack
    for i in range(len(model.blocks)):
        model.blocks[i].attn.forward = makeFun(i)

    def getAttention():
        
        return last_att.copy()
    model.getAttention = getAttention
    return (model,target_layers,image_size,patches)

#Source: https://github.com/jeonsworld/ViT-pytorch/blob/main/visualize_attention_map.ipynb
#Mat mul of the attention
def attention_vis(att_mat,patches,image_size=None,ramen = 0):
    if isinstance(image_size, int):
        image_size = (image_size,image_size)
    if len(att_mat)==0:
        return np.ones((image_size[0],image_size[1]))
    att_mat = torch.stack(att_mat).clone().squeeze(1).cpu()
    # Average the attention weights across all heads.
    att_mat = torch.mean(att_mat, dim=1)
    # To account for residual connections, we add an identity matrix to the
    # attention matrix and re-normalize the weights.
    residual_att = torch.eye(att_mat.size(1))
    aug_att_mat = att_mat + residual_att.cpu()
    aug_att_mat = aug_att_mat / aug_att_mat.sum(dim=-1).unsqueeze(-1)
    # Recursively multiply the weight matrices
    joint_attentions = torch.zeros(aug_att_mat.size())
    joint_attentions[0] = aug_att_mat[0]
    for n in range(1, aug_att_mat.size(0)):
        joint_attentions[n] = torch.matmul(aug_att_mat[n], joint_attentions[n-1])

    # Attention from the output token to the input space.
    v = joint_attentions[-1]
    grid_size = patches #int(np.sqrt(aug_att_mat.size(-1)))
    ramen*=int(image_size[0]/grid_size)
    num_cls_token = att_mat.size(1) - patches*patches
    
    mask = v[0, num_cls_token:].reshape(grid_size, grid_size).detach().numpy()
    mask[mask<0] = 0
    mask -= mask.min()
    mask = cv2.resize(mask / mask.max(), (image_size[0],image_size[1]),interpolation=cv2.INTER_NEAREST)[..., np.newaxis]
    if ramen != 0:
        mask[:ramen]=mask.mean()
        mask[-ramen:]=mask.mean()
        mask[:,:ramen]=mask.mean()
        mask[:,-ramen:]=mask.mean()
    mask -= mask.min()
    mask /= mask.max()
    if ramen != 0:
        mask[:ramen]=mask.min()
        mask[-ramen:]=mask.min()
        mask[:,:ramen]=mask.min()
        mask[:,-ramen:]=mask.min()
    return mask


def getExampleImage(image_size,cuda = False,version = 0):
    if version == 0:
        rgb_img = cv2.imread('salience/gradcam/dog_cat.jfif', 1)[:, :, ::-1]
    elif version ==1:
        rgb_img = cv2.imread('salience/gradcam/dog_cat.jfif', 1)[:, :, ::-1]
        rgb_img = rgb_img[:,150:] # only cat
    elif version ==2:
        rgb_img = cv2.imread('salience/gradcam/dog_cat.jfif', 1)[:, :, ::-1]
        rgb_img = rgb_img[:,:150] #only dog
    elif version ==3:
        rgb_img = cv2.imread('salience/gradcam/cat.jpg', 1)[:, :, ::-1]
        smaller = 700
        #smaller = 1
        lr_cut = int((rgb_img.shape[1]-rgb_img.shape[0])/2) + smaller
        rgb_img = rgb_img[smaller:-smaller,lr_cut:-lr_cut] # only cat
    elif version ==4:
        rgb_img = cv2.imread('salience/gradcam/cat.jpg', 1)[:, :, ::-1]
        smaller = 200
        #smaller = 1
        lr_cut = int((rgb_img.shape[1]-rgb_img.shape[0])/2) + smaller
        rgb_img = rgb_img[smaller:-smaller,lr_cut:-lr_cut] # only cat
    elif version ==5:
        rgb_img = cv2.imread('salience/gradcam/cat.jpg', 1)[:, :, ::-1]
    elif version ==6:
        rgb_img = cv2.imread('salience/gradcam/cat2.jpg', 1)[:, :, ::-1]
    else:
        raise NotImplementedError()    

    rgb_img = cv2.resize(rgb_img, (image_size, image_size))
    rgb_img = np.float32(rgb_img) / 255
    input_tensor = preprocess_image(rgb_img,mean=[0.5, 0.5, 0.5],std=[0.5, 0.5, 0.5])
    if cuda:
        input_tensor = input_tensor.cuda()
    input_tensor_org = input_tensor
    return input_tensor,rgb_img


def pixelAttetion(model, layer_index,pixel_index, head = 0,image_size=224):
    attention = model.getAttention()[layer_index].clone()
    if head == -1:
        #att_mat = torch.stack(att_mat).squeeze(1).cpu()
        # Average the attention weights across all heads.
        #att_mat = torch.mean(att_mat, dim=1)
        pass
    else:
        attention = attention[:,head].cpu()
    w = model.patches
    w2 = image_size
    num_cls_token = attention.size(2) - w*w
    assert(num_cls_token == 1 or num_cls_token == 2)
    pixel_index+=num_cls_token
    img1 = attention[:,pixel_index]
    img2 = attention[:,:,pixel_index]
    
    
    img1 = img1[:, num_cls_token:].reshape((w,w))
    img2 = img2[:, num_cls_token:].reshape((w,w))
    
    img1 =img1/img1.max()
    img1 = img1.detach().numpy()
    img1 = cv2.resize((img1), (w2,w2),interpolation=cv2.INTER_NEAREST)[..., np.newaxis]
    
    img2/=img2.max()
    img2 = img2.detach().numpy()
    img2 = cv2.resize((img2), (w2,w2),interpolation=cv2.INTER_NEAREST)[..., np.newaxis]
    
    

    return img1, img2