import cv2
from PIL import Image
import torch
import torch.nn as nn
import numpy as np
from pytorch_grad_cam.utils.image import show_cam_on_image
import skimage.measure
import math


def printGPUstorage():
    t = torch.cuda.get_device_properties(0).total_memory/2
    r = torch.cuda.memory_reserved(0)
    a = torch.cuda.memory_allocated(0)
    f = r-a  # free inside reserved
    print("GPU:", int(r/t*100), int(100*a/t), int(100*f/t))


### utilfunction for cam vis###
def detransform(img, source):
    # print(img.shape,source.shape)
    # if img.shape[0] >= 3:
    #    img.
    res = cv2.resize(
        img, dsize=(source.shape[1], source.shape[0]), interpolation=cv2.INTER_CUBIC)
    return res


# Set to false if you want the native VIT res; Set to true if you want the native Image res;
high_res = True


def show_cam_on_image2(a, b):
    if a is None:
        return None
    if a.shape[:2] != b.shape[:2]:
        if high_res:
            b = detransform(b, a)
            b -= b.min()  # Prevent underflow from interpolation
        else:
            a = detransform(a, b)
    if a.max()-a.min() != 0:
        a = (a - a.min()) / (a.max() - a.min())
    if b.max()-b.min() != 0:
        b = (b - b.min()) / (b.max() - b.min())
    v = show_cam_on_image(a, b, use_rgb=True)

    #v[b<=0.01] = a[b<=0.01]*255
    return v

#num_patch = 14
# A function that removes the CLS Token; Used for GradCam; Copied form the paper)


def reshape_transform(tensor):
    global num_cls_token
    num_patch = math.floor(math.sqrt(tensor.size(1)))
    num_cls_token = tensor.size(1) - num_patch*num_patch
    assert(num_cls_token >= 0 or num_cls_token <= 2)
    result = tensor[:, num_cls_token:, :].reshape(tensor.size(0),
                                                  num_patch,
                                                  num_patch,
                                                  tensor.size(2))
    result = result.transpose(2, 3).transpose(1, 2)
    return result


registered_models_cam = {}


def generate_visualization_GradCAM(name, model, input_tensor, class_index=None, vit=True, cuda=True, compute_out=True):
    from pytorch_grad_cam import GradCAM
    if not name in registered_models_cam:
        if vit:
            #global num_patch
            #num_patch = model.num_patch

            # You can also pass aug_smooth=True and eigen_smooth=True, to apply smoothing.
            cam = GradCAM(model, model.target_layers, use_cuda=cuda,
                          reshape_transform=reshape_transform)
        else:
            cam = GradCAM(model, model.target_layers, use_cuda=cuda)
        registered_models_cam[name] = cam
    cam = registered_models_cam[name]

    with torch.no_grad():
        if compute_out or class_index is None:
            out = (model(input_tensor))
            if class_index is None:
                class_index = out.argmax().cpu().item()
                out = out[..., class_index].data.cpu().numpy()
            else:
                out = out[..., 0].data.cpu().numpy()
        else:
            out = 0
    #from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget
    #targets = [ClassifierOutputTarget(class_index)]
    #grayscale_cam = cam(input_tensor=input_tensor, targets=targets)
    try:
        grayscale_cam = cam(input_tensor=input_tensor, target_category=class_index)
    except:
        from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget
        targets = [ClassifierOutputTarget(class_index)]
        grayscale_cam = cam(input_tensor=input_tensor, targets=targets)
    grayscale_cam = grayscale_cam[0, :]
    reduced = skimage.measure.block_reduce(grayscale_cam, (16, 16), np.max)
    reduced = np.expand_dims(reduced, 0)
    reduced = np.expand_dims(reduced, 0)

    return grayscale_cam, reduced, out


def upscale(transformer_attribution_org, num_patch=None):
    if num_patch is None:
        num_patch = math.floor(math.sqrt(transformer_attribution_org.size(-1)))
    transformer_attribution = torch.nn.functional.interpolate(transformer_attribution_org,
                                                              scale_factor=16,
                                                              mode='bilinear')
    size = transformer_attribution.size(-1)
    transformer_attribution = transformer_attribution.reshape(
        size, size).cuda().data.cpu().numpy()
    return (transformer_attribution - transformer_attribution.min()) / (transformer_attribution.max() - transformer_attribution.min())

# This function produces a cam with TMME (copied from the paper)


def generate_visualization_tmme(model, original_image, class_index=None, cuda=True, is_CoAt=False):
    transformer_attribution, output = generate_relevance(model,
                                                         original_image,
                                                         is_CoAt,
                                                         index=class_index,
                                                         cuda=cuda)
    num_patch = math.floor(math.sqrt(transformer_attribution.size(0)))
    with torch.no_grad():
        num_cls_token = transformer_attribution.size(0) - num_patch*num_patch
        transformer_attribution_org = transformer_attribution[num_cls_token:].reshape(1, 1,
                                                                                      num_patch, num_patch)
        transformer_attribution = upscale(
            transformer_attribution_org, num_patch=num_patch)

    return transformer_attribution, transformer_attribution_org.data.cpu().numpy(), output

# Classical min Attention matmul form the deiV paper.
# Source: https://github.com/jeonsworld/ViT-pytorch/blob/main/visualize_attention_map.ipynb


def attention_vis(att_mat, ramen=0, num_patch=14, mean=True):
    if len(att_mat) == 0:
        assert(False)
    att_mat = torch.stack(att_mat).squeeze(1).cpu()
    # Average the attention weights across all heads.
    if mean:
        att_mat = torch.mean(att_mat, dim=1)
    else:
        att_mat = torch.amin(att_mat, dim=1)
    # To account for residual connections, we add an identity matrix to the
    # attention matrix and re-normalize the weights.
    residual_att = torch.eye(att_mat.size(1))
    aug_att_mat = att_mat + residual_att.cpu()
    aug_att_mat = aug_att_mat / aug_att_mat.sum(dim=-1).unsqueeze(-1)
    # Recursively multiply the weight matrices
    joint_attentions = torch.zeros(aug_att_mat.size())
    joint_attentions[0] = aug_att_mat[0]
    for n in range(1, aug_att_mat.size(0)):
        joint_attentions[n] = torch.matmul(
            aug_att_mat[n], joint_attentions[n-1])

    # Attention from the output token to the input space.
    v = joint_attentions[-1]
    num_cls_token = aug_att_mat.size(-1) - num_patch*num_patch
    mask = v[0, num_cls_token:].reshape(num_patch, num_patch).detach().numpy()
    mask[mask < 0] = 0
    if ramen != 0:
        mask[:ramen] = mask.mean()
        mask[-ramen:] = mask.mean()
        mask[:, :ramen] = mask.mean()
        mask[:, -ramen:] = mask.mean()
    mask -= mask.min()
    mask /= mask.max()
    if ramen != 0:
        mask[:ramen] = mask.min()
        mask[-ramen:] = mask.min()
        mask[:, :ramen] = mask.min()
        mask[:, -ramen:] = mask.min()
    return mask

# Source: https://github.com/hila-chefer/Transformer-MM-Explainability
# rule 5 from paper


def avg_heads(cam, grad):
    cam = cam.reshape(-1, cam.shape[-2], cam.shape[-1])
    grad = grad.reshape(-1, grad.shape[-2], grad.shape[-1])
    cam = grad * cam
    cam = cam.clamp(min=0).mean(dim=0)
    return cam

# rule 6 from paper


def apply_self_attention_rules(R_ss, cam_ss):
    R_ss_addition = torch.matmul(cam_ss, R_ss)
    return R_ss_addition


# TMME continuation
def generate_relevance(model, input, is_CoAt, cuda=True, index=None):
    output = model(input)
    if index == None:
        index = np.argmax(output.cpu().data.numpy(), axis=-1)

    one_hot = np.zeros((1, output.size()[-1]), dtype=np.float32)
    one_hot[0, index] = 1
    one_hot = torch.from_numpy(one_hot).requires_grad_(True)
    if cuda:
        one_hot = one_hot.cuda()
    one_hot = torch.sum(one_hot * output)
    model.zero_grad()
    one_hot.backward(retain_graph=True)
    if is_CoAt:
        ##### COAT #####
        with torch.no_grad():
            num_tokens = model.s3[-1].attn[1].fn.attention_map.shape[-1]
            R = torch.zeros(num_tokens, num_tokens).cuda()
            R2 = torch.eye(num_tokens, num_tokens).cuda()
            # Why R2: Coat Net seems on to be normalized. The diagonal 1 dominate the image. By not additive adding the torch.eye the problem is fixed.
            for blk in model.s3:
                grad = blk.attn[1].fn.attn_gradients
                cam = blk.attn[1].fn.attention_map
                cam = avg_heads(cam, grad)
                for i in range(cam.shape[0]):
                    if cam[i].max() != 0:
                        cam[i] = cam[i] / cam[i].max()
                R += apply_self_attention_rules(R2.cuda(), cam.cuda())
                R2 = R

            num_tokens_new = model.s4[-1].attn[1].fn.attention_map.shape[-1]
            # print(math.floor(math.sqrt(num_tokens)),math.floor(math.sqrt(num_tokens_new)))
            #assert(num_tokens/num_tokens_new == int(num_tokens/num_tokens_new))
            factor = int(math.floor(math.sqrt(num_tokens)) /
                         math.floor(math.sqrt(num_tokens_new)))

            # for blk in model.s4:
            #    grad = blk.attn[1].fn.attn_gradients
            #    cam = blk.attn[1].fn.attention_map
            #    #images = [grad[:,i].cpu().numpy()[0] for i in range(grad.shape[1])] +[R.cpu().numpy(),R.cpu().numpy()]
            #    grad = upscale_attetion_torch(grad, factor=factor)
            #    cam = upscale_attetion_torch(cam, factor=factor)
            #    cam = avg_heads(cam, grad)
            #
            #    R += apply_self_attention_rules(R.cuda(), cam.cuda())
            #    #R = R.mean(dim=0).unsqueeze(1)
            #    #images = [cam.cpu().numpy(),R.cpu().numpy()] + images
            #    ##images =  [R[i].unsqueeze(0).cpu().numpy() for i in range(R.shape[0])]
            #    ##for a in images:
            #    ##    print(a.shape)
            #    #import matplotlib.pyplot as plt
            #    #fig, ax = plt.subplots(nrows=14,
            #    #                ncols=int(len(images)/14),
            #    #                figsize=(120,120))
            #    #plt.subplots_adjust(hspace=.001)
            #    #if 2 != 1:
            #    #    ax = ax.flatten()
            #    #for id, ax in enumerate(ax):
            #    #    ax.set_title(id)
            #    #    if images[id].shape[1] == 196:
            #    #        im = ax.imshow(images[id][0].reshape(14,14))
            #    #    else:
            #    #        im = ax.imshow(images[id][0].reshape(7,7))
            #    #
            #    #    #plt.colorbar(im,ax=ax)
            #    #fig.show()
            R = R.sum(0).unsqueeze(0)
        ##### COAT #####
    else:
        ##### VIT #####
        with torch.no_grad():
            num_tokens = model.a.blocks[0].attn.attention_map.shape[-1]
            R = torch.eye(num_tokens, num_tokens).cuda()
            for blk in model.a.blocks:
                grad = blk.attn.attn_gradients
                cam = blk.attn.attention_map
                cam = avg_heads(cam, grad)
                R += apply_self_attention_rules(R.cuda(), cam.cuda())
        ##### VIT #####
    one_hot.backward()
    del one_hot
    torch.cuda.empty_cache()
    if R[0].max() != 0:
        R[0] /= R[0].max()
    return R[0].detach(), output.cpu().data.numpy()
# END -- Source: https://github.com/hila-chefer/Transformer-MM-Explainability


model_dic = {}


def hijack_attention(m, name):
    model_dic[name] = m
    m.last_att = []
    i = len(m.a.blocks)-1

    def makeFun(i, name):
        def save_attn_gradients(attn_gradients):
            self = model_dic[name].a.blocks[i].attn
            self.attn_gradients = attn_gradients

        def forward_hijack(x):
            if i == 0:
                del model_dic[name].last_att
                model_dic[name].last_att = []
            self = model_dic[name].a.blocks[i].attn
            B, N, C = x.shape
            qkv = self.qkv(x).reshape(B, N, 3, self.num_heads,
                                      C // self.num_heads).permute(2, 0, 3, 1, 4)
            # make torchscript happy (cannot use tensor as tuple)
            q, k, v = qkv[0], qkv[1], qkv[2]

            attn = (q @ k.transpose(-2, -1)) * self.scale
            attn = attn.softmax(dim=-1)
            # attn = self.attn_drop(attn) #Not useless. Requierd to get a hook
            # HOOK
            model_dic[name].last_att.append(attn)
            self.attention_map = attn
            if x.requires_grad:
                attn.register_hook(save_attn_gradients)
            #print(len(last_att), 'attention',i)
            attn = self.attn_drop(attn)

            x = (attn @ v).transpose(1, 2).reshape(B, N, C)
            x = self.proj(x)
            x = self.proj_drop(x)
            return x
        return forward_hijack
    for i in range(len(m.a.blocks)):
        m.a.blocks[i].attn.forward = makeFun(i, name)


def upscale_attetion_torch(inp, factor: int = 2):
    """Torch hole-number upscaler for attetion

    Args:
        inp (torch.Tensor): input Tensor
        factor (int, optional): Factor. Defaults to 2.

    Returns:
        [torch.Tensor]: Upscaled Tensor
    """
    num_patch = math.floor(math.sqrt(inp.size(-1)))
    inp = inp.reshape(-1, num_patch*num_patch, 1, num_patch, num_patch)
    #inp = torch.nn.functional.interpolate(inp, scale_factor=2, mode='bilinear')
    inp = torch.repeat_interleave(
        torch.repeat_interleave(inp, factor, -1), factor, -2)
    out_size = num_patch*num_patch*factor*factor
    inp = inp.reshape(-1, num_patch, num_patch, out_size)
    inp = torch.repeat_interleave(
        torch.repeat_interleave(inp, factor, -2), factor, -3)
    inp = inp.reshape(1, -1, out_size, out_size)
    return inp/(factor*factor)


def upscale_attetion_np(inp, factor: int = 2):
    """Numpy hole-number upscaler for attetion

    Args:
        inp (np.array): input Tensor
        factor (int, optional): Factor. Defaults to 2.

    Returns:
        [torch.Tensor]: Upscaled Tensor
    """
    num_patch = math.floor(math.sqrt(inp.size(-1)))
    inp = inp.reshape(-1, num_patch*num_patch, 1, num_patch, num_patch)
    inp = np.repeat(np.repeat(inp, factor, axis=-1), factor, axis=-2)
    out_size = num_patch*num_patch*factor*factor
    inp = inp.reshape(-1, num_patch, num_patch, out_size)
    inp = np.repeat(np.repeat(inp, factor, axis=-2), factor, axis=-3)
    inp = inp.reshape(1, -1, out_size, out_size)
    return inp/(factor*factor)
