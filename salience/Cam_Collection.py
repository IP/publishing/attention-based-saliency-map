
from pyexpat import model
import re
from training import Loader
import salience.Cam_Functions as cf
import salience.LRP.LRP_visu as LRP_visu
from salience.LRP.markov_chain_lpr import InterpretTransformer as TAM
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
import torch


class CamCollection:
    def __init__(self, model, name: str, args, class_index=None) -> None:
        self.args = args
        model.eval()
        self.model = model
        self.name = name.lower()
        if class_index == -1:
            class_index = None
        self.class_index = class_index

    id2cam = ['Min-Roll-Out','GradCAM', 'TAM Markov', 'TMME', "LPR+TA"
              ]  # "LPR full", "LPR transformer_attribution_s3", "LPR transformer_attribution_s4"
    id2cam_neg = ['Mean Roll Out']
    sig = torch.nn.Sigmoid()

    def out(self, input_tensor, select_index=True):
        with torch.no_grad():
            if len(input_tensor.shape) == 3:
                input_tensor = input_tensor.unsqueeze(0)
            out = self.sig(self.model(input_tensor))
            # print(out.shape,self.class_index,out.shape[-1]!=1,input_tensor.shape)
            if out.shape[-1] != 1 and select_index:
                if (not self.class_index is None):
                    out = out[..., self.class_index]
                else:
                    print('Using auto out')
                    out = out.max()
            # print(out.shape,self.class_index,out.shape[-1])
            return out.detach().cpu()

    def updateModel(self, name, model, cuda):
        self.model = model
        model.eval()
        name = name.lower()
        is_vit = Loader.isViT(self.args)
        LRP_visu.updateLPR(name, model, is_vit, cuda)
    def get_LPR_model(self, name):
        return LRP_visu.registered_models[name].model
    
    def add_target_layer(self):
        model = self.model
        if self.args.densnet_121:
            model.target_layers = [model.densenet121.features[-2].denselayer16.conv2]
        elif Loader.isCoAt(self.args):
            model.target_layers = [model.s4[-1].ff[1].norm]
        else:
            # Assumption VIT
            model.target_layers = [model.a.blocks[-1].norm1]
            cf.hijack_attention(model, self.name)
        #Note: there are multipa implementations of GradCAM, some one it in a list others not...
        #if True:
        #    model.target_layers = model.target_layers[0]
        
        #import os
        #if not os.path.exists('/space/grafr'):
        #    # The server-side gradcam implemntation wants is in a list...
        #    #print('Location Local')
        #    model.target_layers = model.target_layers[0]
        #else:
        #    #print('The server-side gradcam implemntation wants is in a list...')
        #    pass

    def execut_all_cam(self, input_tensor, org_input, cuda, ignor_errors=False, return_cam=True):
        if len(input_tensor.shape) == 3:
            input_tensor = input_tensor.unsqueeze(0)
        images = []
        names = []
        cams = []
        cams_org = []
        for id in range(len(self.id2cam)):

            try:
                out = self.execut_cam_by_ID(id,
                                            input_tensor.detach(),
                                            org_input, cuda,
                                            return_cam=return_cam)
                img = out[0]
                name = out[1]
                if not img is None:
                    images.append(img)
                    names.append(name)
                    if return_cam:
                        cams.append(out[2])
                        cams_org.append(out[3])
            except Exception as e:
                print(e)
                if not ignor_errors:
                    raise e
        if return_cam:
            return images, names, cams, cams_org
        return images, names

    def print_images(self, images, names, nrows, figsize=(16, 12), file=None, font_size=20):
        # repeat inputs, so any ax has an image
        
        while len(images) % nrows != 0:
            images.append(images[len(images) % nrows-1])
            names.append('repeat')
        fig, axes = plt.subplots(nrows=nrows,
                               ncols=int(len(images)/nrows),
                               figsize=figsize)
        plt.subplots_adjust(hspace=.001)
        if nrows != 1:
            axes = axes.flatten()
        for id, ax in enumerate(axes):
            ax.axis('off')
            if id < len(names):
                ax.set_title(names[id],fontsize=font_size)
            ax.imshow(images[id])
        
        if not file is None:
            plt.savefig(file, bbox_inches='tight')
        fig.show()

    def execut_cam_by_ID(self, id, input_tensor, org_input, cuda, return_cam=True):
        if isinstance(org_input, Image.Image):
            org_input = np.array(org_input)
        visualization = None
        cam = None
        org_cam = None
        is_CoAt = Loader.isCoAt(self.args)
        is_vit = Loader.isViT(self.args)
        # if True:
        #    it = TAM(LRP_visu.getLRP(self.name,
        #                             self.model,
        #                             is_vit=is_vit,
        #                             cuda=cuda))
        #    TAM_mask = it.transition_attention_maps(input_tensor,
        #                                            start_layer=id,
        #                                            steps=20,
        #                                            index=self.class_index)
        #    TAM_mask = TAM_mask[0, 1:].reshape(14, 14)
        #    cam = cf.upscale(TAM_mask.unsqueeze(0).unsqueeze(0))
        #    TAM_mask = TAM_mask.cpu().detach().numpy()
        #    TAM_mask = (TAM_mask - TAM_mask.min())
        #    TAM_mask /= TAM_mask.max()
        #    self.model.last_att = []
        #    org_cam= TAM_mask
        #    visualization = cf.show_cam_on_image2(org_input, cam)
        # el
        if (id == 0 or id == -1) and (is_vit or is_CoAt):

            if not hasattr(self.model, 'target_layers'):
                self.add_target_layer()
            self.model(input_tensor)
            if is_vit:
                att = self.model.last_att
            if is_CoAt:
                num_blocks = len(self.model.s3)
                att = [self.model.s3[i].attn[1].fn.attention_map for i in range(num_blocks)]
                from evaluation.utils import upscale_attetion
                num_blocks = len(self.model.s4)
                att += [upscale_attetion(self.model.s4[i].attn[1].fn.attention_map) for i in range(num_blocks)]
                
            org_cam = cf.attention_vis(att,num_patch=14,mean=id!=0).reshape(14, 14)
            if is_CoAt:
                org_cam[...,0,0] -= 1
            cam = cf.upscale(torch.Tensor(org_cam).unsqueeze(0).unsqueeze(0),num_patch=14)
            visualization = cf.show_cam_on_image2(org_input, cam)
        elif id == 1:
            if not hasattr(self.model, 'target_layers'):
                self.add_target_layer()
            cam, org_cam, _ = cf.generate_visualization_GradCAM(self.name,
                                                                self.model,
                                                                input_tensor,
                                                                class_index=self.class_index,
                                                                vit=is_vit or is_CoAt,
                                                                cuda=cuda, compute_out=False,)
            visualization = cf.show_cam_on_image2(org_input, cam)
        elif id == 2 and (is_vit or is_CoAt):
            if is_vit:
                it = TAM(LRP_visu.getLRP(self.name,
                                         self.model,
                                         is_vit=is_vit,
                                         cuda=cuda))
            else:
                it = TAM(self.model)

            TAM_mask = it.transition_attention_maps(input_tensor,
                                                    start_layer=4,
                                                    steps=20,
                                                    index=self.class_index,
                                                    is_vit=is_vit)

            if is_vit:
                TAM_mask = TAM_mask[0, 2:].reshape(14, 14)
            else:
                TAM_mask = TAM_mask.reshape(14, 14)
            cam = cf.upscale(TAM_mask.unsqueeze(0).unsqueeze(0))
            TAM_mask = TAM_mask.cpu().detach().numpy()
            TAM_mask = (TAM_mask - TAM_mask.min())
            TAM_mask /= TAM_mask.max()
            self.model.last_att = []
            org_cam = TAM_mask
            visualization = cf.show_cam_on_image2(org_input, cam)

        elif id == 3 and (is_vit or is_CoAt):
            cam, org_cam, _ = cf.generate_visualization_tmme(self.model,
                                                             input_tensor,
                                                             is_CoAt=is_CoAt,
                                                             class_index=self.class_index,
                                                             cuda=cuda)
            self.model.last_att = []
            visualization = cf.show_cam_on_image2(org_input, cam)
        elif (is_vit or is_CoAt):
            id2 = id-4
            if is_CoAt:
                methods = ["transformer_attribution_s3",]
                           #"transformer_attribution_s3", "transformer_attribution_s4"]
            else:
                methods = ["transformer_attribution"]
            if len(methods) > id2:
                met = methods[id2]
                cam, org_cam, _ = LRP_visu.LRP_vis(self.name, self.model, input_tensor,
                                                   is_vit=is_vit, cuda=cuda, method=met,
                                                   class_index=self.class_index)
                self.model.last_att = []
                visualization = cf.show_cam_on_image2(org_input, cam)
        if org_input is None:
            visualization = cam
        if id >= 0:
            name = self.id2cam[id]
        else:
            name = self.id2cam_neg[-id-1]
        if return_cam:
            if org_cam is not None:
                org_cam = org_cam.squeeze()
                org_cam = np.nan_to_num(org_cam)
                cam = np.nan_to_num(cam)
                if org_cam.max() != 0:
                    org_cam -= org_cam.min()
                    org_cam /= org_cam.max()
                    #cam -= cam.min()
                    #cam /= cam.max()
                org_cam = np.nan_to_num(org_cam)
                cam = np.nan_to_num(cam)
            
            return visualization, name, cam, org_cam

        return visualization, name
