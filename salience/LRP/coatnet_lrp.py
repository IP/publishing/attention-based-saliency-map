import torch
#import torch.nn as nn
import sys
sys.path.append("..")
import salience.LRP.layers_ours as nn2
from salience.LRP.ViT_LRP import compute_rollout_attention
from einops import rearrange
from einops.layers.torch import Rearrange
#TODO: ist .view() ein problem?
#Remainder: * muss neu definiert werden.
import math
def conv_3x3_bn(inp, oup, image_size, downsample=False):
    stride = 1 if downsample == False else 2
    return nn2.Sequential(
        nn2.Conv2d(inp, oup, 3, stride, 1, bias=False),
        nn2.BatchNorm2d(oup),
        nn2.GELU()
    )


class PreNorm(torch.nn.Module):
    def __init__(self, dim, fn, norm):
        super().__init__()
        self.norm = norm(dim)
        self.fn = fn

    def forward(self, x, **kwargs):
        return self.fn(self.norm(x), **kwargs)
    def relprop(self, cam, **kwargs):
        cam = self.fn.relprop(cam, **kwargs)
        cam = self.norm.relprop(cam, **kwargs)
        return cam



class SE(torch.nn.Module):
    def __init__(self, inp, oup, expansion=0.25):
        super().__init__()
        self.avg_pool = nn2.AdaptiveAvgPool2d(1)
        self.fc = nn2.Sequential(
            nn2.Linear(oup, int(inp * expansion), bias=False),
            nn2.GELU(),
            nn2.Linear(int(inp * expansion), oup, bias=False),
            nn2.Sigmoid()
        )
        self.mul = nn2.Mul()
        self.clone1 = nn2.Clone()
        
        self.view1 = nn2.Rearrange("b c y z","b (c y z)", y=1,z=1)
        self.view2 = nn2.Rearrange("b (c y z)","b c y z", y=1,z=1)

    def forward(self, x):
        x1, x2 = self.clone1(x, 2)
        y = self.avg_pool(x2)
        y = self.view1(y)
        y = self.fc(y)
        y = self.view2(y)
        return self.mul([x1,y])
    def relprop(self, cam, **kwargs):
        (cam1, cam2) = self.mul.relprop(cam, **kwargs) #x1,y
        cam2 = self.view2.relprop(cam2,**kwargs)
        cam2 = self.fc.relprop(cam2, **kwargs) 
        cam2 = self.view1.relprop(cam2,**kwargs)
        cam2 = self.avg_pool.relprop(cam2, **kwargs)
        cam = self.clone1.relprop((cam1, cam2), **kwargs)
        return cam


class FeedForward(torch.nn.Module):
    def __init__(self, dim, hidden_dim, dropout=0.):
        super().__init__()
        self.net = nn2.Sequential(
            nn2.Linear(dim, hidden_dim),
            nn2.GELU(),
            nn2.Dropout(dropout),
            nn2.Linear(hidden_dim, dim),
            nn2.Dropout(dropout)
        )

    def forward(self, x):
        return self.net(x)
    def relprop(self, cam, **kwargs):
        cam = self.net.relprop(cam, **kwargs)
        return cam


class MBConv(torch.nn.Module):
    def __init__(self, inp, oup, image_size, downsample=False, expansion=4):
        super().__init__()
        self.downsample = downsample
        stride = 1 if self.downsample == False else 2
        hidden_dim = int(inp * expansion)
        self.add = nn2.Add()
        self.clone = nn2.Clone()
        if self.downsample:
            self.pool = nn2.MaxPool2d(3, 2, 1)
            self.proj = nn2.Conv2d(inp, oup, 1, 1, 0, bias=False)
            

        if expansion == 1:
            self.conv = nn2.Sequential(
                # dw
                nn2.Conv2d(hidden_dim, hidden_dim, 3, stride,
                          1, groups=hidden_dim, bias=False),
                nn2.BatchNorm2d(hidden_dim),
                nn2.GELU(),
                # pw-linear
                nn2.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                nn2.BatchNorm2d(oup),
            )
        else:
            self.conv = nn2.Sequential(
                # pw
                # down-sample in the first conv
                nn2.Conv2d(inp, hidden_dim, 1, stride, 0, bias=False),
                nn2.BatchNorm2d(hidden_dim),
                nn2.GELU(),
                # dw
                #in_channels, out_channels, kernel_size, stride=1, padding
                nn2.Conv2d(hidden_dim, hidden_dim, 3, 1, 1,
                          groups=hidden_dim, bias=False),
                nn2.BatchNorm2d(hidden_dim),
                nn2.GELU(),
                SE(inp, hidden_dim),
                # pw-linear
                nn2.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                nn2.BatchNorm2d(oup),
            )
        
        self.conv = PreNorm(inp, self.conv, nn2.BatchNorm2d)

    def forward(self, x):
        x1, x2 = self.clone(x, 2)
        if self.downsample:
            return self.add([self.proj(self.pool(x1)) , self.conv(x2)])
        else:
            return self.add([x1 , self.conv(x2)])
    def relprop(self, cam, **kwargs):
        (cam1, cam2) = self.add.relprop(cam, **kwargs) #x1,x2
        #x1
        if self.downsample:
            cam1 = self.proj.relprop(cam1, **kwargs)
            cam1 = self.pool.relprop(cam1, **kwargs)
        #x2
        cam2 = self.conv.relprop(cam2, **kwargs)
        cam = self.clone.relprop((cam1, cam2), **kwargs)
        return cam

class Attention(torch.nn.Module):
    def __init__(self, inp, oup, image_size, heads=8, dim_head=32, dropout=0.):
        super().__init__()
        self.matmul1 = nn2.einsum('bhid,bhjd->bhij')
        self.matmul2 = nn2.einsum('bhij,bhjd->bhid')
        self.add1 = nn2.Add()
        inner_dim = dim_head * heads
        project_out = not (heads == 1 and dim_head == inp)

        self.ih, self.iw = image_size

        self.heads = heads
        self.scale = dim_head ** -0.5

        # parameter table of relative position bias
        self.relative_bias_table = torch.nn.Parameter(
            torch.zeros((2 * self.ih - 1) * (2 * self.iw - 1), heads))

        coords = torch.meshgrid((torch.arange(self.ih), torch.arange(self.iw)))
        coords = torch.flatten(torch.stack(coords), 1)
        relative_coords = coords[:, :, None] - coords[:, None, :]

        relative_coords[0] += self.ih - 1
        relative_coords[1] += self.iw - 1
        relative_coords[0] *= 2 * self.iw - 1
        relative_coords = rearrange(relative_coords, 'c h w -> h w c')
        relative_index = relative_coords.sum(-1).flatten().unsqueeze(1)
        self.register_buffer("relative_index", relative_index)

        self.softmax = nn2.Softmax(dim=-1)
        self.to_qkv = nn2.Linear(inp, inner_dim * 3, bias=False)

        self.to_out = nn2.Sequential(
            nn2.Linear(inner_dim, oup),
            nn2.Dropout(dropout)
        ) if project_out else nn2.Identity()
    def save_attn_gradients(self,attn_gradients):
        self.attn_gradients = attn_gradients
    def get_attn_gradients(self):
        return self.attn_gradients
    def save_v(self, v):
        self.v = v
    def save_attn(self, attn):
        self.attn = attn
    def get_attn(self):
        return self.attn

    def save_v_cam(self, cam):
        self.v_cam = cam

    def get_v_cam(self):
        return self.v_cam
    def save_attn_cam(self, cam):
        self.attn_cam = cam

    def get_attn_cam(self):
        return self.attn_cam
    def forward(self, x):
        qkv = self.to_qkv(x).chunk(3, dim=-1)
        q, k, v = map(lambda t: rearrange(
            t, 'b n (h d) -> b h n d', h=self.heads), qkv)

        self.save_v(v)
        dots = self.matmul1([q, k]) * self.scale

        # Use "gather" for more efficiency on GPUs
        relative_bias = self.relative_bias_table.gather(
            0, self.relative_index.repeat(1, self.heads))
        relative_bias = rearrange(
            relative_bias, '(h w) c -> 1 c h w', h=self.ih*self.iw, w=self.ih*self.iw)
        
        dots = self.add1([dots , relative_bias])

        attn = self.softmax(dots)
        self.save_attn(attn)
        #if x.requires_grad:
        attn.register_hook(self.save_attn_gradients) 
        out = self.matmul2([attn, v])
        out = rearrange(out, 'b h n d -> b n (h d)')
        out = self.to_out(out)
        return out

    def relprop(self, cam, **kwargs):
        cam = self.to_out.relprop(cam, **kwargs)
        cam = rearrange(cam, 'b n (h d) -> b h n d', h=self.heads)

        # attn = A*V
        (cam1, cam_v)= self.matmul2.relprop(cam, **kwargs)
        cam1 /= 2
        cam_v /= 2

        self.save_v_cam(cam_v)
        self.save_attn_cam(cam1)

        cam1 = self.softmax.relprop(cam1, **kwargs)
        (cam1, cam_relative_bias)= self.add1.relprop(cam1, **kwargs)

        # A = Q*K^T
        (cam_q, cam_k) = self.matmul1.relprop(cam1, **kwargs)
        cam_q /= 2
        cam_k /= 2

        cam_qkv = rearrange([cam_q, cam_k, cam_v], 'qkv b h n d -> b n (qkv h d)', qkv=3, h=self.heads)

        return self.to_qkv.relprop(cam_qkv, **kwargs)



class Transformer(torch.nn.Module):
    def __init__(self, inp, oup, image_size, heads=8, dim_head=32, downsample=False, dropout=0.):
        super().__init__()
        hidden_dim = int(inp * 4)

        self.ih, self.iw = image_size
        self.downsample = downsample

        if self.downsample:
            self.pool1 = nn2.MaxPool2d(3, 2, 1)
            self.pool2 = nn2.MaxPool2d(3, 2, 1)
            self.proj = nn2.Conv2d(inp, oup, 1, 1, 0, bias=False)

        self.attn = Attention(inp, oup, image_size, heads, dim_head, dropout)
        self.ff = FeedForward(oup, hidden_dim, dropout)

        self.attn = nn2.Sequential(
            nn2.Rearrange('b c ih iw','b (ih iw) c',ih=self.ih, iw=self.iw),
            PreNorm(inp, self.attn, nn2.LayerNorm),
            nn2.Rearrange('b (ih iw) c','b c ih iw', ih=self.ih, iw=self.iw)
        )

        self.ff = nn2.Sequential(
            nn2.Rearrange('b c ih iw','b (ih iw) c',ih=self.ih, iw=self.iw),
            PreNorm(oup, self.ff, nn2.LayerNorm),
            nn2.Rearrange('b (ih iw) c','b c ih iw', ih=self.ih, iw=self.iw)
        )
        self.add1 = nn2.Add()
        self.add2 = nn2.Add()
        self.clone1 = nn2.Clone()
        self.clone2 = nn2.Clone()

    def forward(self, x):
        x1, x2 = self.clone1(x, 2)
        if self.downsample:
            x = self.add1([self.proj(self.pool1(x1)) , self.attn(self.pool2(x2))])
        else:
            x = self.add1([x1 , self.attn(x2)])
        x1, x2 = self.clone2(x, 2)
        x = self.add2([x1,self.ff(x2)])
        return x
    def relprop(self, cam, **kwargs):
        (cam1, cam2) = self.add2.relprop(cam, **kwargs)
        cam2 = self.ff.relprop(cam2,**kwargs)
        cam = self.clone2.relprop((cam1, cam2), **kwargs)

        (cam1, cam2) = self.add1.relprop(cam, **kwargs)
        cam2 = self.attn.relprop(cam2,**kwargs)
        if self.downsample:
            cam1 = self.proj.relprop(cam1,**kwargs)
            cam1 = self.pool1.relprop(cam1,**kwargs)
            cam2 = self.pool2.relprop(cam2,**kwargs)
        cam = self.clone1.relprop((cam1, cam2), **kwargs)
        return cam
def upscale_attetion(inp, factor=2):
    num_patch = math.floor(math.sqrt(inp.size(-1)))
    inp = inp.reshape(num_patch*num_patch,1,num_patch,num_patch)
    #inp = torch.nn.functional.interpolate(inp, scale_factor=2, mode='bilinear')
    inp = torch.repeat_interleave(torch.repeat_interleave(inp, factor, -1), factor,-2)
    out_size = num_patch*num_patch*factor*factor
    inp = inp.reshape(num_patch,num_patch,out_size)
    inp = torch.repeat_interleave(torch.repeat_interleave(inp, factor, -2), factor,-3)
    inp = inp.reshape(1,out_size,out_size)
    return inp/(factor*factor)
            
class CoAtNet(torch.nn.Module):
    def __init__(self, image_size, in_channels, num_blocks, channels, num_classes=1000, block_types=['C', 'C', 'T', 'T']):
        super().__init__()
        
        ih, iw = image_size
        block = {'C': MBConv, 'T': Transformer}

        self.image_size = image_size
        self.in_channels = in_channels
        self.num_blocks = num_blocks
        self.channels = channels
        self.num_classes=num_classes
        self.block_types=block_types

        self.s0 = self._make_layer(
            conv_3x3_bn, in_channels, channels[0], num_blocks[0], (ih // 2, iw // 2))
        self.s1 = self._make_layer(
            block[block_types[0]], channels[0], channels[1], num_blocks[1], (ih // 4, iw // 4))
        self.s2 = self._make_layer(
            block[block_types[1]], channels[1], channels[2], num_blocks[2], (ih // 8, iw // 8))
        self.s3 = self._make_layer(
            block[block_types[2]], channels[2], channels[3], num_blocks[3], (ih // 16, iw // 16))
        self.s4 = self._make_layer(
            block[block_types[3]], channels[3], channels[4], num_blocks[4], (ih // 32, iw // 32))

        self.pool = nn2.AvgPool2d(ih // 32, 1)
        self.fc = nn2.Linear(channels[-1], num_classes, bias=False)
        self.view1 = nn2.Rearrange("b c y z","b (c y z)", y=1,z=1)

    def forward(self, x):
        if len(x.shape)==5:
            x = x.squeeze(0)
        x = self.s0(x)
        x = self.s1(x)
        x = self.s2(x)
        x = self.s3(x)
        x = self.s4(x)
        x = self.pool(x)#.view(-1, x.shape[1])
        x = self.view1(x)
        x = self.fc(x)
        #cam = cam.unsqueeze(0)
        return x

    def _make_layer(self, block, inp, oup, depth, image_size):
        layers = torch.nn.ModuleList([])
        for i in range(depth):
            if i == 0:
                layers.append(block(inp, oup, image_size, downsample=True))
            else:
                layers.append(block(oup, oup, image_size))
        return nn2.Sequential(*layers)

    def relprop(self, cam=None,method="transformer_attribution", is_ablation=False, start_layer=0, **kwargs):
        cam = self.fc.relprop(cam, **kwargs)
        cam = self.view1.relprop(cam, **kwargs)
        cam = self.pool.relprop(cam, **kwargs)
        cam = self.s4.relprop(cam,**kwargs)
        cam = self.s3.relprop(cam,**kwargs)
        cam = self.s2.relprop(cam,**kwargs)
        cam = self.s1.relprop(cam,**kwargs)
        cam = self.s0.relprop(cam,**kwargs)

        if method == "full":
            #There is no patch_embed and no cls token
            # sum on channels
            cam = cam.sum(dim=1)
            cam -= cam.mean()
            cam = cam.clamp(min=0)
            return cam.reshape(1,-1)

        elif method == "rollout":
            # cam rollout
            #s3
            attn_cams = []
            for blk in self.s3:
                attn_heads = blk.attn[1].fn.get_attn_cam().clamp(min=0)
                avg_heads = (attn_heads.sum(dim=1) / attn_heads.shape[1]).detach()
                attn_cams.append(avg_heads)
            cam = compute_rollout_attention(attn_cams, start_layer=start_layer)
            #s4
            attn_cams = []
            for blk in self.s4:
                attn_heads = blk.attn[1].fn.get_attn_cam().clamp(min=0)
                avg_heads = (attn_heads.sum(dim=1) / attn_heads.shape[1]).detach()
                attn_cams.append(avg_heads)
            cam2 = compute_rollout_attention(attn_cams, start_layer=start_layer)
            cam2 = upscale_attetion(cam2)

            cam2 = cam.bmm(cam2)
            return cam2.reshape(1,-1)
        
        # our method, method name grad is legacy
        elif method == "transformer_attribution" or method == "transformer_attribution_s3" or method == "transformer_attribution_s4":
            #s3
            #cam_source = cam.sum(dim=1)
            #cam = cam.sum(dim=1).reshape(1,1,224,224)
            #img_size = self.s3[0].attn[1].fn.get_attn_gradients().size(-1)
            #cam_source = torch.nn.functional.interpolate(cam, size=(img_size,img_size), mode='bilinear').reshape(1,1,img_size,img_size)
            
            attn_cams = []
            for blk in self.s3:
                grad = blk.attn[1].fn.get_attn_gradients()
                cam = blk.attn[1].fn.get_attn_cam()
                cam = cam[0].reshape(-1, cam.shape[-1], cam.shape[-1])
                grad = grad[0].reshape(-1, grad.shape[-1], grad.shape[-1])
                cam = grad * cam
                cam = cam.clamp(min=0).mean(dim=0)
                attn_cams.append(cam.unsqueeze(0))
            cam1 = compute_rollout_attention(attn_cams, start_layer=start_layer)
            #s4
            attn_cams = []
            for blk in self.s4:
                grad = blk.attn[1].fn.get_attn_gradients()
                cam = blk.attn[1].fn.get_attn_cam()
                cam = cam[0].reshape(-1, cam.shape[-1], cam.shape[-1])
                grad = grad[0].reshape(-1, grad.shape[-1], grad.shape[-1])
                cam = grad * cam
                cam = cam.clamp(min=0).mean(dim=0)
                attn_cams.append(cam.unsqueeze(0))
            cam2 = compute_rollout_attention(attn_cams, start_layer=start_layer)
            cam2 = upscale_attetion(cam2)
            cam_mul = cam1.bmm(cam2)
            #shape (1,196,196)
            cam_mul = torch.sum(cam_mul,axis=1)
            cam2 = torch.sum(cam2,axis=1)
            cam1 = torch.sum(cam1,axis=1)
            
            #print(cam2.shape,cam1.shape)
            #cam2 = torch.nn.functional.interpolate(cam2.reshape((1,1,7,7)), scale_factor=2, mode='bilinear').reshape((1,-1))
            #cam2 = upscale_attetion(cam2.reshape((1,7,7)),factor=2).reshape((1,-1))
            #print(cam2.shape,cam1.shape,cam_source.shape)
            #return cam_source.reshape(1,-1) - torch.min(cam_source)
            if method == "transformer_attribution":
                return cam_mul
            if method == "transformer_attribution_s3":
                return cam1
            if method == "transformer_attribution_s4":
                return cam2
            return cam2/torch.max(cam2) + cam1/torch.max(cam1)
            
        elif method == "last_layer":
            cam = self.s4[-1].attn[1].fn.get_attn_cam()
            cam = cam[0].reshape(-1, cam.shape[-1], cam.shape[-1])
            if is_ablation:
                grad = self.s4[-1].attn[1].fn.get_attn_gradients()
                grad = grad[0].reshape(-1, grad.shape[-1], grad.shape[-1])
                cam = grad * cam
            cam = cam.clamp(min=0).mean(dim=0)
            cam = cam
            return cam.reshape(1,-1)

        elif method == "last_layer_attn":
            cam = self.s4[-1].attn[1].fn.get_attn()
            cam = cam[0].reshape(-1, cam.shape[-1], cam.shape[-1])
            cam = cam.clamp(min=0).mean(dim=0)
            cam = cam
            return cam.reshape(1,-1)

        elif method == "second_layer":
            cam = self.s3[1].attn[1].fn.get_attn_cam()
            cam = cam[0].reshape(-1, cam.shape[-1], cam.shape[-1])
            if is_ablation:
                grad = self.s3[1].attn[1].fn.get_attn_gradients()
                grad = grad[0].reshape(-1, grad.shape[-1], grad.shape[-1])
                cam = grad * cam
            cam = cam.clamp(min=0).mean(dim=0)
            cam = cam
            return cam.reshape(1,-1)
        elif method == "nothing":
            return cam

def transfer_weights(net,net2):
    if net2 is None:
        return net
    try:
        tmp = net2.target_layers
    except:
        tmp = None
    net2.target_layers = None
    net2.cuda()
    net.cuda()
    img = torch.randn(1, net.in_channels, 224, 224)
    img=img.cuda()
    net.load_state_dict(net2.state_dict(),strict=True)
    net2.target_layers = tmp
    net.eval()
    net2.eval()
    out2 = net2(img)
    out = net(img)
    assert(torch.isclose(out, out2).all())
    del out
    del out2
    del img
    return net

def coatnet_S(num_classes, model = None):
    channels = [32,64,128,128,128]
    num_blocks = [2, 2, 2, 2, 2]
    net = CoAtNet((224, 224), 3, num_blocks, channels, num_classes=num_classes)
    return transfer_weights(net,model)

def coatnet_0(num_classes, model = None):
    num_blocks = [2, 2, 3, 5, 2]            # L
    channels = [64, 96, 192, 384, 768]      # D
    net = CoAtNet((224, 224), 3, num_blocks, channels, num_classes=num_classes)
    #net.target_layers = net.s4[-1].ff[1].norm
    return transfer_weights(net,model)

def coatnet_1(num_classes, model = None):
    num_blocks = [2, 2, 6, 14, 2]           # L
    channels = [64, 96, 192, 384, 768]      # D
    net = CoAtNet((224, 224), 3, num_blocks, channels, num_classes=num_classes)
    return transfer_weights(net,model)
    

def coatnet_2(num_classes, model = None):
    num_blocks = [2, 2, 6, 14, 2]           # L
    channels = [128, 128, 256, 512, 1026]   # D
    net = CoAtNet((224, 224), 3, num_blocks, channels, num_classes=num_classes)
    return transfer_weights(net,model)
    

def coatnet_3(num_classes, model = None):
    num_blocks = [2, 2, 6, 14, 2]           # L
    channels = [192, 192, 384, 768, 1536]   # D
    net = CoAtNet((224, 224), 3, num_blocks, channels, num_classes=num_classes)
    return transfer_weights(net,model)
    

def coatnet_4(num_classes, model = None):
    num_blocks = [2, 2, 12, 28, 2]          # L
    channels = [192, 192, 384, 768, 1536]   # D
    net = CoAtNet((224, 224), 3, num_blocks, channels, num_classes=num_classes)
    return transfer_weights(net,model)

def coatnet_lrp(model = None):
    image_size = model.image_size
    in_channels = model.in_channels
    num_blocks = model.num_blocks
    channels = model.channels
    num_classes= model.num_classes
    block_types= model.block_types
    net = CoAtNet(image_size, in_channels, num_blocks, channels, num_classes=num_classes,block_types=block_types)
    return transfer_weights(net,model)



def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


if __name__ == '__main__':
    img = torch.randn(2, 3, 224, 224)
    img = img.cuda()
    from training.CoAtNet import coatnet_0 as coatnet_0_2
    import training.Loader as Loader
    args = Loader.reloadSettings('C:/Users/rober/Desktop/ios/output/Net2020/CoAtNet0_new')    ##ugly stuff##
    args.local = True
    args.batch_size = 1
    args.ckpt_path = "C:/Users/rober/Desktop/ios/output/Net2020"
    net2 = Loader.getNN(args)
    net2.target_layers = None
    #net2.train()
    
    #net2 = coatnet_0_2(1)
    
    net = coatnet_0(1,model=net2)
    
   
    net.load_state_dict(net2.state_dict())
    
    out = net(img)
    
    
    from ViT_explanation_generator import LRP
    print(LRP)
    #LRPNet = LRP(net)
    #out2 = LRPNet.generate_LRP(img,cuda=True)[0]
    #print(out.shape,out2.shape, count_parameters(net))

    out2 = net2(img)
    print(out2,"\n",out)
    assert(torch.isclose(out2, out).all())
    #net = coatnet_1(4)
    #out = net(img)
    #print(out.shape, count_parameters(net))
    #
    #net = coatnet_2(4)
    #out = net(img)
    #print(out.shape, count_parameters(net))
    #
    #net = coatnet_3(4)
    #out = net(img)
    #print(out.shape, count_parameters(net))
    #
    #net = coatnet_4(4)
    #out = net(img)
    #print(out.shape, count_parameters(net))
