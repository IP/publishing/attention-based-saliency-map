from imp import new_module
import torch
import numpy as np
import cv2
import sys

sys.path.append("salience/LRP")
from salience.LRP.samples.CLS2IDX import CLS2IDX
#
from salience.LRP.ViT_LRP import vit2lrp as vit_LRP
from salience.LRP.ViT_explanation_generator import LRP
from timm.data.constants import IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD

# create heatmap from mask on image
def show_cam_on_image(img, mask):
    heatmap = cv2.applyColorMap(np.uint8(255 * mask), cv2.COLORMAP_JET)
    heatmap = np.float32(heatmap) / 255
    cam = heatmap + np.float32(img)
    cam = cam / np.max(cam)
    return cam
registered_models = {}

methods = ["full","rollout","transformer_attribution","last_layer","last_layer_attn","second_layer"]

def LRP_vis(name,model, image,is_vit=True,cuda=True,class_index=None,method="transformer_attribution"):
    if not name in registered_models:
        registered_models[name] = getLRP_Model(model,is_vit,cuda=cuda)
    return generate_visualization(registered_models[name],image,cuda,method=method,class_index=class_index)

def getLRP(name,model,is_vit=True,cuda=True):
    if not name in registered_models:
        registered_models[name] = getLRP_Model(model,is_vit,cuda=cuda)
    return registered_models[name].model
def updateLPR(name,model,is_vit=True,cuda=True):
    if name in registered_models:
        torch.cuda.empty_cache()
        new_model = getLRP_Model(model,is_vit,cuda=False)
        registered_models[name].model.load_state_dict(new_model.model.state_dict())
        registered_models[name].model.eval()
        del new_model.model
        del new_model
        torch.cuda.empty_cache()

def getLRP_Model(in_model,is_vit,cuda=True):
    # initialize ViT pretrained with DeiT
    if is_vit:
        model = vit_LRP(in_model)
    else:
        from salience.LRP.coatnet_lrp import coatnet_lrp
        model = coatnet_lrp(in_model)
    if cuda:
        model = model.cuda()
    model.eval()
    attribution_generator = LRP(model)
    return attribution_generator
    
def generate_visualization(attribution_generator, original_image, cuda, class_index=None,method="transformer_attribution"):
    
    original_image = original_image.unsqueeze(0)
    if cuda:
        original_image = original_image.cuda() 
    transformer_attribution, out = attribution_generator.generate_LRP(original_image, method=method, index=class_index,cuda=cuda)
    out.sum().backward()
    with torch.no_grad():
        transformer_attribution = transformer_attribution.detach()
        import math
        num_patch = math.floor(math.sqrt(transformer_attribution.size(1)))
        
        num_cls_token = transformer_attribution.size(1) - num_patch*num_patch
        transformer_attribution_org = transformer_attribution[:,num_cls_token:].reshape(1, 1, num_patch, num_patch)
        transformer_attribution = torch.nn.functional.interpolate(transformer_attribution_org, scale_factor=16, mode='bilinear')
        transformer_attribution = transformer_attribution.reshape(num_patch*16, num_patch*16)
        if cuda:
            transformer_attribution = transformer_attribution.cuda().data.cpu().numpy()
        else:
            transformer_attribution = transformer_attribution.data.numpy()
        transformer_attribution = (transformer_attribution - transformer_attribution.min()) / (transformer_attribution.max() - transformer_attribution.min())
        
        #image_transformer_attribution = original_image.squeeze().permute(1, 2, 0).data.cpu().numpy()
        #image_transformer_attribution = (image_transformer_attribution - image_transformer_attribution.min()) / (image_transformer_attribution.max() - image_transformer_attribution.min())
        #vis = show_cam_on_image(image_transformer_attribution, transformer_attribution)
        #vis =  np.uint8(255 * vis)
        #vis = cv2.cvtColor(np.array(vis), cv2.COLOR_RGB2BGR)
    return transformer_attribution,transformer_attribution_org.data.cpu().numpy(),out.cpu().data.numpy()


