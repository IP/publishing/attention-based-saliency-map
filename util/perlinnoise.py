
import numpy as np
import matplotlib.pyplot as plt
#Source: https://stackoverflow.com/questions/42147776/producing-2d-perlin-noise-with-numpy
def perlinnoise(strachY=(0,15),strachX=(0,3),shape=(28,672),seed = None,minV = -0.1,maxV = 0.1):
    linY = np.linspace(strachY[0],strachY[1],shape[-2],endpoint=False)
    linX = np.linspace(strachX[0],strachX[1],shape[-1],endpoint=False)
    x,y = np.meshgrid(linX,linY) # FIX3: I thought I had to invert x and y here but it was a mistake
    img = perlin(x,y,seed=seed)
    #Normalize
    img -= np.min(img)
    img /= np.max(img)    
    img *= maxV-minV    
    img += minV
    
    return img
def perlin(x,y,seed=None):
    # permutation table
    np.random.seed(seed)
    p = np.arange(256,dtype=int)
    np.random.shuffle(p)
    p = np.stack([p,p]).flatten()
    # coordinates of the top-left
    xi = x.astype(int)
    yi = y.astype(int)
    # internal coordinates
    xf = x - xi
    yf = y - yi
    # fade factors
    u = fade(xf)
    v = fade(yf)
    # noise components
    n00 = gradient(p[p[xi]+yi],xf,yf)
    n01 = gradient(p[p[xi]+yi+1],xf,yf-1)
    n11 = gradient(p[p[xi+1]+yi+1],xf-1,yf-1)
    n10 = gradient(p[p[xi+1]+yi],xf-1,yf)
    # combine noises
    x1 = lerp(n00,n10,u)
    x2 = lerp(n01,n11,u) # FIX1: I was using n10 instead of n01
    return lerp(x1,x2,v) # FIX2: I also had to reverse x1 and x2 here

def lerp(a,b,x):
    "linear interpolation"
    return a + x * (b-a)

def fade(t):
    "6t^5 - 15t^4 + 10t^3"
    return 6 * t**5 - 15 * t**4 + 10 * t**3

def gradient(h,x,y):
    "grad converts h to the right gradient vector and return the dot product with (x,y)"
    vectors = np.array([[0,1],[0,-1],[1,0],[-1,0]])
    g = vectors[h%4]
    return g[:,:,0] * x + g[:,:,1] * y
if __name__ == "__main__":
    import sys #28, 672
    #for i in range(640):
    #    sys.stdout.write('\r'+str(i)+"/"+str(640))
    #    perlin(x,y,seed=3)
    fig=plt.figure(figsize=(15,10))
    for i in range(5):
        #print(np.max(perlinnoise(strachY=(0,3*10),shape=(672,28*10))))
        #print('min ',np.min(perlinnoise(strachY=(0,3*10),shape=(672,28*10))))
        img = perlinnoise(minV=0.0,maxV=1.0,shape=(224,224),strachX=(0,2*i+1),strachY=(0,2*i+1))
        #img[0,0] =1
        fig.add_subplot(4, 5,i+1)
        
        plt.imshow(1-img,cmap='gray')
    for i in range(5):
        #print(np.max(perlinnoise(strachY=(0,3*10),shape=(672,28*10))))
        #print('min ',np.min(perlinnoise(strachY=(0,3*10),shape=(672,28*10))))
        img = perlinnoise(minV=0.0,maxV=1.0,shape=(224,224),strachX=(0,2*i+1),strachY=(0,2*i+1),)
        img *= perlinnoise(minV=0.0,maxV=1.0,shape=(224,224),strachX=(0,2*i+1),strachY=(0,2*i+1))
        #img[0,0] =1
        fig.add_subplot(4, 5,i+6)
        plt.imshow(1-img,cmap='gray')
    for i in range(5):
        #print(np.max(perlinnoise(strachY=(0,3*10),shape=(672,28*10))))
        #print('min ',np.min(perlinnoise(strachY=(0,3*10),shape=(672,28*10))))
        img = perlinnoise(minV=0.0,maxV=1.0,shape=(224,224),strachX=(0,2*i+1),strachY=(0,2*i+1),)
        img *= perlinnoise(minV=0.0,maxV=1.0,shape=(224,224),strachX=(0,2*i+1),strachY=(0,2*i+1))
        img *= perlinnoise(minV=0.0,maxV=1.0,shape=(224,224),strachX=(0,2*i+1),strachY=(0,2*i+1))
        #img[0,0] =1
        fig.add_subplot(4, 5,i+11)
        plt.imshow(1-img,cmap='gray')
    for i in range(5):
        #print(np.max(perlinnoise(strachY=(0,3*10),shape=(672,28*10))))
        #print('min ',np.min(perlinnoise(strachY=(0,3*10),shape=(672,28*10))))
        img = perlinnoise(minV=0.0,maxV=1.0,shape=(224,224),strachX=(0,2*i+1),strachY=(0,2*i+1),)
        img *= perlinnoise(minV=0.0,maxV=1.0,shape=(224,224),strachX=(0,2*i+1),strachY=(0,2*i+1))
        img *= perlinnoise(minV=0.0,maxV=1.0,shape=(224,224),strachX=(0,2*i+1),strachY=(0,2*i+1))
        img *= perlinnoise(minV=0.0,maxV=1.0,shape=(224,224),strachX=(0,1),strachY=(0,1))
        #img[0,0] =1
        fig.add_subplot(4, 5,i+16)
        plt.imshow(1-img,cmap='gray')
    plt.show()
    print('perlinnoise')