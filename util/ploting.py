from matplotlib.pyplot import title
import plotly.graph_objects as go
import plotly.express as px
import plotly.subplots as sub
import numpy as np
import pandas as pd
import itertools
from sklearn.metrics import roc_curve, roc_auc_score,auc
from sklearn.metrics import precision_recall_curve, average_precision_score

import sys
sys.path.append("..")
sys.path.append("../..")
font_size_axis = 16
font_size_legend = 12
font_size_title = 18

#https://stackoverflow.com/questions/37765197/darken-or-lighten-a-color-in-matplotlib
def lighten_color(color, amount=0.4,opacity=0.6):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    c = colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])
    def scale(i):
        return int(i*255)
    return f"rgba({scale(c[0])},{scale(c[1])},{scale(c[2])},{opacity})"

#https://stackoverflow.com/questions/19124239/scikit-learn-roc-curve-with-confidence-intervals
def bootstrap_confindence(y_true,y_pred, interval=0.95,prc=False):
    import numpy as np
    from sklearn.metrics import roc_auc_score

    y_pred = np.array(y_pred)
    y_true = np.array(y_true)

    #print("Original ROC area: {:0.3f}".format(roc_auc_score(y_true, y_pred)))

    n_bootstraps = 10000
    rng_seed = 42  # control reproducibility
    bootstrapped_scores = []

    rng = np.random.RandomState(rng_seed)
    for i in range(n_bootstraps):
        # bootstrap by sampling with replacement on the prediction indices
        indices = rng.randint(0, len(y_pred), len(y_pred))
        if len(np.unique(y_true[indices])) < 2:
            # We need at least one positive and one negative sample for ROC AUC
            # to be defined: reject the sample
            continue
        if prc:
            score = average_precision_score(y_true[indices], y_pred[indices])
        else:        
            score = roc_auc_score(y_true[indices], y_pred[indices])
        
        bootstrapped_scores.append(score)
        #print("Bootstrap #{} ROC area: {:0.3f}".format(i + 1, score))
        sorted_scores = np.array(bootstrapped_scores)
    sorted_scores.sort()

    # Computing the lower and upper bound of the 90% confidence interval
    # You can change the bounds percentiles to 0.025 and 0.975 to get
    # a 95% confidence interval instead.
    interval = (1-interval)/2
    confidence_lower = sorted_scores[int(interval * len(sorted_scores))]
    confidence_upper = sorted_scores[int((1- interval)* len(sorted_scores))]
    #return f"[{confidence_lower:.2f}-{confidence_upper:.2f}]"
    return f" [{confidence_lower:.3f}-{confidence_upper:.3f}]"
    

def plot_prc(list_curves,title="",size=400,font_size = 10):
    fig = go.Figure()
    fig.add_shape(
        type='line', line=dict(dash='dash'),
        x0=0, x1=1, y0=1, y1=0,
    )
    #https://medium.com/plotly/introducing-plotly-express-808df010143d
    col_pal = px.colors.qualitative.Plotly 
    col_pal_iterator = itertools.cycle(col_pal) 
    later_list = []
    min_prc = 1
    for i in list_curves:
        color = next(col_pal_iterator)
        name, gt, pred,_ = i
        precision, recall, thresholds = precision_recall_curve(gt, pred)
        precision = np.concatenate(([0], precision))
        recall = np.concatenate(([1], recall))

        f1_scores = 2*recall*precision/(recall+precision)
        f1_score_max_id = np.argmax(f1_scores)
        point = (recall[f1_score_max_id],precision[f1_score_max_id])
        prc_auc = average_precision_score(gt, pred) 
        name = f"{name} {prc_auc:.2f}{bootstrap_confindence(gt,pred,prc=True)}"
        min_prc = min(min_prc,prc_auc)
        trace = go.Scatter(x=recall, y=precision, name=name, mode='lines',line=dict(color=color),)
        fig.add_trace(trace)
        later_list.append(go.Scatter(
            x=[point[0]],
            y=[point[1]],
            marker_symbol="square",
            marker=dict(color=color, 
                        size=10,
                        line=dict(width=.5,
                                color='DarkSlateGrey')),
            mode="markers",
            showlegend = False
        ))
    for t in later_list:
        fig.add_trace(t)    
    if min_prc <=0.34:
        legend = dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99,
            font_family="Cascadia Mono",
        )
    else:
        legend = dict(
            yanchor="bottom",
            y=0.01,
            xanchor="left",
            x=0.01,
            font_family="Cascadia Mono",
            
        )
    legend['font_size'] = font_size_legend
    fig.update_layout(
        xaxis_title='Recall',
        yaxis_title='Precision',
        margin=dict(l=20, r=20, t=20, b=20),
        title={
            'text': title,
            'y':.99,
            'x':0.55,
            'xanchor': 'center',
            'yanchor': 'top',
            'font_size' : font_size_title
        },
        legend=legend,
        yaxis=dict(constrain='domain',range=[0,1],),
        xaxis=dict(constrain='domain',range=[0,1]), #scaleanchor="x", scaleratio=1
        font_size=font_size_axis,
        width=size, height=size
    )
    return fig

def plot_roc(list_curves,title="", size=400,font_size = 10):
    #from evaluation import roc as r
    #args.hint1 = 1 #Set (1,3,4)
    #args.hint2 = 0 #Class
    #d = r.compute_roc(args,model,do_print=False)
    #fig1 = roc([('TEST',d['gt'],d['pred']),('LRP*XX',d['gt'][1:]+[d['gt'][0]],d['pred'])],title='title')
    
    # Create an empty figure, and iteratively add new lines
    # every time we compute a new class
    fig = go.Figure()
    fig.add_shape(
        type='line', line=dict(dash='dash'),
        x0=0, x1=1, y0=0, y1=1,
    )
    #https://medium.com/plotly/introducing-plotly-express-808df010143d
    col_pal = px.colors.qualitative.Plotly 
    col_pal_iterator = itertools.cycle(col_pal) 
    later_list = []
    for i in list_curves:
        color = next(col_pal_iterator)
        name, gt, pred, point = i
        y_true = gt
        y_score = pred

        fpr, tpr, th = roc_curve(gt, pred)
        
        auc_score = roc_auc_score(y_true, y_score)
        
        

        name = f"{name} {auc_score:.2f}{bootstrap_confindence(gt,pred)}"
        
        trace = go.Scatter(x=fpr, y=tpr, name=name, mode='lines',line=dict(color=color),)
        fig.add_trace(trace)
        later_list.append(go.Scatter(
            x=[point[0]],
            y=[point[1]],
            marker_symbol="square",
            marker=dict(color=color, 
                        size=10,
                        line=dict(width=.5,
                                color='DarkSlateGrey')),
            mode="markers",
            showlegend = False
        ))
        
    for t in later_list:
        fig.add_trace(t)
    fig.update_layout(
        xaxis_title='False Positive Rate',
        yaxis_title='True Positive Rate',
        margin=dict(l=20, r=20, t=20, b=20),
        title={
            'text': title,
            'y':.99,
            'x':0.55,
            'xanchor': 'center',
            'yanchor': 'top',
            'font_size' : font_size_title
        },
        legend=dict(
            yanchor="bottom",
            y=0.01,
            xanchor="right",
            x=.99,
            font_size = font_size_legend+2,
            font_family="Cascadia Mono",
        ),
        yaxis=dict(constrain='domain',
                    range=[0,1],
                    tickmode = 'linear',
                    tick0 = 0,
                    dtick = 0.2
                    ),
        xaxis=dict(constrain='domain',
                    range=[0,1],
                    tickmode = 'linear',
                    tick0 = 0,
                    dtick = 0.2
                   ), #scaleanchor="x", scaleratio=1
        font_size=font_size_axis,
        width=size, height=size,
        
    )
    return fig
    fig.show()
    
def plot_curve_AUC(xs,ys,names,title="",width=500,height=500, xtitle='',ytitle='',legend=dict(
            yanchor="bottom",
            y=0.01,
            xanchor="right",
            x=.99
        )):
    fig = go.Figure()
    #https://medium.com/plotly/introducing-plotly-express-808df010143d
    col_pal = px.colors.qualitative.Plotly 
    col_pal_iterator = itertools.cycle(col_pal) 
    for x,y,name in zip(xs,ys,names):
        color = next(col_pal_iterator)
        auc_score = auc(x, y)
        
        

        name = f"{name:<12}\t{auc_score:.2f}"
        
        trace = go.Scatter(x=x, y=y, name=name, mode='lines',line=dict(color=color),)
        fig.add_trace(trace)

    fig.update_layout(
        xaxis_title=xtitle,
        yaxis_title=ytitle,
        margin=dict(l=20, r=20, t=20, b=20),
        title={
            'text': title,
            'y':.99,
            'x':0.55,
            'xanchor': 'center',
            'yanchor': 'top',
            'font_size' : font_size_title
        },
        legend=legend,
        yaxis=dict(constrain='domain'),
        xaxis=dict(constrain='domain'), #scaleanchor="x", scaleratio=1
        width=width, height=height
    )
    return fig
    
def plot_curves(xs,ys,names,title="",width=500,height=500, xtitle='',ytitle='',legend=dict(
            yanchor="bottom",
            y=0.01,
            xanchor="right",
            x=.99,
            font_family="Cascadia Mono",
            
            ),
            padding=20,
            font_size=10,
            showlegend = True,
            stds = None,
            yrange = None):
    legend['font_size'] = font_size_legend
    fig = go.Figure()
    #https://medium.com/plotly/introducing-plotly-express-808df010143d
    col_pal = px.colors.qualitative.Plotly 
    col_pal_iterator = itertools.cycle(col_pal) 
    if not stds is None:
        for x,y,std,name in zip(xs,ys,stds,names):
            color = next(col_pal_iterator)
            #print(color)
            fig.add_trace(go.Scatter(
                name='Upper Bound ' +name,
                x=x,
                y=y+std,
                mode='lines',
                marker=dict(color="#444"),
                line=dict(width=0),
                showlegend=False,
                opacity=0.1
            ))
            fig.add_trace(go.Scatter(
                name='Lower Bound ' + name,
                x=x,
                y=y-std,
                marker=dict(color="#444"),
                line=dict(width=0),
                mode='lines',
                fillcolor=lighten_color(color),
                fill='tonexty',
                showlegend=False,
                opacity=0.1
            ))
            
        
    col_pal = px.colors.qualitative.Plotly 
    col_pal_iterator = itertools.cycle(col_pal) 
    for x,y,name in zip(xs,ys,names):
        color = next(col_pal_iterator)
        trace = go.Scatter(x=x, y=y, name=name, mode='lines',line=dict(color=color),)
        fig.add_trace(trace)
        
    yaxis = dict(constrain='domain')
    if not yrange is None:
        yaxis['range'] = yrange
    fig.update_layout(
        xaxis_title=xtitle,
        yaxis_title=ytitle,
        margin=dict(l=padding, r=padding, t=padding, b=padding),
        title={
            'text': title,
            'y':.99,
            'x':0.55,
            'xanchor': 'center',
            'yanchor': 'top',
            'font_size' : font_size_title
        },
        legend=legend,
        
        yaxis=yaxis,
        xaxis=dict(constrain='domain'), #scaleanchor="x", scaleratio=1
        width=width, height=height,
        showlegend = showlegend,
        font_size=font_size_axis,
    )
    return fig


def plot_curves_pertubation(buffer,title,size=None,width=400, height=400,
                            padding=20,
                            font_size=10,
                            showlegend = True,
                            yrange = None,
                            count=False,filename='',legend2side=False):
    if not size is None:
        width=size
        height=size
    map_old2new ={
        'min_Min Roll Out' : 'min_Min-Roll-Out', 
        'min_GradCam': 'min_GradCAM', 
        'min_TAM Marcov':'min_TAM Markov', 
        'min_LPR TA': 'min_LPR+TA', 
        'max_Min Roll Out' :'max_Min-Roll-Out', 
        'max_GradCam': 'max_GradCAM', 
        'max_TAM Marcov':'max_TAM Markov', 
        'max_LPR TA': 'max_LPR+TA'
    }
    typs = ['min_', 'max_']
    figs = [go.Figure(),go.Figure()]
    if legend2side:
        legends = [dict(yanchor="top",
                y=0.99,
                xanchor="left",
                x=1.01,
                font_size = font_size_legend,
                font_family="Cascadia Mono",
                ),
                dict(yanchor="top",
                y=0.99,
                xanchor="left",
                x=1.01,
                font_family="Cascadia Mono",
                font_size = font_size_legend
                ),
                ]
        
    else:
        legends = [dict(yanchor="bottom",
                y=0.01,
                xanchor="left",
                x=-0.15,
                font_size = font_size_legend-2,
                font_family="Cascadia Mono",
                ),
                dict(yanchor="top",
                y=0.99,
                xanchor="right",
                x=0.99,
                font_family="Cascadia Mono",
                font_size = font_size_legend-2
                ),
                ]
        if 'CoAt1' in filename or 'Vit5' in filename:
            legends[0] = legends[1]
        elif 'Vit1' in filename and "14 - PTX" in filename:
            legends[0] = legends[1]
        elif 'Vit1' in filename and "14 - CNS" in filename:
            legends[0] = legends[1]
        elif ('Vit1' in filename or 'Vit3' in filename) and "14 - PE" in filename:
            legends[0] = dict(yanchor="bottom",
                y=0.01,
                xanchor="left",
                x=0.01,
                font_size = font_size_legend-2,
                font_family="Cascadia Mono",
                )
        elif ('Vit1' in filename and "14 - ATC" in filename)  or ('Vit3' in filename and "14 - PTX" in filename):
            legends[0] = legends[1]
            #legends[0] = dict(yanchor="bottom",
            #    y=-0.15,
            #    xanchor="left",
            #    x=-0.2,
            #    font_size = font_size_legend,
            #    font_family="Cascadia Mono",
            #    )
        elif 'Vit3' in filename and ("14 - CMG" in filename or "14 - CNS" in filename or "14 - ATC" in filename ):
            legends[0] = legends[1]
        legends[1] = dict(yanchor="top",
                    y=0.99,
                    xanchor="right",
                    x=0.99,
                    font_family="Cascadia Mono",
                    font_size = font_size_legend+2
                    )
    aucs = {}
    for name_typs,fig,legend in zip(typs,figs,legends):
        col_pal = px.colors.qualitative.Plotly 
        col_pal_iterator = itertools.cycle(col_pal) 
        
        for k, v in buffer.items():
            if 'auc_' in k:
                value = np.array(v)
                if k[4:] in map_old2new:
                    name = map_old2new[k[4:]]
                else:
                    name = k[4:]
                aucs[name] = (value.mean(),value.std())
        for k, v in buffer.items():
            
            if 'auc_' in k and name_typs == '':
                pass
            elif k in 'index':
                pass
            else:
                
                if k.startswith(name_typs):
                    if k in map_old2new:
                        k = map_old2new[k]

                    color = next(col_pal_iterator)
                    mean, std = aucs[k]
                    name = f"{k[4:]:<12} {mean:.2f}±{std:.2f}"
                    value = np.array(v).mean(axis=0)
                    x = [i/value.shape[0] for i in range(value.shape[0])]
                    trace = go.Scatter(x=x, y=list(value), name=name, mode='lines',line=dict(color=color),)
                    fig.add_trace(trace)
                    #print(k, value.shape)
                    
        yaxis = dict(constrain='domain')
        if not yrange is None:
            yaxis['range'] = yrange
        if count:
            title = f"{title}n={len(buffer['index'])}"
        fig.update_layout(
            xaxis_title='% Removed',
            yaxis_title='Model Confidence',
            margin=dict(l=padding, r=padding, t=padding, b=padding),
            title={
                'text': title,
                'y':.99,
                'x':0.55,
                'xanchor': 'center',
                'yanchor': 'top',
                'font_size' : font_size_title
            },
            legend=legend,        
            yaxis=yaxis,
            xaxis=dict(constrain='domain'), #scaleanchor="x", scaleratio=1
            width=width, height=height,
            showlegend = showlegend,
            font_size=font_size_axis,
        )
    return figs,aucs

def plot_curves_sensitivity_n(buffers,results,title,size=400,
                            padding=20,
                            font_size=10,
                            showlegend = True,
                            yrange = None,
                            count = False):
   
    fig = go.Figure()
    legend = dict(yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99,
            font_size = font_size_legend,
            font_family="Cascadia Mono",
            )
    aucs = {}
    curves = {}
    col_pal = px.colors.qualitative.Plotly 
    col_pal_iterator = itertools.cycle(col_pal) 
    #buffers : (index -> list of idx, method_names -> buffer)
    #buffer : (n_list -> list of number of patches, index-> list of idx, corr_all_n,corr_all_idx -> dic of n)
    #buffer['corr_all_n'][str(n)] -> list of correlation scors
    #buffer['corr_all_idx'][str(n)] = -> list of idx
    #results: method_names -> result
    #result: n -> y
    num_masks = 200
    for methode_name, buffer in buffers.items():
        if methode_name in 'index':
            continue
        else:
            dic_n_2_cor = buffer['corr_all_n']
            x = []
            y = []
            for n in buffer['n_list']:
                x.append(n)
                y.append(dic_n_2_cor[str(n)]) #2D list
            x = np.array(x)
            y = np.array(y)
            value = []
            for i in range(len(buffer['index'])):
                a = auc(x,y[:,i])
                value.append(a)
            value = np.array(value)
            
            result = results[methode_name]
            try:
                num_masks = results[num_masks]
            except:
                num_masks = 200
            y2 = []
            for n in buffer['n_list']:
                y2.append(result[str(n)])
            y2 = np.array(y2)
            max_n = buffer['n_list'][-1]
            aucs[methode_name] = (value.mean(),value.std())
            curves[methode_name] = list(x),list(y2)
            print('AUC_curve:',auc(x,y2),'AUC_mean:',aucs[methode_name])
            
    for methode_name, buffer in buffers.items():
        if methode_name in 'index':
            pass
        else:
            color = next(col_pal_iterator)
            mean, std = aucs[methode_name]
            x , y= curves[methode_name] 
            name = f"{methode_name:<12}\t{mean:>6.2f}±{std:>5.2f}"
            
            trace = go.Scatter(x=x, y=y, name=name, mode='lines',line=dict(color=color),)
            fig.add_trace(trace)
                
    yaxis = dict(constrain='domain')
    if not yrange is None:
        yaxis['range'] = yrange
    if count:
        title = f"{title}n={len(buffers['index'])}"
    fig.update_layout(
        xaxis_title='n Patches Removed (log)',
        yaxis_title='Mean Correlation Score',
        margin=dict(l=padding, r=padding, t=padding, b=padding),
        title={
            'text': title,
            'y':.99,
            'x':0.55,
            'xanchor': 'center',
            'yanchor': 'top',
            'font_size' : font_size_title
        },
        legend=legend,        
        yaxis=yaxis,
        xaxis=dict(constrain='domain',type="log"), #scaleanchor="x", scaleratio=1
        width=size, height=size,
        showlegend = showlegend,
        font_size=font_size_axis,
    )
    return fig,aucs


def plot_curves_ehr(buffer,title,size=400,
                            padding=20,
                            font_size=10,
                            showlegend = True,
                            yrange = None,
                            leg=False, count = False):
   
    fig = go.Figure()
    legend = dict(yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99,
            font_size = font_size_legend,
            font_family="Cascadia Mono",
            )
    if leg:
        legend = dict(yanchor="bottom",
            y=0.01,
            xanchor="right",
            x=0.99,
            font_size = font_size_legend,
            font_family="Cascadia Mono",
            )
        
    aucs = {}
    curves = {}
    col_pal = px.colors.qualitative.Plotly 
    col_pal_iterator = itertools.cycle(col_pal) 
    ###THRESHOLD_GRAPH###
    names = [key for key in buffer.keys() if str(key).endswith('_th')]
    names = [i[:-3] for i in names]
    ##EHR_GRAPH##
    #results = [buffer[key] for key in legend]
    length = len(buffer[names[0]])
    #results = [np.stack(np.array(r), axis=0).mean(axis=0) for r in results]
    #legend = [i[:-3] for i in names]
    #plt.ylabel('% segmented')
    #plt.xlabel('threshold (relativ)')
    #plt.title(f"EHR  n = {length}")
     
    for methode_name in names:
        y = buffer[methode_name]
        x = np.array([i/len(y[0]) for i in range(len(y[0]))])
        value = []
        for i in range(length):
            a = auc(x,y[i])
            value.append(a)
        value = np.array(value)
        y = np.array(buffer[methode_name]).mean(axis=0)
        aucs[methode_name] = (value.mean(),value.std())
        curves[methode_name] = list(x),list(y)
        #print('AUC_curve:',auc(x,y),'AUC_mean:',aucs[methode_name])
            
    for methode_name in names:
        color = next(col_pal_iterator)
        mean, std = aucs[methode_name]
        x , y= curves[methode_name] 
        name = f"{methode_name:<12} {mean:.2f}±{std:.2f}"
        
        trace = go.Scatter(x=x, y=y, name=name, mode='lines',line=dict(color=color),)
        fig.add_trace(trace)
                
    yaxis = dict(constrain='domain')
    if not yrange is None:
        yaxis['range'] = yrange
    if count:
        title = f"{title}n={length}"
    fig.update_layout(
        xaxis_title='Threshold',
        yaxis_title='EHR',
        margin=dict(l=padding, r=padding, t=padding, b=padding),
        title={
            'text': title,
            'y':.99,
            'x':0.55,
            'xanchor': 'center',
            'yanchor': 'top',
            'font_size' : font_size_title
        },
        legend=legend,        
        yaxis=yaxis,
        xaxis=dict(constrain='domain'), #scaleanchor="x", scaleratio=1
        width=size, height=size,
        showlegend = showlegend,
        font_size=font_size_axis,
    )
    return fig,aucs



#https://stackoverflow.com/questions/45577255/plotly-plot-multiple-figures-as-subplots 
def figures_to_html(figs, filename="dashboard.html"):
    with open(filename, 'w') as dashboard:
        dashboard.write("<html><head></head><body>" + "\n")
        for fig in figs:
            inner_html = fig.to_html().split('<body>')[1].split('</body>')[0]
            dashboard.write(inner_html)
        dashboard.write("</body></html>" + "\n")
    import webbrowser
    import os
    webbrowser.open('file://' + os.path.realpath(filename))
    
if __name__ == "__main__":
    
    
    from evaluation import utils
    args, model = utils.reload('C:/Users/rober/Desktop/ios/output/02_new/','Vit3_cl5')
    #args, model = utils.reload('C:/Users/rober/Desktop/ios/output/02_new/','vit1_5cl')
    #args, model = utils.reload('C:/Users/rober/Desktop/ios/output/02_new/','Vit3_cl5')
    #args, model = utils.reload('C:/Users/rober/Desktop/ios/output/Net2020/','Vit3_on_class2')
    #args, model = utils.reload('C:/Users/rober/Desktop/ios/output/Net2020/','CoAtNet0_new')
    from evaluation import roc as r
    args.hint1 = 1 #Set (1,3,4)
    args.hint2 = 0 #Class
    d = r.compute_roc(args,model,do_print=False)
    test = []
    for h1 in [1,3,4]:#1,3,
        args.hint1 = h1 #Set (1,3,4)
        for h2 in range(5):
            if h1 == 1 and h2 != 0:
                break
            args.hint2 = h2 #Class
            d = r.compute_roc(args,model,do_print=False)
            test.append((r.get_name(args).replace('_vals_',' '),d['gt'],d['pred'],d['point_roc']))
            
    #{'gt': gt, 'pred':  pred, 'fpr': fpr, 'tpr': tpr, 'roc_auc': roc_auc, 'th':thresholds[np.argmax(f1_scores)]}
    fig1 = plot_roc(test,title='ROC')
    #fig1 = plot_prc(test,title='ROC')
    
    #figures_to_html([fig1,fig2])
    fig1.write_image("fig1.svg")
    fig1.write_image("fig1.png")
    fig1.show()

    