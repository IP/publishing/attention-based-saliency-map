import os
import sys

import time

import matplotlib.pyplot as plt
import time
import datetime
import sys
import numpy as np

class Logger():
    def __init__(self,epoch, n_epochs, batches_epoch,path='output',):#graphupdate = 40000
        self.epoch = epoch
        self.n_epochs = n_epochs
        self.batch = 0
        self.batches_epoch = batches_epoch+1
        self.n_epochs = n_epochs
        self.path = path
        #self.graphupdate = graphupdate
        self.status = {}
        self.start_time = time.time()-1
        self.val = False
    
    def log(self, losses:dict, val:bool, epoch:int):
        if self.val != val:
            self.batch = 0
        self.val = val
        if self.epoch != epoch:
            reset = True
            self.epoch = epoch
            self.batch = 0
        else:
            reset = False
            self.batch +=1
        #Print text
        time_passed = (time.time() - self.start_time)
        self.prev_time = time.time()
        if val:
            outputstring = f'\rVAL--Epoch %03d/%03d [%04d] -- ' % (self.epoch, self.n_epochs, self.batch)
        else:
            outputstring = f'\rEpoch %03d/%03d [%04d/%04d] -- ' % (self.epoch, self.n_epochs, self.batch, self.batches_epoch)
            
        
        
        if 'ALL' in self.status:
            outputstring+=('%.7f| ' % self.status['ALL'].get_val(val))
        for loss_name, loss_child in self.status.items():
            if loss_name == 'ALL':
                continue        
            outputstring+=('%s:%.5f|' % (loss_name, loss_child.get_val(val)))
        batches_done = (self.batches_epoch)*(self.epoch - 1) + self.batch
        batches_left = (self.batches_epoch)*(self.n_epochs - self.epoch) + self.batches_epoch - self.batch 
        if not val:
            outputstring+=f'ETA: {datetime.timedelta(seconds=time_passed/batches_done*batches_left)}' 
        sys.stdout.write(outputstring)
        epoch_exact = epoch + self.batch/self.batches_epoch
        #Add new
        for name,loss in losses.items():
            if not name in self.status:
                self.status[name] = Logger_Child()
            lc = self.status[name]
            lc.log(loss,val,clear=reset,epoch_exact=epoch_exact)
            
        if reset:
            for loss_name, loss_child in self.status.items():
                loss_plot_auto(loss_name,loss_child,self)
                
            
            
        
             
class Logger_Child():
    def __init__(self):
        self.train = []
        self.val = []
        self.train_print = []
        self.val_print = []
        self.x = []
        
    def clear(self,epoch_exact):
        self.train_print.append(self.get_val(False))
        self.val_print.append(self.get_val(True))
        self.x.append(epoch_exact)
        self.train = []
        self.val = []
    def log(self, loss:dict, val:bool,epoch_exact=0, clear:bool = False,):
        if clear:
            self.clear(epoch_exact)
        if val:
            l = self.val
        else:
            l = self.train
        l.append(loss)
        
    def get_val(self,val):
        if val:
            l = self.val
        else:
            l = self.train
        if len(l) == 0:
            return 0
        return sum(l)/len(l)
    
def loss_plot_auto(loss_name,loss_child,logger):
    mini = int(min(loss_child.x))
    loss_plot(X=np.array(loss_child.x),
              Y=np.array(loss_child.train_print),
              X2=np.array([i for i in range(mini,mini+len(loss_child.val_print))]),
              Y2=np.array(loss_child.val_print),
              filename=os.path.join(logger.path,loss_name),
              opts={'xlabel': 'epochs', 'ylabel': loss_name, 'title': loss_name}
              )

def loss_plot(X,Y,X2,Y2,filename,opts={'xlabel': 'epochs', 'ylabel': 'y', 'title': ''} , ):
    
    #print(X,Y)
    plt.plot(X,Y[:len(X)])
    plt.plot(X2,Y2[:len(X2)])
    
    
    plt.ylabel(opts['ylabel'])
    plt.xlabel(opts['xlabel'])
    plt.title(opts['title'])
    plt.legend(['Train', 'Val'], loc='upper right')
    try:
        plt.savefig(filename)
    except:
        pass
    plt.close()   
    try:
        Y_ = Y
        Y_[Y == 0] = 1
        plt.plot(X,np.log(Y_[:len(X)]))
    except Exception as e:
        print(e)
    try:
        Y2_ = Y2
        Y2_[Y2 == 0] = 1
        
        plt.plot(X2,np.log(Y2_[:len(X2)]))
    except Exception as e:
        print(e)
    
    plt.ylabel(opts['ylabel'])
    plt.xlabel(opts['xlabel'])
    plt.title(opts['title']+' - Log-Scale')
    plt.legend(['Train', 'Val'], loc='upper right')
    try:
        plt.savefig(filename+'_log')
    except:
        pass
    plt.close()                 

