import argparse


from training.MLSMTrain import train
from training.STTrain import train as train_one_class
import training.Loader as Loader


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    Loader.parserNN(parser)
    Loader.parseArgmentation(parser)
    ##add coustom parse info here##
    args = Loader.parserDefaults(parser)
    # Make models
    model = Loader.getNN(args)
    # To Cuda
    Loader.toCuda(args, nets=[model], shape=model.input_shape)
    # load checkpoint
    if not args.new:
        Loader.load(args, model)
    if args.train:
        # Multiclass training
        train(args, model)
    if args.train2:
        # Train on a single class (unstable)
        model.output_shape = (1,)
        train_one_class(args, model)
    elif args.test or args.pertubate:
        # Compute ROC/AUC
        from evaluation.roc import compute_roc
        model.mode = 1
        out = compute_roc(args, model)
    if args.pertubate:
        # Run Pertubation test
        from evaluation import perturbation
        model.mode = 1
        out = perturbation.pertubate(args, model)
    elif args.ehr:
        # Run Effective Hear Ratio test
        from evaluation import ehr
        out = ehr.compute_ehr(args, model)
    elif args.example:
        # Genertate Images with the Segmentaion (unstable)
        from evaluation import example_maker
        example_maker.compute_exampls(args, model)
    elif args.sanity:
        # Run Sanity tests
        from evaluation import sanity_check
        sanity_check.sanity_check(args, model)
    elif args.sensitivity_n:
        # Run sensitivity_n tests (token size = 16x16)
        from evaluation import sensitivity_n
        sensitivity_n.compute_sensitivity_n(args, model, patch=True)
    elif args.sensitivity_n_pix:
        # Run sensitivity_n tests (token size = 1 x 1) (unstable)
        from evaluation import sensitivity_n
        sensitivity_n.compute_sensitivity_n(args, model, patch=False)
